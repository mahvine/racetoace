angular
	.module("racetoace")
	.config(function($urlRouterProvider, $stateProvider,$httpProvider){
		$stateProvider
			//awards
			.state('awardlist',{
				url : '/awardlist',
				templateUrl : '/admin/scripts/app/award/award-list.html',
				controller:'AwardController'
			})
			.state('newaward',{
	            url : '/newaward',
				templateUrl : '/admin/scripts/app/award/award-form.html',
				controller:'AwardController'
			})
			.state('editaward',{
				url : '/editaward/:awardId',
				templateUrl : '/admin/scripts/app/award/award-form.html',
				controller:'AwardController'
			})
			.state('awardnominations',{
				url : '/awards/:awardId/nominations',
				templateUrl : '/admin/scripts/app/award/nominationlist.html',	
	            params:{
	            	'nominations':true
	            },
				controller:'AwardController'
			})
			;
	});

