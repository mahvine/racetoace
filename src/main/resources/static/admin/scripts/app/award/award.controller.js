angular
	.module("racetoace")
	.controller("AwardController",function($scope,$http,$state, $stateParams){
		$scope.awards = [];
		$scope.mainAward = {type:"GROUP",name:"",description:""};

		$scope.userGroups = [];
		$scope.listUserGroups = function(){
			$http.get("/api/userGroups")
				.success(function(data){
					$scope.userGroups = data;
				});
		};
		$scope.listUserGroups();

		$scope.formatDate = function(millis) {
			var date = new Date(millis);
			return date;
		};

		$scope.page=1;
		$scope.loadPage = function(page){
			if(page){
				$scope.page = page;
			}
			$scope.listAwards();
		}
		
		
		$scope.listAwards = function(){
			$http.get("/api/awards/all?page="+$scope.page)
				.success(function(data,status,headers){
					$scope.awards = data;
        			$scope.links = PagingUtil.parse(headers('link'));
				})
				.error(function(data){
					alert(data.message);
				});
		};
		
		$scope.getAwardById = function(awardId){
			$http.get("/api/awards/"+awardId)
			.success(function(data){
				data.startDate = $scope.formatDate(data.startDate);
				data.endDate = $scope.formatDate(data.endDate);
				$scope.mainAward = data;
			})
			.error(function(data){
				alert(data.message);
			});
		}
		

		$scope.getUserNominationsAwardById = function(awardId){
			$http.get("/api/awards/"+awardId+"/userNominations")
			.success(function(data){
				$scope.userNominations = data;
			})
			.error(function(data){
				alert(data.message);
			});
		}
		
		
		$scope.saveAward = function(){
			$http.post("/api/awards", $scope.mainAward).success(function(data){
				$state.go("awardlist");
			}).error(function(data){
				alert(data.message);
			});
		}
		
		
		$scope.init = function(){
			if($stateParams.awardId){
				$scope.getAwardById($stateParams.awardId);
				if($stateParams.nominations){
					$scope.getUserNominationsAwardById($stateParams.awardId);
				}
			}else{
				$scope.listAwards();
			}
		};
		$scope.init();
	});