angular
	.module("racetoace",["mdl",'StorageUtil',"racetoace.login","ui.router","ngFileUpload","leaflet-directive",'PagingUtil','ngAnimate'])
	.run(['$http','$rootScope','SessionService',function($http, $rootScope,SessionService,$state,$location){
		console.log("racetoace is running!");
		var session = SessionService.getSession();
		$rootScope.session = session;
		if(session){
			$http.defaults.headers.common['X-session'] = session.sessionToken;
		}
		
	}])
	.config(function($urlRouterProvider, $stateProvider,$httpProvider){
		$urlRouterProvider.otherwise('/');
		$stateProvider
			.state('home',{
				url : '/',
				templateUrl : '/admin/scripts/app/home/home.html'
			})
			.state('login',{
				url : '/login',
				templateUrl : '/admin/scripts/app/login/login-form.html',
				controller: 'SessionController'
			})
			
			
			//users
			.state('userlist',{
				url : '/userlist',
				templateUrl : '/admin/scripts/app/users/list.html',
				controller:'UserController'
			})
			
			
			;
	});

