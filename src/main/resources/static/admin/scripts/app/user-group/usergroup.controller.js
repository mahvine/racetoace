angular
	.module("racetoace")
	.controller("UserGroupController",function($scope,$http,$state,$stateParams){
		$scope.userGroups = [];
		$scope.listUserGroups = function(){
			$http.get("/api/userGroups")
				.success(function(data){
					$scope.userGroups = data;
				});
		};

		$scope.getUserGroupById = function(id){
			$http.get("/api/userGroups/"+id)
				.success(function(data){
					$scope.mainUserGroup = data;
				});
		};
		
		
		
		$scope.mainUserGroup = {};

		$scope.members = [];
		
		$scope.saveUserGroup = function(){
			$http.post("/api/userGroups",$scope.mainUserGroup)
				.success(function(data){
					$state.go("usergroups")
				})
				.error(function(data){
					alert(data.message);
				});
		};
		
		
		$scope.listMainUserGroupMembers = function(userGroupId){
			$http.get("/api/userGroups/"+userGroupId+"/members").success(function(data){
				$scope.members = data;
			});
		};

		$scope.init = function(){
			console.log("stateParams");
			console.log($stateParams);
			if($stateParams.userGroupId){
				$scope.getUserGroupById($stateParams.userGroupId);
				if($stateParams.getmembers){
					$scope.listMainUserGroupMembers($stateParams.userGroupId);
				}
			}else{
				$scope.listUserGroups();
			}
		};
		$scope.init();
	});