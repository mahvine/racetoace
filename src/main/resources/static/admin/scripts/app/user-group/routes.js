angular
	.module("racetoace")
	.config(function($urlRouterProvider, $stateProvider,$httpProvider){
		$stateProvider
	
			//user groups
			.state('usergroups',{
				url : '/usergroups',
				templateUrl : '/admin/scripts/app/user-group/list.html',
				controller:'UserGroupController'
			})
			.state('newusergroup',{
	            url : '/newusergroup',
				templateUrl : '/admin/scripts/app/user-group/usergroup-form.html',
				controller:'UserGroupController'
			})
			.state('editusergroup',{
				url : '/editusergroup/:userGroupId',
				templateUrl : '/admin/scripts/app/user-group/usergroup-form.html',
				controller:'UserGroupController'
			})
			.state('usergroupmembers',{
	            url : '/usergroups/:userGroupId/members',
	            params:{
	            	'getmembers':true
	            },
				templateUrl : '/admin/scripts/app/user-group/memberlist.html',
				controller:'UserGroupController'
			})
			
			;
	});

