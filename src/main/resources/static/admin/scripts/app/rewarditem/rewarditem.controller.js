angular
	.module("racetoace")
	.controller("RewardItemController",function($scope,$http,$state, $stateParams, Upload, PagingUtil){
		$scope.rewardItems = [];
		$scope.mainRewardItem = {resourcePath:"/admin/assets/images/src_item_default.png"};
		

		$scope.page=1;
		$scope.loadPage = function(page){
			if(page){
				$scope.page = page;
			}
			$scope.listRewardItems();
		}
		
		$scope.listRewardItems = function(){
			$http.get("/api/items?page="+$scope.page)
				.success(function(data,status,headers){
					$scope.rewardItems = data;
        			$scope.links = PagingUtil.parse(headers('link'));
				})
				.error(function(data){
					alert(data.message);
				});
		};
		
		$scope.getRewardItemById = function(challengeId){
			$http.get("/api/items/"+challengeId)
			.success(function(data){
				$scope.mainRewardItem = data;
			})
			.error(function(data){
				alert(data.message);
			});
		}
		
		$scope.save = function(){
			$http.post("/api/items", $scope.mainRewardItem).success(function(data){
				$state.go("rewarditems");
			}).error(function(data){
				alert(data.message);
			});
		}
		

		$scope.claim = function(){
			$http.get("/api/rewards/confirm/"+$scope.redemptionCode).success(function(data){
				alert("Valid redemption code.");
				$state.go("rewarditems");
			}).error(function(data){
				alert(data.message);
			});
		}

		$scope.upload = function (file) {
			console.log(file);
	        Upload.upload({
	            url: '/resources',
	            file:file
	        }).progress(function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            if(evt.config){
	            	if(evt.config.file)
	            	console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	            }
	        }).success(function (data, status, headers, config) {
	            console.log('file ' + config.file.name + 'uploaded. Response: ');
	            console.log(data);
	            $scope.mainRewardItem.resourcePath="/resources/"+data.data.resourcePath;
	            /*pollItem.imageFile=data.data.resourcePath;*/
	        }).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	        })
	    };
		
		$scope.init = function(){
			if($stateParams.rewardItemId){
				$scope.getRewardItemById($stateParams.rewardItemId);
			}else{
				$scope.listRewardItems();
			}
		};
		$scope.init();
	});