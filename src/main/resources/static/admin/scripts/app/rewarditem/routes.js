angular
	.module("racetoace")
	.config(function($urlRouterProvider, $stateProvider,$httpProvider){
		$stateProvider
	
			//reward items
			.state('rewarditems',{
				url : '/rewarditems',
				templateUrl : '/admin/scripts/app/rewarditem/rewarditem-list.html',
				controller:'RewardItemController'
			})
			.state('newrewarditem',{
	            url : '/newrewarditem',
				templateUrl : '/admin/scripts/app/rewarditem/rewarditem-form.html',
				controller:'RewardItemController'
			})
			.state('editrewarditem',{
				url : '/editrewarditem/:rewardItemId',
				templateUrl : '/admin/scripts/app/rewarditem/rewarditem-form.html',
				controller:'RewardItemController'
			})
			
			.state('claimrewarditem',{
				url : '/claimrewarditem/:rewardItemId',
				templateUrl : '/admin/scripts/app/rewarditem/claim-reward-form.html',
				controller:'RewardItemController'
			})
			
			;
	});

