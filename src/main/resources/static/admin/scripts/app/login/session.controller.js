angular
	.module("racetoace.login",['StorageUtil',"ui.router"])
	.run(['$http','$rootScope','SessionService','$location','$state',function($http, $rootScope,SessionService, $location,$state){
		console.log("Checking session.");
		var session = SessionService.getSession();

		if(session){
				console.log("session found:");
				console.log(session);
				var path = $location.path();
				console.log(path);
				if(path.indexOf("login")>-1){
					$location.path("/");
				}
		}else{
			SessionService.clear();
			console.log("no session found.");
			$location.path("/login");
		}
	}])
	.controller("SessionController",function($http,$scope,SessionService, $state,$location,$window){
		$scope.loginRequest = {};
		$scope.user = SessionService.getSession();
		$scope.login = function(){
			$http.post("/api/login",$scope.loginRequest)
			.success(function(data){
				SessionService.save(data);
				$window.location.reload();
				
			}).error(function(data){
				alert(data.message);
			});
		};
		
		$scope.checkSession = function(){
			if(SessionService.getSession()){
				$http.get("/api/session").success(function(data){
					if(data){
						$scope.user = data;
						SessionService.save(data);
					}else{
						$scope.user = false;
						SessionService.clear();
						$state.go('login');
					}
				}).error(function(){
					$scope.logout();
				});
			}
		};
		$scope.checkSession();
		$scope.logout = function(){
			SessionService.clear();
			$scope.user = false;
			location.reload();
		};
		
	});

