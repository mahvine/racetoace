angular
	.module("racetoace")
	.config(function($urlRouterProvider, $stateProvider,$httpProvider){
		$stateProvider
			//Challenge

			.state('challengelist',{
				url : '/challengelist',
				templateUrl : '/admin/scripts/app/challenge/challenge-list.html',
				controller: 'ChallengeController'
			})
			.state('newchallenge',{
				url : '/newchallenge',
				templateUrl : '/admin/scripts/app/challenge/challenge-form.html',
				controller: 'ChallengeController',
			    onEnter: ["$state", function($state) {
			    	
			    }]
			})
			.state('editchallenge',{
				url : '/editchallenge/:challengeId',
				templateUrl : '/admin/scripts/app/challenge/challenge-form.html',
				controller: 'ChallengeController'
			})
			
			.state('challengeaccepted',{
				url : '/challengeaccepted/:challengeId',
				templateUrl : '/admin/scripts/app/challenge/acceptedlist.html',
				controller: 'ChallengeController',
				params:{
					accepted:true
				}
			})
		
			;
	});

