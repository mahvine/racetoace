angular
	.module("racetoace")
	.controller("ChallengeController",function($scope,$http,$state, $stateParams, Upload, PagingUtil){
		$scope.challenges = [];
		$scope.mainChallenge = {tasks:[]};
		
		$scope.longitude = 121.018497;
		$scope.latitude = 14.559109;
		
		$scope.center = {lng:121.018497, lat:14.559109,zoom:17};
		$scope.formatDate = function(millis) {
			var date = new Date(millis);
			return date;
		};
		
		$scope.page=1;
		$scope.loadPage = function(page){
			if(page){
				$scope.page = page;
			}
			$scope.listChallenges();
		}
		
		$scope.listChallenges = function(){
			$scope.links = false;
			$scope.challenges = [];
			$http.get("/api/challenges?page="+$scope.page)
				.success(function(response,status,headers){
					$scope.challenges = response;
        			$scope.links = PagingUtil.parse(headers('link'));
				})
				.error(function(data){
					alert(data.message);
				});
		};
		
		$scope.getChallengeById = function(challengeId){
			$http.get("/api/challenges/"+challengeId)
			.success(function(data){
				data.startDate = $scope.formatDate(data.startDate);
				data.endDate = $scope.formatDate(data.endDate);
				$scope.mainChallenge = data;
			})
			.error(function(data){
				alert(data.message);
			});
		}
		
		
		$scope.addTask = function(){
			$scope.mainChallenge.tasks.push({
				title:"",
				description:"",
           		type:"SIMPLE",
           		selections:
           			[
           			 {
           				 choice:"A",
           			 },
           			 {
           				 choice:"B",
           			 },

           			 {
           				 choice:"C",
           			 }
           			],
	       		longitude: $scope.longitude,
	       		latitude: $scope.latitude,
	       		radius: 20
           	});
		};

		$scope.addTask();
		
		$scope.removeTask = function(task){
			$scope.mainChallenge.tasks.splice($scope.mainChallenge.tasks.indexOf(task),1);
		}
		
		$scope.addSelection = function(task){
			task.selections.push({
           				 choice:"",
           				 text:""
           			 });
		}
		
		$scope.removeSelection = function(task,selection){
			console.log(task);
			task.selections.splice(task.selections.indexOf(selection),1);
		}
		
		$scope.saveChallenge = function(){
			$http.post("/api/challenges", $scope.mainChallenge).success(function(data){
				$state.go("challengelist");
			}).error(function(data){
				alert(data.message);
			});
		}
		
		
		/**
		 * location tasks
		 */

		if (navigator.geolocation) {
	        navigator.geolocation.getCurrentPosition(function (position) {
	                $scope.latitude = position.coords.latitude; 
	                $scope.longitude = position.coords.longitude;
	                console.log($scope.longitude+" "+$scope.latitude);
	        });
	    }else{
	    	
	    }
		

		$scope.upload = function (file,task) {
			console.log(file);
	        Upload.upload({
	            url: '/resources',
	            file:file
	        }).progress(function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            if(evt.config){
	            	if(evt.config.file)
	            	console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	            }
	        }).success(function (data, status, headers, config) {
	            console.log('file ' + config.file.name + 'uploaded. Response: ');
	            console.log(data);
//	            $scope.mainRewardItem.resourcePath="/resources/"+data.data.resourcePath;
	            task.image="/resources/"+data.data.resourcePath;
	            /*pollItem.imageFile=data.data.resourcePath;*/
	        }).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	        })
	    };
		
		
		$scope.mainTask = false;
		$scope.pollTask = false;
		$scope.viewResults = function(task){
			$http.get("/api/polls/"+task.id+"/results").success(function(response){
				console.log(response);
				$scope.pollTask = task;
				for(var i in response){
					var result = response[i];
					for(var j in $scope.pollTask.selections){
						var selection = $scope.pollTask.selections[j];
						if(selection.choice == result.choice){
							selection.votes = result.votes;
						}
					}
				}
			});
		}
		
		$scope.markers = {
            marker1: {
                lat: 51.505,
                lng: -0.09,
                focus: true,
                draggable: true,
                label: {
                    message: "Place me anywhere",
                    options: {
                        noHide: true
                    }
                }
            }
		}
		$scope.editLocation = function(task){
			console.log(task);
			$scope.center.lat = task.latitude;
			$scope.center.lng = task.longitude;
			$scope.markers.marker1.lat = task.latitude;
			$scope.markers.marker1.lng = task.longitude;
			
			$scope.mainTask = task;
			console.log($scope.markers);
		};

		$scope.$on('leafletDirectiveMarker.click', function(event){
	        $scope.eventDetected = "Drag";
	        console.log(event);
	    });

		$scope.$on('leafletDirectiveMarker.dragend', function(event, args){
	        console.log("Dragend");
	        var leafEvent = args.leafletEvent;
	        console.log(leafEvent);
	        $scope.mainTask.latitude =  leafEvent.target._latlng.lat;
	        $scope.mainTask.longitude =  leafEvent.target._latlng.lng;
	    });
		
		$scope.saveLocation = function(){

			console.log($scope.markers);
			
			var taskIndex = $scope.mainChallenge.tasks.indexOf($scope.mainTask);
			console.log("Save location of taskIndex:"+taskIndex);
			$scope.mainChallenge.tasks[taskIndex].longitude = $scope.mainTask.longitude;
			$scope.mainChallenge.tasks[taskIndex].latitude = $scope.mainTask.latitude;
			
			$scope.mainTask = false;
		}
		
		//USER CHALLENGES
		$scope.userChallenges = [];
		$scope.listUserChallenges = function(challengeId, status){
			var query = "/api/challenges/"+challengeId+"/users";
			if(status){
				query+="?status="+status;
			}
			console.log(query);
			$http.get(query).success(function(response){
				console.log(response);
				$scope.userChallenges = response;

			}).error(function(response){
				alert(response.message);
			});
		}
		
		
		$scope.init = function(){
			if($stateParams.challengeId){
				$scope.getChallengeById($stateParams.challengeId);
				console.log($stateParams);
				if($stateParams.accepted){
					console.log("getting list of userChallenges");
					$scope.listUserChallenges($stateParams.challengeId);
				}
			}else{
				$scope.loadPage();
			}
		};
		$scope.init();
	});