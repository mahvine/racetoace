angular
	.module("racetoace")
	.controller("SurveyController",function($scope,$http,$state, $stateParams, Upload, PagingUtil){
		$scope.surveys = [];
		$scope.mainSurvey = {title:"", questions:[{text:""}]};
		

		$scope.page=1;
		$scope.loadPage = function(page){
			if(page){
				$scope.page = page;
			}
			$scope.listSurveys();
		}
		$scope.formatDate = function(millis) {
			var date = new Date(millis);
			return date;
		};
		$scope.listSurveys = function(){
			$http.get("/api/surveys?page="+$scope.page)
				.success(function(data,status,headers){
					$scope.surveys = data;
        			$scope.links = PagingUtil.parse(headers('link'));
				})
				.error(function(data){
					alert(data.message);
				});
		};
		
		$scope.getSurveyById = function(id){
			$http.get("/api/surveys/"+id)
				.success(function(data,status,headers){
					$scope.mainSurvey = data;
					$scope.mainSurvey.startDate = $scope.formatDate($scope.mainSurvey.startDate);
					$scope.mainSurvey.endDate = $scope.formatDate($scope.mainSurvey.endDate);
				})
				.error(function(data){
					alert(data.message);
				});
		}
		
		$scope.save = function(){
			$http.post("/api/surveys", $scope.mainSurvey).success(function(data){
				$state.go("surveylist");
			}).error(function(data){
				alert(data.message);
			});
		}

		$scope.addQuestion = function(){
			$scope.mainSurvey.questions.push({text:""});
		}

		$scope.removeQuestion = function(question){
			$scope.mainSurvey.questions.splice($scope.mainSurvey.questions.indexOf(question),1);
		}
		
		$scope.init = function(){
			if($stateParams.surveyId){
				$scope.getSurveyById($stateParams.surveyId);
			}else{
				$scope.listSurveys();
			}
		};
		$scope.init();
	});