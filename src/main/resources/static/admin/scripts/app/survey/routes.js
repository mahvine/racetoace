angular
	.module("racetoace")
	.config(function($urlRouterProvider, $stateProvider,$httpProvider){
		$stateProvider
			//surveys
			.state('surveylist',{
				url : '/surveylist',
				templateUrl : '/admin/scripts/app/survey/survey-list.html',
				controller:'SurveyController'
			})
			.state('newsurvey',{
                url : '/newsurvey',
				templateUrl : '/admin/scripts/app/survey/survey-form.html',
				controller:'SurveyController'
			})
			.state('editsurvey',{
				url : '/editsurvey/:surveyId',
				templateUrl : '/admin/scripts/app/survey/survey-form.html',
				controller:'SurveyController'
			})
			;
	});

