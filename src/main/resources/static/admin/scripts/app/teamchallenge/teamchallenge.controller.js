angular
	.module("racetoace")
	.controller("TeamChallengeController",function($scope,$http,$state, $stateParams, Upload, PagingUtil){
		$scope.challenges = [];
		$scope.mainChallenge = {};
		$scope.userTeams = [];
		$scope.formatDate = function(millis) {
			var date = new Date(millis);
			return date;
		};
		
		$scope.page=1;
		$scope.loadPage = function(page){
			if(page){
				$scope.page = page;
			}
			$scope.listChallenges();
		}
		
		$scope.listChallenges = function(){
			$scope.links = false;
			$scope.challenges = [];
			$http.get("/api/teamChallenges?page="+$scope.page)
				.success(function(response,status,headers){
					$scope.challenges = response;
        			$scope.links = PagingUtil.parse(headers('link'));
				})
				.error(function(data){
					alert(data.message);
				});
		};
		
		$scope.getChallengeById = function(challengeId){
			$http.get("/api/teamChallenges/"+challengeId)
			.success(function(data){
				data.startDate = $scope.formatDate(data.startDate);
				data.endDate = $scope.formatDate(data.endDate);
				$scope.mainChallenge = data;
			})
			.error(function(data){
				alert(data.message);
			});
			$http.get("/api/teamChallenges/"+challengeId+"/teams")
				.success(function(data){
					$scope.userTeams = data;
				});
		}
		
		
		$scope.removeTask = function(task){
			$scope.mainChallenge.tasks.splice($scope.mainChallenge.tasks.indexOf(task),1);
		}
		
		$scope.addSelection = function(task){
			task.selections.push({
           				 choice:"",
           				 text:""
           			 });
		}
		
		$scope.removeSelection = function(task,selection){
			console.log(task);
			task.selections.splice(task.selections.indexOf(selection),1);
		}
		
		$scope.saveChallenge = function(){
			$http.post("/api/teamChallenges", $scope.mainChallenge).success(function(data){
				$state.go("teamchallengelist");
			}).error(function(data){
				alert(data.message);
			});
		}
		
		
		/**
		 * location tasks
		 */

		$scope.upload = function (file,task) {
			console.log(file);
	        Upload.upload({
	            url: '/resources',
	            file:file
	        }).progress(function (evt) {
	            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
	            if(evt.config){
	            	if(evt.config.file)
	            	console.log('progress: ' + progressPercentage + '% ' + evt.config.file.name);
	            }
	        }).success(function (data, status, headers, config) {
	            console.log('file ' + config.file.name + 'uploaded. Response: ');
	            console.log(data);
//	            $scope.mainRewardItem.resourcePath="/resources/"+data.data.resourcePath;
	            task.image="/resources/"+data.data.resourcePath;
	            /*pollItem.imageFile=data.data.resourcePath;*/
	        }).error(function (data, status, headers, config) {
	            console.log('error status: ' + status);
	        })
	    };
		
		
		//USER CHALLENGES
		$scope.userChallenges = [];
		$scope.listUserChallenges = function(challengeId, status){
			var query = "/api/challenges/"+challengeId+"/users";
			if(status){
				query+="?status="+status;
			}
			console.log(query);
			$http.get(query).success(function(response){
				console.log(response);
				$scope.userChallenges = response;

			}).error(function(response){
				alert(response.message);
			});
		}
		
		$scope.setTeam = function(userTeam){
			$scope.mainUserTeam = userTeam;
			$scope.viewMembers = true;
		}
		
		$scope.init = function(){
			if($stateParams.teamChallengeId){
				$scope.getChallengeById($stateParams.teamChallengeId);
				if($stateParams.accepted){
					console.log("getting list of userChallenges");
					$scope.listUserChallenges($stateParams.challengeId);
				}
			}else{
				$scope.loadPage();
			}
		};
		$scope.init();
	});