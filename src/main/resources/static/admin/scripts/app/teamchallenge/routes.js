angular
	.module("racetoace")
	.config(function($urlRouterProvider, $stateProvider,$httpProvider){
		$stateProvider
			//TeamChallenge

			.state('teamchallengelist',{
				url : '/teamchallenges',
				templateUrl : '/admin/scripts/app/teamchallenge/teamchallenge-list.html',
				controller: 'TeamChallengeController'
			})
			.state('newteamchallenge',{
				url : '/teamchallenges/new',
				templateUrl : '/admin/scripts/app/teamchallenge/teamchallenge-form.html',
				controller: 'TeamChallengeController',
			    onEnter: ["$state", function($state) {
			    	
			    }]
			})
			.state('editteamchallenge',{
				url : '/teamchallenges/:teamChallengeId',
				templateUrl : '/admin/scripts/app/teamchallenge/teamchallenge-form.html',
				controller: 'TeamChallengeController'
			})
			;
	});

