angular
	.module("racetoace")
	.controller("UserController",function($scope,$http,$state,$stateParams,PagingUtil, $timeout){
		$scope.users = [];
		$scope.page = 1;
		$scope.listUsers = function(){
			$http.get("/api/users?page="+$scope.page)
				.success(function(data,status,headers){
					$scope.users = data;
        			$scope.links = PagingUtil.parse(headers('link'));
				});
		};

		$scope.listUserGroups = function(){
			$http.get("/api/userGroups")
				.success(function(data){
					$scope.userGroups = data;
				});
		};
		
		$scope.loadPage = function(page){
			if(page){
				$scope.page = page;
			}
			$scope.listUsers();
		}

		$scope.setUser = function(user){
			$scope.mainUser = user;
		}
		$scope.saveUser = function(user){
//			console.log(user);
			$http.put("/api/users/"+user.id,user).success(function(response){
				$scope.loadPage();
			});
		};
		
		$scope.init = function(){
			$scope.listUsers();
		};

		$scope.listUserGroups();
		$scope.showGroups = function(){
            var el = document.getElementById('menuButton');
            el.click();
		}
		
		$scope.saveUserGroup = function(userGroup){
			$scope.mainUser.userGroup = userGroup;
			$scope.saveUser($scope.mainUser);
			$scope.mainUser = null;
		}
		$scope.init();
	});