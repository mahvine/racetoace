/**
 * 
 */
package ph.com.smart.racetoace.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author JRDomingo
 * @since Aug 26, 2015 2:07:29 PM
 * 
 */

@Configuration
@ConfigurationProperties(prefix = "app")
public class AppConfig {
	private static String baseUrl;

	public static String getBaseUrl() {
		return baseUrl;
	}

	public static void setBaseUrl(String baseUrl) {
		AppConfig.baseUrl = baseUrl;
	}
}
