/**
 * 
 */
package ph.com.smart.racetoace.web.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.UserGroup;
import ph.com.smart.racetoace.rnr.repository.UserGroupRepository;
import ph.com.smart.racetoace.web.rest.dto.UserProfileDTO;
import ph.com.smart.racetoace.web.rest.dto.mapper.UserMapper;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 4:35:46 PM
 * 
 */
@RestController
@RequestMapping("/api")
public class UserGroupResource {

	@Inject
	UserGroupRepository userGroupRepository;

	@Inject
	UserResource userResource;
	
	@Inject
	UserMapper userMapper;
	
	@RequestMapping(value="/userGroups", method=RequestMethod.POST)
	public void save(@RequestBody UserGroup userGroup){
		userGroupRepository.save(userGroup);
	}
	
	@RequestMapping(value="/userGroups", method=RequestMethod.GET)
	public List<UserGroup> listUserGroups(@RequestParam(value="direction", required=false, defaultValue="ASC") Direction direction,
			@RequestParam(value="sort", required=false, defaultValue="name") String sort ){
		
		return userGroupRepository.findAll(new Sort(direction, sort));
	}

	@RequestMapping(value="/userGroups/{id}", method=RequestMethod.GET)
	public UserGroup getUserGroupById(@PathVariable("id") Long id){
		UserGroup userGroup = userGroupRepository.findOne(id);
		return userGroup;
	}
	
	@RequestMapping(value="/userGroups/{id}/members", method=RequestMethod.GET)
	public List<UserProfileDTO> listUserGroupMembers(@PathVariable("id") Long id){
		UserGroup userGroup = userGroupRepository.findOne(id);
		List<UserProfileDTO> userSessionDTOs = UserMapper.usersToUserProfileDTOs(userGroup.users);
		return userSessionDTOs;
	}

	@RequestMapping(value = "/userGroups/my/members", method = RequestMethod.GET)
	public List<UserProfileDTO> listMyGroupMembers(@RequestHeader("X-session") String stringToken) {
		User user = userResource.getUser(stringToken);
		UserGroup userGroup = userGroupRepository.findOne(user.userGroup.id);
		userGroup.users.remove(user);
		return UserMapper.usersToUserProfileDTOs(userGroup.users);
	}
	
	
}
