/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

/**
 * @author JRDomingo
 * @since Sep 2, 2015 8:38:39 AM
 * 
 */
public class ChangePasswordDTO {
	
	public String oldPassword;
	public String newPassword;

}
