package ph.com.smart.racetoace.web.rest.dto;

import java.util.ArrayList;
import java.util.List;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.UserTeam;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class UserTeamDTO {
	
	public Long id;
	
	public String name;
	
	public String code;
	
	public boolean isMember;
	
	public boolean completed;
	
	public List<UserProfileDTO> members = new ArrayList<UserProfileDTO>();
	
	public UserTeamDTO(){
		
	}
	
	public UserTeamDTO(UserTeam userTeam,User userRequestor){
		id=userTeam.id;
		name=userTeam.name;
		completed = userTeam.winner;
		if(userTeam.members.contains(userRequestor)){
			code = userTeam.code;
			isMember = true;
		}
		for(User user :userTeam.members){
			members.add(new UserProfileDTO(user));
		}
	}
	
	
}
