/**
 *
 */
package ph.com.smart.racetoace.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiParam;
import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.rnr.model.Award;
import ph.com.smart.racetoace.rnr.model.Award.Status;
import ph.com.smart.racetoace.rnr.model.Nomination;
import ph.com.smart.racetoace.rnr.model.UserGroup;
import ph.com.smart.racetoace.rnr.repository.AwardRepository;
import ph.com.smart.racetoace.rnr.repository.NominationRepository;
import ph.com.smart.racetoace.rnr.repository.UserGroupRepository;
import ph.com.smart.racetoace.rnr.service.AwardService;
import ph.com.smart.racetoace.web.rest.dto.AwardDTO;
import ph.com.smart.racetoace.web.rest.dto.NominateUserDTO;
import ph.com.smart.racetoace.web.rest.dto.NominatedUserWithProfileDTO;
import ph.com.smart.racetoace.web.rest.dto.UserNominationCountDTO;
import ph.com.smart.racetoace.web.rest.dto.UserProfileDTO;
import ph.com.smart.racetoace.web.rest.util.PaginationUtil;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 1:33:46 PM
 */
@Api
@RestController
@RequestMapping("/api")
public class AwardResource {

	@Inject
	AwardRepository awardRepository;

	@Inject
	UserResource userResource;

	@Inject
	UserRepository userRepository;

	@Inject
	UserGroupRepository userGroupRepository;

	@Inject
	NominationRepository nominationRepository;

	@Inject
	AwardService awardService;

	@RequestMapping(value = "/awards", method = RequestMethod.POST)
	public void save(@RequestBody Award award) {
		UserGroup userGroup = null;
		if (award.userGroup != null) {
			userGroup = userGroupRepository.findOne(award.userGroup.id);
			award.userGroup = userGroup;
		}
		if (award.endDate != null) {
			if (award.endDate.getTime() > new Date().getTime()) {
				award.status = Status.OPEN;
			}
		}
		awardRepository.save(award);

	}

	/**
	 * list all created awards
	 * 
	 * @param page
	 * @param size
	 * @return
	 */
	@RequestMapping(value = "/awards/all", method = RequestMethod.GET)
	public ResponseEntity<List<AwardDTO>> listAwards(
			@ApiParam("page") @RequestParam(value = "page", required = false, defaultValue = "0") int page,
			@RequestParam(value = "size", required = false, defaultValue = "20") int size,
			@RequestParam(value = "sort", defaultValue = "id") String sortBy,
			@RequestParam(value = "direction", defaultValue = "DESC") Direction direction)
			throws URISyntaxException {
		Sort sort = new Sort(direction,sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);

		Page<Award> pageAwards = awardRepository.findAll(pageRequest);
		List<AwardDTO> awards = new ArrayList<AwardDTO>();
		for (Award award : pageAwards.getContent()) {
			awards.add(new AwardDTO(award));
		}
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageAwards, "/api/awards/all", page, size);
		return new ResponseEntity<List<AwardDTO>>(awards,headers,HttpStatus.OK);
	}

	@RequestMapping(value = "/awards/{id}", method = RequestMethod.GET)
	public AwardDTO getAwardById(@PathVariable("id") Long id,
			@RequestHeader("X-session") String sessionToken) {
		User user = userResource.getUser(sessionToken);
		Award award = awardRepository.findOne(id);
		return new AwardDTO(award,
				nominationRepository.findByNominatorAndAward(user, award));
	}

	// TODO must change to userAwards
	@RequestMapping(value = "/awards", method = RequestMethod.GET)
	public List<AwardDTO> listAwards(
			@ApiParam("page") @RequestParam(value = "page", required = false, defaultValue = "0") int page,
			@RequestParam(value = "size", required = false, defaultValue = "20") int size,
			@RequestParam(value = "status") Award.Status status,
			@RequestHeader("X-session") String sessionToken,
			@RequestParam(value = "sort", required = false, defaultValue = "endDate") String sortBy,
			@RequestParam(value = "direction", required = false, defaultValue = "ASC") Direction direction) {
		User user = userResource.getUser(sessionToken);
		// PageRequest pageRequest = new PageRequest(page, size);
		Sort sort = new Sort(new Order(direction, sortBy));
		Specification<Award> filter = filter(status, new Date(), user.userGroup);
		List<Award> awards = awardRepository.findAll(filter, sort);
		List<AwardDTO> awardsDto = new LinkedList<>();
		for (Award a : awards) { // TODO must be remove in loop
			awardsDto.add(new AwardDTO(a, nominationRepository
					.findByNominatorAndAward(user, a)));
		}
		return awardsDto;
	}

	@RequestMapping(value = "/awards/{id}/nominate", method = RequestMethod.POST)
	public void nominate(@PathVariable("id") Long awardId,
			@RequestBody NominateUserDTO nominationDTO,
			@RequestHeader("X-session") String sessionToken) {
		User nominator = userResource.getUser(sessionToken);
		awardService.nominate(nominator, awardId, nominationDTO.nominated,
				nominationDTO.reason);
	}

	@RequestMapping(value = "/awards/{id}/nominate", method = RequestMethod.GET)
	public NominatedUserWithProfileDTO getNomination(
			@PathVariable("id") Long awardId,
			@RequestHeader("X-session") String sessionToken) {
		Nomination nomination = nominationRepository.findByNominatorAndAward(
				userResource.getUser(sessionToken),
				awardRepository.findOne(awardId));
		return nomination == null ? null : new NominatedUserWithProfileDTO(
				nomination);
	}

	@RequestMapping(value = "/awards/{id}/userNominations", method = RequestMethod.GET)
	public List<UserNominationCountDTO> getNominationCount(
			@PathVariable("id") Long awardId) {
		List<Object[]> rawResults = nominationRepository.countUserNominationsPerAward(awardId);
		List<UserNominationCountDTO> nominationCounts = new ArrayList<UserNominationCountDTO>();
		for (Object[] result : rawResults) {
			User user = (User) result[0];
			Long count = (Long) result[1];
			UserNominationCountDTO userNominationCountDTO = new UserNominationCountDTO();
			userNominationCountDTO.count = count;
			userNominationCountDTO.user = new UserProfileDTO(user);
			nominationCounts.add(userNominationCountDTO);
		}

		return nominationCounts;
	}

	public static Specification<Award> filter(final Award.Status status,
			final Date date, final UserGroup userGroup) {
		return new Specification<Award>() {

			@Override
			public Predicate toPredicate(Root<Award> root,
					CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				final List<Predicate> predicates = new ArrayList<Predicate>();
				if (Award.Status.OPEN.equals(status)) {
					Predicate greaterThanEqualDate = criteriaBuilder
							.greaterThanOrEqualTo(root.<Date> get("endDate"),
									date);
					predicates.add(greaterThanEqualDate);
				} else {
					Predicate lessThanDate = criteriaBuilder.lessThan(
							root.<Date> get("endDate"), date);
					predicates.add(lessThanDate);
				}

				if (userGroup != null) {
					Predicate userGroupPredicate = criteriaBuilder.equal(
							root.<UserGroup> get("userGroup"), userGroup);
					predicates.add(userGroupPredicate);
				}

				return criteriaBuilder.and(predicates
						.toArray(new Predicate[predicates.size()]));
			}
		};
	}

}
