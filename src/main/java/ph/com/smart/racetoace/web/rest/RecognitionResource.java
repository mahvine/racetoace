/**
 *
 */
package ph.com.smart.racetoace.web.rest;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.Recognition;
import ph.com.smart.racetoace.rnr.repository.RecognitionRepository;
import ph.com.smart.racetoace.rnr.service.PointService;
import ph.com.smart.racetoace.rnr.service.RecognitionService;
import ph.com.smart.racetoace.web.rest.dto.RecognitionDTO;
import ph.com.smart.racetoace.web.rest.filter.RecognitionFilter;
import ph.com.smart.racetoace.web.rest.util.PaginationUtil;

/**
 * @author Jrdomingo
 * Jan 11, 2016 10:56:07 AM
 */
@RestController
@RequestMapping("/api")
public class RecognitionResource {
	
	@Inject
	RecognitionRepository recognitionRepository;
	
	@Inject
	RecognitionService recognitionService;
	
	@Inject
	UserResource userResource;
	
	@RequestMapping(value="/recognitions",method=RequestMethod.POST)
	public void save(@RequestBody RecognitionDTO request, @RequestHeader("X-session") String sessionToken){
		User user = userResource.getUser(sessionToken);
		recognitionService.recognize(request.userId, request.title, request.description, user.id, request.rewardPoints);
	}
	

	@RequestMapping(value="/recognitions", method=RequestMethod.GET)
	public ResponseEntity<List<RecognitionDTO>> list(@RequestParam(value="page",required=false, defaultValue="1") int page,
			@RequestParam(value="size",required=false, defaultValue="20") int size, @RequestParam(value="sort", defaultValue="id") String sortBy,
			@RequestParam(value="direction", defaultValue="DESC") Direction direction,
			@RequestParam(value="idBefore", required = false) Long idBefore,
			@RequestParam(value="idAfter", required = false) Long idAfter,
			HttpServletRequest request) throws URISyntaxException{
		Sort sort = new Sort(direction,sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<Recognition> paged = recognitionRepository.findAll(new RecognitionFilter(request), pageRequest);
		List<RecognitionDTO> list = new ArrayList<RecognitionDTO>();
		for(Recognition recognition:paged.getContent()){
			list.add(new RecognitionDTO(recognition));
		}
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(paged, "/api/recognition", page, size);	
		return new ResponseEntity<List<RecognitionDTO>>(list,headers,HttpStatus.OK);
	}

	@RequestMapping(value="/recognitions/{id}",method=RequestMethod.GET)
	public RecognitionDTO getById(@PathVariable("id")Long id){
		return new RecognitionDTO(recognitionRepository.findOne(id));
	}
	@RequestMapping(value="/recognitions/{id}",method=RequestMethod.DELETE)
	public void delete(@PathVariable("id")Long id){
		recognitionRepository.delete(id);
	}
	

}
