package ph.com.smart.racetoace.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import ph.com.smart.racetoace.notification.PushGateway;
import ph.com.smart.racetoace.reports.UserSurveyExcelBuilder;
import ph.com.smart.racetoace.rnr.model.Survey;
import ph.com.smart.racetoace.rnr.model.SurveyQuestion;
import ph.com.smart.racetoace.rnr.model.UserSurvey;
import ph.com.smart.racetoace.rnr.repository.SurveyAnswerRepository;
import ph.com.smart.racetoace.rnr.repository.SurveyQuestionRepository;
import ph.com.smart.racetoace.rnr.repository.SurveyRepository;
import ph.com.smart.racetoace.rnr.repository.UserSurveyRepository;
import ph.com.smart.racetoace.web.rest.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class SurveyResource {
	
	private static final Logger logger = LoggerFactory.getLogger(SurveyResource.class);
	@Inject
	SurveyRepository surveyRepository;
	
	@Inject
	SurveyQuestionRepository surveyQuestionRepository;
	
	@Inject 
	SurveyAnswerRepository surveyAnswerRepository;
	
	@Inject
	PushGateway pushGateway;
	
	@Inject
	UserSurveyRepository userSurveyRepository;
	
	@RequestMapping(value="/surveys", method=RequestMethod.POST)
	public void save(@RequestBody Survey survey){
		boolean surveyNew = (survey.id == null);
		surveyQuestionRepository.save(survey.questions);
		surveyRepository.save(survey);
		if(surveyNew){
			pushGateway.notifyNewSurvey(survey);
		}
	}
	
	@RequestMapping(value="/surveys", method=RequestMethod.GET)
	public ResponseEntity<List<Survey>> listSurveys(@RequestParam(value="page",required=false, defaultValue="1") int page,
			@RequestParam(value="size",required=false, defaultValue="20") int size, @RequestParam(value="sort", defaultValue="id") String sortBy,
			@RequestParam(value="direction", defaultValue="DESC") Direction direction) throws URISyntaxException{
		Sort sort = new Sort(direction,sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<Survey> pageChallenges = surveyRepository.findAll(pageRequest);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageChallenges, "/api/surveys", page, size);
		return new ResponseEntity<List<Survey>>(pageChallenges.getContent(),headers,HttpStatus.OK);
	}

	@RequestMapping(value="/surveys/{id}", method=RequestMethod.GET)
	public Survey getById(@PathVariable("id") Long id){
		Survey survey = surveyRepository.findOne(id);
		for(int i = 0;i<survey.questions.size();i++){
			SurveyQuestion question = survey.questions.get(i);
			List<Object[]> rawResults = surveyAnswerRepository.getAverageAnswer(question.id);
			question.average = 0D;
			question.respondents = 0L;
			if(rawResults!=null){
				if(rawResults.size()>0){
					Object[] object = rawResults.get(0);
					if(object.length>0){
						Double average = (Double) object[1];
						Long respondents = (Long) object[2];
						if(average!=null){
							question.average = average;
						}
						if(respondents!=null){
							question.respondents = respondents;
						}
					}
				}
			}
		}
		return survey;
		
	}
	

	@RequestMapping(value="/surveys/{id}/response", method=RequestMethod.GET)
	public ModelAndView downloadSurveyResponse(@PathVariable("id") Long id, ModelAndView mav,
	        HttpServletRequest request, 
	        HttpServletResponse response){
		Survey survey = surveyRepository.findOne(id);
		List<UserSurvey> userSurveys = userSurveyRepository.findBySurvey(survey);
	    mav.setView(new UserSurveyExcelBuilder(survey,userSurveys, surveyAnswerRepository));
	    return mav;
		
	}
	

	@RequestMapping(value="/surveys/{id}", method=RequestMethod.DELETE)
	public void delete(@PathVariable("id") Long id){
		surveyRepository.delete(id);
	}
	
}
