/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto.mapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.core.model.CompletedTask;
import ph.com.smart.racetoace.core.model.Task;
import ph.com.smart.racetoace.core.model.UserChallenge;
import ph.com.smart.racetoace.core.model.UserChallenge.Status;
import ph.com.smart.racetoace.core.model.task.LocationTask;
import ph.com.smart.racetoace.core.model.task.PollTask;
import ph.com.smart.racetoace.core.model.task.QuizTask;
import ph.com.smart.racetoace.core.model.task.SimpleTask;
import ph.com.smart.racetoace.web.rest.dto.BaseTaskDTO.Type;
import ph.com.smart.racetoace.web.rest.dto.UserChallengeDTO;
import ph.com.smart.racetoace.web.rest.dto.UserChallengeDTO.TaskStatus;
import ph.com.smart.racetoace.web.rest.dto.UserChallengeDTO.UserTaskDTO;

/**
 * @author JRDomingo
 * @since Sep 2, 2015 12:59:53 PM
 * 
 */
@Component
public class UserChallengeDTOTranslator {
	private static Logger logger = LoggerFactory.getLogger(UserChallengeDTOTranslator.class);
	
	/**
	 * if Challenge has a corresponding userChallenge get STATUS ACCEPTED
	 * 
	 * @param challenges
	 * @param userChallenges
	 * @param completedTasks
	 * @return
	 */
	public static List<UserChallengeDTO> translate(List<Challenge> challenges, List<UserChallenge> userChallenges, List<CompletedTask> completedTasks, Date today, Status status){
		logger.info("translating {}-challenges, {}-userChallenges, {}-completedTasks", challenges.size(), userChallenges.size(), completedTasks.size());
		//store completedtasks in map for quick retrieval 
		Map<Long, CompletedTask> completedTaskMap = new HashMap<Long, CompletedTask>();
		for(CompletedTask completedTask: completedTasks){
			completedTaskMap.put(completedTask.task.id, completedTask);
		}
		

		//store acceptedChallenges in map for quick retrieval 
		Map<Long, UserChallenge> acceptedChallenges = new HashMap<Long, UserChallenge>();
		for(UserChallenge userChallenge: userChallenges){
			acceptedChallenges.put(userChallenge.challenge.id, userChallenge);
		}
		

		List<UserChallengeDTO> result = new ArrayList<UserChallengeDTO>();
		
		for(Challenge challenge: challenges){
			UserChallengeDTO userChallengeDTO = new UserChallengeDTO();
			
			//Challenge Details
			userChallengeDTO.id = challenge.id;
			userChallengeDTO.details = challenge.details;
			userChallengeDTO.startDate = challenge.startDate;
			userChallengeDTO.endDate = challenge.endDate;
			userChallengeDTO.title = challenge.title;
			userChallengeDTO.winnerPoints = challenge.winnerPoints;
			userChallengeDTO.consolationPoints = challenge.consolationPoints;
			userChallengeDTO.penalty = challenge.penalty;
			
			
			UserChallenge userChallenge = acceptedChallenges.get(challenge.id);
			Status challengeStatus = Status.OPEN;
			if(userChallenge != null){
				challengeStatus = userChallenge.status;
				if(userChallenge.status.equals(Status.COMPLETED)){
					if(userChallenge.winner){
						userChallengeDTO.winner = true;
					}else{
						userChallengeDTO.winner = false;
					}
				}else{
					//if accepted and ended
					Date endDate = challenge.endDate;
					if(endDate.getTime()<today.getTime()){
						challengeStatus = Status.FAILED;
					}
				}
				
				
			}else{
				Date endDate = challenge.endDate;
				if(endDate.getTime()<today.getTime()){
					challengeStatus = Status.CLOSED;
				}
				
			}
			
			//Task Details
			for(Task task: challenge.tasks){
				UserTaskDTO userTaskDTO = new UserTaskDTO();
				userTaskDTO.id = task.id;
				userTaskDTO.description = task.description;
				userTaskDTO.title = task.title;
				userTaskDTO.image = task.image;
				if(task instanceof SimpleTask){
					userTaskDTO.type = Type.SIMPLE;
				}else if(task instanceof QuizTask){
					userTaskDTO.type = Type.QUIZ;
					
				}else if(task instanceof PollTask){
					userTaskDTO.type = Type.POLL;
					PollTask pollTask = (PollTask) task;
					userTaskDTO.selections = pollTask.selections;
				}else if(task instanceof LocationTask){
					userTaskDTO.type = Type.LOCATION;
				}else{
					logger.warn("Cannot determine type of Task");
				}
				
				CompletedTask completedTask = completedTaskMap.get(task.id);
				if(completedTask!=null){
					userTaskDTO.status = TaskStatus.DONE;
				}else{
					userTaskDTO.status = TaskStatus.UNDONE;
				}
				
				userChallengeDTO.tasks.add(userTaskDTO);
			}
			
			
			userChallengeDTO.status = challengeStatus;
			
			if(challenge.startDate.getTime() <= today.getTime()){
				result.add(userChallengeDTO);
			}
			
			
			//status filtering
			if(status!=null){
				if(!userChallengeDTO.status.equals(status)){
					//remove because its not the status being filtered
					result.remove(userChallengeDTO);
				}
			}
			
			
		}
		return result;
	}
	
}
