package ph.com.smart.racetoace.web.rest.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ph.com.smart.racetoace.rnr.model.Redemption;
import ph.com.smart.racetoace.rnr.model.RewardItem;

public class RedemptionDTO {
	
	public String code;
	public Date dateClaimed;
	public Date dateCreated;
	public Long id;
	public RewardItem rewardItem;
	public UserProfileDTO user;
	
	public RedemptionDTO(){
		
	}
	public RedemptionDTO(Redemption redemption){
		this.code = redemption.code;
		this.dateClaimed = redemption.dateClaimed;
		this.dateCreated = redemption.dateCreated;
		this.id = redemption.id;
		this.rewardItem = redemption.rewardItem;
		this.user = new UserProfileDTO(redemption.user);
	}
	
	public static List<RedemptionDTO> list(List<Redemption> redemptions){
		List<RedemptionDTO> redemptionDTOs = new ArrayList<RedemptionDTO>();
		for(Redemption redemption:redemptions){
			redemptionDTOs.add(new RedemptionDTO(redemption));
		}
		return redemptionDTOs;
	}
	
}
