/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

import java.util.ArrayList;
import java.util.List;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 10:36:50 AM
 * 
 */
public class ChallengeDTO extends BaseChallengeDTO{

	public List<TaskDTO> tasks = new ArrayList<TaskDTO>();
	
}
