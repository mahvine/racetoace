package ph.com.smart.racetoace.web.rest;

import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.core.model.Task;
import ph.com.smart.racetoace.core.model.UserChallenge;
import ph.com.smart.racetoace.core.repository.ChallengeRepository;
import ph.com.smart.racetoace.core.repository.UserChallengeRepository;
import ph.com.smart.racetoace.rnr.service.TaskService;
import ph.com.smart.racetoace.web.rest.dto.AcceptedChallengeDTO;
import ph.com.smart.racetoace.web.rest.dto.ChallengeDTO;
import ph.com.smart.racetoace.web.rest.dto.mapper.ChallengeDTOMapper;
import ph.com.smart.racetoace.web.rest.dto.mapper.UserMapper;
import ph.com.smart.racetoace.web.rest.util.PaginationUtil;

@RestController
@RequestMapping("/api")
public class ChallengeResource {
	
	@Inject
	ChallengeRepository challengeRepository;
	
	@Inject
	TaskService taskService;
	
	@Inject
	ChallengeDTOMapper challengeDTOMapper;
	
	@Inject
	UserChallengeRepository userChallengeRepository;
	
	@Inject
	UserMapper userMapper;
	
	@RequestMapping(value="/challenges", method=RequestMethod.POST)
	public Map<String,Object> save(@RequestBody ChallengeDTO challengeDTO){
		List<Task> tasks = taskService.save(challengeDTO.tasks);
		Challenge challenge = challengeDTOMapper.challengeDTOToChallenge(challengeDTO);
		challenge.tasks = tasks;
		challengeRepository.save(challenge);
		Map<String,Object> returnValue = new HashMap<String,Object>();
		returnValue.put("id", challenge.id);
		return returnValue;
	}
	

	@RequestMapping(value="/challenges", method=RequestMethod.GET)
	public ResponseEntity<List<ChallengeDTO>> listChallenges(@RequestParam(value="page",required=false, defaultValue="1") int page,
			@RequestParam(value="size",required=false, defaultValue="20") int size, @RequestParam(value="sort", defaultValue="id") String sortBy,
			@RequestParam(value="direction", defaultValue="DESC") Direction direction) throws URISyntaxException{
		Sort sort = new Sort(direction,sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<Challenge> pageChallenges = challengeRepository.findAll(pageRequest);
		List<ChallengeDTO> challengeDTOs = challengeDTOMapper.challengeToChallengeDTO(pageChallenges.getContent());
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageChallenges, "/api/challenges", page, size);
		return new ResponseEntity<List<ChallengeDTO>>(challengeDTOs,headers,HttpStatus.OK);
	}
	

	@RequestMapping(value="/challenges/{id}", method=RequestMethod.GET)
	public ChallengeDTO getChallenges(@PathVariable("id") Long id){
		Challenge challenge = challengeRepository.findOne(id);
		ChallengeDTO challengeDTO = ChallengeDTOMapper.challengeToChallengeDTO(challenge);
		return challengeDTO;
		
	}
	

	@RequestMapping(value="/challenges/{id}/users", method=RequestMethod.GET)
	public List<AcceptedChallengeDTO> getChallengeAcceptedUsers(@PathVariable("id") Long id,
			@RequestParam(value="status", required=false) UserChallenge.Status status,
			@RequestParam(value="sort", required=false, defaultValue="dateCompleted") String sortBy,
			@RequestParam(value="direction", required=false, defaultValue="ASC") Direction direction){
		Challenge challenge = challengeRepository.findOne(id);
		Sort sort = new Sort(direction, sortBy);
		List<UserChallenge> userChallengeAccepted = null;
		if(status== null){
			userChallengeAccepted = userChallengeRepository.findByChallenge(challenge, sort);
		}else{
			userChallengeAccepted = userChallengeRepository.findByChallengeAndStatus(challenge, status, sort);
		}
		List<AcceptedChallengeDTO> acceptedChallengeDTOs = userMapper.userChallengesToAcceptedChallengeDTOs(userChallengeAccepted);
		return acceptedChallengeDTOs;
	}
	

}
