/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

import javax.validation.constraints.Size;

/**
 * @author JRDomingo
 * @since Aug 26, 2015 1:19:32 PM
 * 
 */
public class LoginDTO {

	public String email;
	
	public String password;
	
}
