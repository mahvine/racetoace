/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

import java.util.List;

import ph.com.smart.racetoace.core.model.task.PollTask;
import ph.com.smart.racetoace.core.model.task.PollTask.Selection;
import ph.com.smart.racetoace.core.model.task.QuizTask;
import ph.com.smart.racetoace.core.model.task.SimpleTask;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * DTO for creating a {@link SimpleTask}, {@link PollTask} and {@link QuizTask}  used for management
 * @author JRDomingo
 * @since Aug 24, 2015 10:36:56 AM
 * 
 */
@JsonInclude(Include.NON_NULL)
public class TaskDTO extends BaseTaskDTO{
	public TaskDTO(){
		
	}
	public TaskDTO(BaseTaskDTO baseTaskDTO){
		this.id = baseTaskDTO.id;
		this.description = baseTaskDTO.description;
		this.type = baseTaskDTO.type;
	}
	
	/**
	 * for location task
	 */
	public Float longitude;
	
	public Float latitude;
	
	public Integer radius;
	
	/**
	 * for poll task
	 */
	public List<Selection> selections;
	
	/**
	 * for simple task
	 */
	public String code;
	
	/**
	 * for quiz task
	 */
	public String answer;
	
	
}
