package ph.com.smart.racetoace.web.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.web.rest.dto.UserProfileDTO;
import ph.com.smart.racetoace.web.rest.dto.mapper.UserMapper;

@RestController
@RequestMapping("/api")
public class UserScoreResource {
	
	private static final String sortBy = "scorePoints";
	
	@Inject
	UserRepository userRepository;
	
	@RequestMapping(value="/userScores",method=RequestMethod.GET)
	public HttpEntity<List<UserProfileDTO>> listUserScores(@RequestParam(value="top",required=false, defaultValue="20") int top,
			@RequestParam(value="sort", required=false, defaultValue="DESC") Direction direction){
			Sort sort = new Sort( new Order(direction, sortBy));
			PageRequest pageRequest = new PageRequest(0,top, sort);
			Page<User> pagedUsers = userRepository.findByUserGroupNotNull(pageRequest);
			List<UserProfileDTO> userProfiles = UserMapper.usersToUserProfileDTOs(pagedUsers.getContent());
			HttpHeaders header = new HttpHeaders();
			header.add("totalParticipants", pagedUsers.getTotalElements()+"");
			return new HttpEntity<List<UserProfileDTO>>(userProfiles,header);
	}

}
