/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

import java.util.List;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.model.User.Role;
import ph.com.smart.racetoace.core.model.UserProfile;
import ph.com.smart.racetoace.rnr.model.UserGroup;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 4:55:27 PM
 * 
 */
@JsonInclude(Include.NON_NULL)
public class UserSessionDTO {

	public String email;
	
	public UserProfile profile;
	
	public String sessionToken;
	
	public Boolean admin;
	
	public Long userGroupId;
	
	public UserGroup userGroup;
	
	public Long currentPoints;
	
	public Long totalPoints;
	
	public List<Role> roles;
	public UserSessionDTO(){
		
	}
	
	public UserSessionDTO(User user, String sessionToken){
		this.email = user.email;
		this.profile = user.profile;
		this.sessionToken = sessionToken;
		this.roles = user.roles;
		this.totalPoints = user.scorePoints;
		this.currentPoints = user.rewardPoints;
	}
	
}
