/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.core.model.Task;
import ph.com.smart.racetoace.core.model.task.LocationTask;
import ph.com.smart.racetoace.core.model.task.PollTask;
import ph.com.smart.racetoace.core.model.task.QuizTask;
import ph.com.smart.racetoace.core.model.task.SimpleTask;
import ph.com.smart.racetoace.web.rest.dto.BaseTaskDTO.Type;
import ph.com.smart.racetoace.web.rest.dto.ChallengeDTO;
import ph.com.smart.racetoace.web.rest.dto.TaskDTO;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 12:20:04 PM
 * 
 */
@Component
public class ChallengeDTOMapper {
	
	public List<ChallengeDTO> challengeToChallengeDTO(List<Challenge> challenges){
		List<ChallengeDTO> listChallengeDTO = new ArrayList<ChallengeDTO>();
		for(Challenge challenge: challenges){
			listChallengeDTO.add(challengeToChallengeDTO(challenge));
		}
		return listChallengeDTO;
	}
	
	public static ChallengeDTO challengeToChallengeDTO(Challenge challenge){
		ChallengeDTO challengeDTO = challengeToChallengeDTO(challenge, true);
		return challengeDTO;
	}
	

	public static ChallengeDTO challengeToChallengeDTO(Challenge challenge, boolean full){
		ChallengeDTO challengeDTO = new ChallengeDTO();
		challengeDTO.details = challenge.details;
		challengeDTO.endDate = challenge.endDate;
		challengeDTO.id = challenge.id;
		challengeDTO.startDate = challenge.startDate;
		challengeDTO.title = challenge.title;
		challengeDTO.consolationPoints = challenge.consolationPoints;
		challengeDTO.winnerPoints = challenge.winnerPoints;
		challengeDTO.penalty = challenge.penalty;
		if(full){
			challengeDTO.tasks = listTaskToListTaskDTO(challenge.tasks);
		}
		return challengeDTO;
	}
	

	public Challenge challengeDTOToChallenge(ChallengeDTO challengeDTO){
		Challenge challenge = new Challenge();
		challenge.details= challengeDTO.details;
		challenge.endDate= challengeDTO.endDate;
		challenge.id= challengeDTO.id;
		challenge.startDate = challengeDTO.startDate;
		challenge.title = challengeDTO.title;
		challenge.winnerPoints = challengeDTO.winnerPoints;
		challenge.consolationPoints = challengeDTO.consolationPoints;
		challenge.penalty = challengeDTO.penalty;
		challenge.tasks = listTaskDTOToListTask(challengeDTO.tasks);
		return challenge;
	}
	
	
	
	public static List<TaskDTO> listTaskToListTaskDTO(List<Task> listTask){
		List<TaskDTO> listTaskDTO = new ArrayList<TaskDTO>();
		for(Task task: listTask){
			listTaskDTO.add(taskToTaskDTO(task));
		}
		return listTaskDTO;
	}
	
	public static TaskDTO taskToTaskDTO(Task task){
		TaskDTO taskDTO = new TaskDTO();
		taskDTO.title = task.title;
		taskDTO.description = task.description;
		if(task instanceof SimpleTask){
			SimpleTask simpleTask = (SimpleTask) task;
			taskDTO.code = simpleTask.code;
			taskDTO.type = Type.SIMPLE;
			taskDTO.id = simpleTask.id;
			
		}else if (task instanceof QuizTask){

			QuizTask quizTask = (QuizTask) task;
			taskDTO.answer = quizTask.answer;
			taskDTO.type = Type.QUIZ;
			taskDTO.id = quizTask.id;
			taskDTO.image = quizTask.image;
		}else if (task instanceof PollTask){

			PollTask pollTask = (PollTask) task;
			taskDTO.id = pollTask.id;
			taskDTO.selections = pollTask.selections;
			taskDTO.type = Type.POLL;
		}else if (task instanceof LocationTask){

			LocationTask locationTask = (LocationTask) task;
				taskDTO.id = locationTask.id;
				taskDTO.type = Type.LOCATION;
				taskDTO.longitude = locationTask.longitude;
				taskDTO.latitude = locationTask.latitude;
				taskDTO.radius = locationTask.radius;
		}else{
			throw new RuntimeException("Invalid task.");
		}
		return taskDTO;
	}


	public List<Task> listTaskDTOToListTask(List<TaskDTO> listTaskDTO){
		List<Task> listTask = new ArrayList<Task>();
		for(TaskDTO taskDTO: listTaskDTO){
			listTask.add(taskDTOToTask(taskDTO));
		}
		return listTask;
	}

	public Task taskDTOToTask(TaskDTO taskDTO){
		Task task = null;
		
		if(taskDTO.type.equals(TaskDTO.Type.SIMPLE)){
			task = new SimpleTask();
			SimpleTask simpleTask = (SimpleTask)task;
			simpleTask.code = taskDTO.code;
			simpleTask.id = taskDTO.id;
			simpleTask.title = taskDTO.title;
			simpleTask.description = taskDTO.description;
		}else if(taskDTO.type.equals(TaskDTO.Type.QUIZ)){
			task = new QuizTask();
			QuizTask quizTask = (QuizTask) task;
			quizTask.answer = taskDTO.answer;
			quizTask.id = taskDTO.id;
			quizTask.title = taskDTO.title;
			quizTask.description = taskDTO.description;
			quizTask.image = taskDTO.image;
		}else if(taskDTO.type.equals(TaskDTO.Type.POLL)){
			task = new PollTask();
			PollTask pollTask = (PollTask) task;
			pollTask.title = taskDTO.title;
			pollTask.description = taskDTO.description;
			pollTask.id = taskDTO.id;
			pollTask.selections = taskDTO.selections;
		}else if(taskDTO.type.equals(TaskDTO.Type.LOCATION)){
			task = new LocationTask();
			LocationTask locationTask = (LocationTask) task;
			locationTask.title = taskDTO.title;
			locationTask.description = taskDTO.description;
			locationTask.id = taskDTO.id;
			locationTask.longitude = taskDTO.longitude;
			locationTask.latitude = taskDTO.latitude;
			locationTask.radius = taskDTO.radius;
		}
			
		return task;
	}
	
	
}
