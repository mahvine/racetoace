/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

import ph.com.smart.racetoace.core.model.UserProfile;

/**
 * @author JRDomingo
 * @since Aug 26, 2015 1:20:25 PM
 * 
 */
public class RegisterDTO extends LoginDTO{

	public UserProfile profile;
	
	public Boolean admin;
	
	public Long userGroupId;
}
