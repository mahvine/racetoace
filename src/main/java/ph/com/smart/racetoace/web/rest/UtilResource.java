/**
 * 
 */
package ph.com.smart.racetoace.web.rest;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javapns.notification.PushNotificationPayload;
import ph.com.smart.racetoace.config.AppConfig;
import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.repository.ChallengeRepository;
import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.notification.Event;
import ph.com.smart.racetoace.notification.PushGateway;
import ph.com.smart.racetoace.notification.apns.ApplePushNotificationService;
import ph.com.smart.racetoace.rnr.model.Award;
import ph.com.smart.racetoace.rnr.model.Recognition;
import ph.com.smart.racetoace.rnr.model.Survey;
import ph.com.smart.racetoace.rnr.model.TeamChallenge;
import ph.com.smart.racetoace.rnr.model.UserTeam;
import ph.com.smart.racetoace.rnr.repository.AwardRepository;
import ph.com.smart.racetoace.rnr.repository.RecognitionRepository;
import ph.com.smart.racetoace.rnr.repository.SurveyRepository;
import ph.com.smart.racetoace.rnr.repository.TeamChallengeRepository;
import ph.com.smart.racetoace.rnr.repository.UserTeamRepository;
import ph.com.smart.racetoace.rnr.service.MailService;
import ph.com.smart.racetoace.web.rest.dto.CreateNotificationDTO;
import ph.com.smart.racetoace.web.rest.dto.UserProfileDTO;

/**
 * @author JRDomingo
 * @since Aug 26, 2015 2:59:06 PM
 * 
 */
@RestController
@RequestMapping("/api")
public class UtilResource {
	
	@Inject
	MailService mailService;
	
	@Inject
	UserRepository userRepository;
	
	@Inject
	PushGateway pushGateway;
	
	@Inject
	ChallengeRepository challengeRepository;
	
	@Inject
	AwardRepository awardRepository;
	
	@Inject
	TeamChallengeRepository teamChallengeRepository;
	
	@Inject
	SurveyRepository surveyRepository;
	
	@Inject
	RecognitionRepository recognitionRepository;
	
	@Inject
	ApplePushNotificationService apns;
	
	@Inject
	UserTeamRepository userTeamRepository;
	
	@RequestMapping(value="/resendActivation", method=RequestMethod.GET)
	public void resendActivationEmail(@RequestParam("email") String email){
		User user = userRepository.findByEmailIgnoreCase(email);
		
		mailService.sendActivationEmail(user, AppConfig.getBaseUrl());
	}
	
	@RequestMapping(value="/gcm/racetoace/events", method=RequestMethod.POST)
	public void simulateNotification(@RequestBody CreateNotificationDTO event){
		if( event.type.equals(Event.Type.AWARD_STARTED)){
			Award award = awardRepository.findOne(event.awardId);
			pushGateway.notifyAwardHasStarted(award);
		}else if(event.type.equals(Event.Type.AWARD_ENDED)){
			Award award = awardRepository.findOne(event.awardId);
			pushGateway.notifyAwardHasEnded(award);
		}else if( event.type.equals(Event.Type.CHALLENGE_STARTED)){
			Challenge challenge = challengeRepository.findOne(event.challengeId);
			pushGateway.notifyChallengeHasStarted(challenge);
		}else if(event.type.equals(Event.Type.CHALLENGE_ENDED)){
			Challenge challenge = challengeRepository.findOne(event.challengeId);
			pushGateway.notifyChallengeHasEnded(challenge);
		}else if(event.type.equals(Event.Type.CHALLENGE_HAS_WINNER)){
			Challenge challenge = challengeRepository.findOne(event.challengeId);
			User user = userRepository.findOne(event.userId);
			pushGateway.notifyChallengeHasWinner(challenge, new UserProfileDTO(user) );
		}else if( event.type.equals(Event.Type.TEAM_CHALLENGE_STARTED)){
			TeamChallenge teamChallenge = teamChallengeRepository.findOne(event.teamChallengeId);
			pushGateway.notifyTeamChallengeHasStarted(teamChallenge);
		}else if(event.type.equals(Event.Type.TEAM_CHALLENGE_ENDED)){
			TeamChallenge teamChallenge = teamChallengeRepository.findOne(event.teamChallengeId);
			pushGateway.notifyTeamChallengeHasEnded(teamChallenge);
		}else if(event.type.equals(Event.Type.TEAM_CHALLENGE_COMPLETED)){
			TeamChallenge teamChallenge = teamChallengeRepository.findOne(event.teamChallengeId);
			UserTeam userTeam = userTeamRepository.findOne(event.teamId);
			pushGateway.notifyTeamChallengeHasCompleted(teamChallenge, userTeam);
		}else if(event.type.equals(Event.Type.RECOGNITION_NEW)){
			Recognition recognition = recognitionRepository.findOne(event.recognitionId);
			pushGateway.notifyNewRecognition(recognition);
		}else if(event.type.equals(Event.Type.SURVEY_NEW)){
			Survey survey = surveyRepository.findOne(event.surveyId);
			pushGateway.notifyNewSurvey(survey);
		}else if(event.type.equals(Event.Type.ANNOUNCEMENT)){
			pushGateway.notifyAnnouncement(event.message);
		}	
	}

	@RequestMapping(value="/apns/racetoace/alert", method=RequestMethod.GET)
	public void simulateApnsPush(@RequestParam("apnsToken") String apnsToken,@RequestParam("message")  String alertMessage){
		PushNotificationPayload payload =PushNotificationPayload.alert(alertMessage);
		List<String> apnsTokens = new ArrayList<String>();
		apnsTokens.add(apnsToken);
		apns.sendApns(apnsTokens, payload);
	}
	
}
