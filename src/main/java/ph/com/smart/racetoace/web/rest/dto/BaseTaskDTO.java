/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

/**
 * @author JRDomingo
 * @since Sep 2, 2015 1:58:20 PM
 * 
 */
public class BaseTaskDTO {

	public Long id;
	
	public String title;
	
	public String description;
	
	public Type type;
	
	public String image;

	public enum Type{
		SIMPLE, POLL, QUIZ, LOCATION
	}
}
