/**
 *
 */
package ph.com.smart.racetoace.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.racetoace.rnr.model.TeamChallenge;
import ph.com.smart.racetoace.rnr.model.UserTeam;
import ph.com.smart.racetoace.rnr.repository.TeamChallengeRepository;
import ph.com.smart.racetoace.rnr.repository.UserTeamRepository;
import ph.com.smart.racetoace.rnr.service.TeamChallengeService;
import ph.com.smart.racetoace.web.rest.util.PaginationUtil;

/**
 * @author Jrdomingo
 * Jan 21, 2016 3:49:20 PM
 * TODO
 */
@RestController
@RequestMapping("/api")
public class TeamChallengeResource {
	
	@Inject
	TeamChallengeRepository teamChallengeRepository;
	
	@Inject
	TeamChallengeService teamChallengeService;
	
	@Inject
	UserTeamRepository userTeamRepository;

	@RequestMapping(value="/teamChallenges", method=RequestMethod.POST)
	public void createChallenge(@RequestBody TeamChallenge teamChallenge){
		teamChallengeRepository.save(teamChallenge);
	}

	@RequestMapping(value="/teamChallenges", method=RequestMethod.GET)
	public ResponseEntity<List<TeamChallenge>> listTeamChallenges(@RequestParam(value="page",required=false, defaultValue="1") int page,
			@RequestParam(value="size",required=false, defaultValue="20") int size, @RequestParam(value="sort", defaultValue="id") String sortBy,
			@RequestParam(value="direction", defaultValue="DESC") Direction direction) throws URISyntaxException{
		Sort sort = new Sort(direction,sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<TeamChallenge> pageChallenges = teamChallengeRepository.findAll(pageRequest);
		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageChallenges, "/api/teamChallenges", page, size);
		return new ResponseEntity<List<TeamChallenge>>(pageChallenges.getContent(),headers,HttpStatus.OK);
	}

	@RequestMapping(value="/teamChallenges/{id}", method=RequestMethod.GET)
	public TeamChallenge getById(@PathVariable("id") Long id){
		return teamChallengeRepository.findOne(id);
	}
	

	@RequestMapping(value="/teamChallenges/{id}/teams", method=RequestMethod.GET)
	public List<UserTeam> getTeamsById(@PathVariable("id") Long id){
		return userTeamRepository.findByTeamChallengeId(id);
		
	}

	@RequestMapping(value="/teamChallenges/{id}", method=RequestMethod.DELETE)
	public void deleteChallenge(@PathVariable("id") Long id){
		teamChallengeRepository.delete(id);
	}
	
	
	

}
