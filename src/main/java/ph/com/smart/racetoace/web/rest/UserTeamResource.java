/**
 * 
 */
package ph.com.smart.racetoace.web.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.TeamChallenge;
import ph.com.smart.racetoace.rnr.model.TeamChallenge.Status;
import ph.com.smart.racetoace.rnr.model.UserTeam;
import ph.com.smart.racetoace.rnr.repository.TeamChallengeRepository;
import ph.com.smart.racetoace.rnr.repository.UserTeamRepository;
import ph.com.smart.racetoace.rnr.service.TeamChallengeService;
import ph.com.smart.racetoace.web.rest.dto.TeamChallengeDTO;
import ph.com.smart.racetoace.web.rest.dto.UserTeamDTO;

/**
 * @author JRDomingo
 * @since Sep 2, 2015 10:14:58 AM
 * 
 */
@RestController
@RequestMapping("/api")
public class UserTeamResource {

	@Inject
	UserResource userResource;
	
	@Inject
	TeamChallengeService teamChallengeService;
	
	@Inject
	TeamChallengeRepository teamChallengeRepository;
	
	@Inject
	UserTeamRepository userTeamRepository;
	
	
	@RequestMapping(value="/userTeamChallenges", method=RequestMethod.GET)
	public List<TeamChallengeDTO> getActiveTeamChallenges(@RequestHeader("X-session") String sessionToken,
			@RequestParam(value="status",required=false, defaultValue="OPEN") Status status){
		Sort sort = new Sort( new Order(Direction.DESC, "startDate"),
				new Order(Direction.ASC, "endDate"));
		User user = userResource.getUser(sessionToken);
		Date today = new Date();
		List<TeamChallengeDTO> teamChallengeDTOs = new ArrayList<TeamChallengeDTO>();

		//open
		if(status.equals(Status.OPEN)){
			List<TeamChallenge> teamChallenges = teamChallengeRepository.findByStartDateBeforeAndEndDateAfter(today, today, sort);
			for(TeamChallenge teamChallenge: teamChallenges){
				teamChallengeDTOs.add(new TeamChallengeDTO(teamChallenge,user));
			}
		}else if( status.equals(Status.JOINED)){
			List<UserTeam> userTeams = userTeamRepository.findByMembersIdAndWinner(user.id, false);
			for(UserTeam userTeam: userTeams){
				teamChallengeDTOs.add(new TeamChallengeDTO(userTeam.teamChallenge,user));
			}
		}else if( status.equals(Status.COMPLETED)){

			List<UserTeam> userTeams = userTeamRepository.findByMembersIdAndWinner(user.id, true);
			for(UserTeam userTeam: userTeams){
				teamChallengeDTOs.add(new TeamChallengeDTO(userTeam.teamChallenge,user));
			}
		}
		//completed
		
		return teamChallengeDTOs;
	}


	@RequestMapping(value="/userTeamChallenges/{teamChallengeId}", method=RequestMethod.GET)
	public TeamChallengeDTO getActiveTeamChallenges(@RequestHeader("X-session") String sessionToken,
			@PathVariable("teamChallengeId") Long teamChallengeId){
		User user = userResource.getUser(sessionToken);

		TeamChallenge teamChallenge = teamChallengeRepository.findOne(teamChallengeId);
		return new TeamChallengeDTO(teamChallenge,user);
	}

	
	
	@RequestMapping(value="/userTeamChallenges/{id}/teams", method=RequestMethod.GET)
	public List<UserTeamDTO> getTeams(@PathVariable("id") Long teamChallengeId, @RequestHeader("X-session") String sessionToken){
		User requestor = userResource.getUser(sessionToken);
		List<UserTeam> userTeams = userTeamRepository.findByTeamChallengeId(teamChallengeId);
		List<UserTeamDTO> response = new ArrayList<UserTeamDTO>();
		for(UserTeam userTeam: userTeams){
			response.add(new UserTeamDTO(userTeam, requestor));
		}
		return response;
	}

	@RequestMapping(value="/userTeamChallenges/{teamChallengeId}/teams", method=RequestMethod.POST)
	public void createTeam(@PathVariable("teamChallengeId") Long teamChallengeId, @RequestHeader("X-session") String sessionToken,
			@RequestBody CreateTeamDTO request){
		User user = userResource.getUser(sessionToken);
		teamChallengeService.createTeam(teamChallengeId, user, request.name, request.code);
		
	}
	

	@RequestMapping(value="/userTeamChallenges/{teamChallengeId}", method=RequestMethod.PUT)
	public void completeTeamChallenge(@PathVariable("teamChallengeId") Long teamChallengeId, @RequestHeader("X-session") String sessionToken,
			@RequestBody CompleteTeamChallengeDTO request){
		User user = userResource.getUser(sessionToken);
		teamChallengeService.completeTeamChallenge(teamChallengeId, request.teamId);
	}
	

	@RequestMapping(value="/userTeams/{teamId}", method=RequestMethod.POST)
	public void joinTeam(@PathVariable("teamId") Long teamId, @RequestHeader("X-session") String sessionToken,
			@RequestBody JoinTeamDTO request){
		User user = userResource.getUser(sessionToken);
		teamChallengeService.addMemberToTeam(teamId, user, request.code);
	}
	
	
	public static class CreateTeamDTO{
		public String name;
		public String code;
	}

	public static class JoinTeamDTO{
		public String code;
	}

	public static class CompleteTeamChallengeDTO{
		public Long teamId;
	}
	
}
