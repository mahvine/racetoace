package ph.com.smart.racetoace.web.rest.dto;

import ph.com.smart.racetoace.rnr.model.Nomination;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Created by KCAsis on 9/9/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class NominatedUserWithProfileDTO extends NominateUserDTO {

    public UserProfileDTO nominatedUser;

    public NominatedUserWithProfileDTO() {}

    public NominatedUserWithProfileDTO(Nomination nomination) {
        this.nominated = nomination.nominated.id;
        this.nominatedUser = new UserProfileDTO(nomination.nominated);
        this.reason = nomination.reason;
    }
}
