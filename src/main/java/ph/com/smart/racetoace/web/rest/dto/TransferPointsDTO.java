package ph.com.smart.racetoace.web.rest.dto;

public class TransferPointsDTO {
	public Long userId;
	public Long points;
}
