package ph.com.smart.racetoace.web.rest;

import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.rnr.repository.AwardRepository;
import ph.com.smart.racetoace.rnr.service.AwardWinnerManager;
import ph.com.smart.racetoace.web.rest.dto.UserProfileDTO;

/**
 * Created by KCAsis on 9/10/2015.
 */
@RestController
@RequestMapping("/api")
public class WinnerResource {

    @Inject
    AwardRepository awardRepository;

    @Inject
    AwardWinnerManager awardWinnerManager;
    
    @Inject
    UserRepository userRepository;

    @RequestMapping(value = "/awards/{id}/winners", method = RequestMethod.GET)
    public List<UserProfileDTO> getWinnersPerAward(@PathVariable("id") Long awardId) {
        List<User> winners = awardRepository.findOne(awardId).winners;
    	Sort sort = new Sort(new Order(Direction.ASC, "userGroup"), new Order(Direction.ASC, "name"));
//    	List<User> winners = userRepository.findWinnersOfAward(awardId, sort);
        List<UserProfileDTO> winnersDTO = new LinkedList<>();
        for (User u : winners) {
            winnersDTO.add(new UserProfileDTO(u));
        }
        return winnersDTO;
    }

//    @RequestMapping(value = "/awards/{id}/process", method = RequestMethod.GET)
//    public void processWinners(@PathVariable("id") Long awardId, @RequestHeader("secret") String sekrit) {
//        if (sekrit.equals("sooper")) {
//            // TODO: make this a scheduled task
//            awardWinnerManager.processWinnersAndPoints(awardId);
//        } else throw new RuntimeException("not sekrit enuf");
//    }

}
