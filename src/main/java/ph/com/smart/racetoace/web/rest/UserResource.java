/**
 * 
 */
package ph.com.smart.racetoace.web.rest;

import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import com.codahale.metrics.annotation.Timed;

import ph.com.smart.racetoace.config.AppConfig;
import ph.com.smart.racetoace.core.model.CompletedTask;
import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.model.User.Role;
import ph.com.smart.racetoace.core.model.User.Status;
import ph.com.smart.racetoace.core.model.UserChallenge;
import ph.com.smart.racetoace.core.model.UserProfile;
import ph.com.smart.racetoace.core.model.task.PollFeedback;
import ph.com.smart.racetoace.core.repository.CompletedTaskRepository;
import ph.com.smart.racetoace.core.repository.UserChallengeRepository;
import ph.com.smart.racetoace.core.repository.UserProfileRepository;
import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.core.task.repository.PollFeedbackRepository;
import ph.com.smart.racetoace.rnr.model.Award;
import ph.com.smart.racetoace.rnr.model.Nomination;
import ph.com.smart.racetoace.rnr.model.Redemption;
import ph.com.smart.racetoace.rnr.model.UserGroup;
import ph.com.smart.racetoace.rnr.repository.AwardRepository;
import ph.com.smart.racetoace.rnr.repository.NominationRepository;
import ph.com.smart.racetoace.rnr.repository.RedemptionRepository;
import ph.com.smart.racetoace.rnr.repository.UserGroupRepository;
import ph.com.smart.racetoace.rnr.service.MailService;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.AccountNotVerified;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.EmailAlreadyRegistered;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.InvalidPassword;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.InvalidResetKey;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.InvalidSessionToken;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.UnregisteredEmail;
import ph.com.smart.racetoace.web.rest.dto.ChangePasswordDTO;
import ph.com.smart.racetoace.web.rest.dto.ChangePasswordResetDTO;
import ph.com.smart.racetoace.web.rest.dto.LoginDTO;
import ph.com.smart.racetoace.web.rest.dto.RegisterDTO;
import ph.com.smart.racetoace.web.rest.dto.SaveApnsTokenDTO;
import ph.com.smart.racetoace.web.rest.dto.SaveGcmIdDTO;
import ph.com.smart.racetoace.web.rest.dto.UserProfileDTO;
import ph.com.smart.racetoace.web.rest.dto.UserSessionDTO;
import ph.com.smart.racetoace.web.rest.dto.mapper.UserMapper;
import ph.com.smart.racetoace.web.rest.util.PaginationUtil;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 4:54:46 PM
 * 
 */
@RestController
public class UserResource {
	
	private static final Logger logger = LoggerFactory.getLogger(UserResource.class);
	
	@Inject
	UserRepository userRepository;

	@Inject
	UserProfileRepository userProfileRepository;
	
	@Inject
	UserGroupRepository userGroupRepository;
	
	@Inject
	MailService mailService;
	
	@Inject
	UserMapper userMapper;
	
	@Inject
	UserChallengeRepository userChallengeRepository;
	
	@Inject
	CompletedTaskRepository completedTaskRepository;
	
	@Inject
	NominationRepository nominationRepository;
	
	@Inject
	RedemptionRepository redemptionRepository;
	
	@Inject
	PollFeedbackRepository pollFeedbackRepository;
	
	@Inject
	AwardRepository awardRepository;
	
	
	public static Map<String,Long> sessionHash = new HashMap<String,Long>();
	static{
		sessionHash.put("ecb252044b5ea0f679ee78ec1a12904739e2904d", 2L);
	}
	public static String encrypt(String string){
		return DigestUtils.sha1Hex(string);
	}
	
	@Timed
	@RequestMapping(value="/api/register", method=RequestMethod.POST)
	public void register(@Valid @RequestBody RegisterDTO registerDTO,HttpServletRequest request){
		String email = registerDTO.email.toLowerCase().trim();
		String password = registerDTO.password;
		
		User user = userRepository.findByEmailIgnoreCase(email);
		if(user == null){
			user = new User();
			user.dateRegistered = new Date();
			user.email = email;
			user.salt = RandomStringUtils.randomAlphanumeric(6);
			user.encryptedPassword = encrypt(password, user.salt);
			user.profile = registerDTO.profile;
			user.roles.add(Role.CHALLENGER);
			if(registerDTO.admin!=null){
				if(registerDTO.admin){
					user.roles.add(Role.CHALLENGE_MANAGER);
				}
			}
			UserGroup userGroup = null;
			if(registerDTO.userGroupId!=null){
				userGroup = userGroupRepository.findOne(registerDTO.userGroupId);
				if(userGroup==null){
					throw new GenericUserException("Invalid user group");
				}
					
			}
			user.activationKey = encrypt(user.email);
			userProfileRepository.save(user.profile);
			userRepository.save(user);
			String baseUrl = request.getScheme() + // "http"
                    "://" +                                // "://"
                    request.getServerName() +              // "myhost"
                    ":" +                                  // ":"
                    request.getServerPort();               // "80"
			
			mailService.sendActivationEmail(user, baseUrl);
			
			if(userGroup!=null){
				userGroup.users.add(user);
				userGroupRepository.save(userGroup);
			}
			
		}else{
			throw new EmailAlreadyRegistered();
		}
	}
	
	@Timed
	@RequestMapping(value="/api/login", method=RequestMethod.POST)
	public UserSessionDTO login(@Valid @RequestBody LoginDTO loginDTO){
		String email = loginDTO.email.toLowerCase().trim();
		String password = loginDTO.password;
		User user = userRepository.findByEmailIgnoreCase(email);
		if(user != null){
			String encryptedPassword = encrypt(password,user.salt);
			if(user.encryptedPassword.equals(encryptedPassword)){
				String sessionToken = encrypt(user.email);
				sessionHash.put(sessionToken, user.id);
				UserSessionDTO userDTO = new UserSessionDTO();
				userDTO.email = email;
				userDTO.profile = user.profile;
				userDTO.sessionToken = sessionToken;
				userDTO.userGroup = userGroupRepository.findByUsersId(user.id);
				if(!user.status.equals(Status.VERIFIED)){
					throw new AccountNotVerified();
				}
				return userDTO;
			}else{
				throw new InvalidPassword();
			}
		}else{
			throw new UnregisteredEmail();
		}
	}
	
	@Timed
	@RequestMapping(value="/api/session", method=RequestMethod.GET)
	public UserSessionDTO getSession(@RequestHeader("X-session") String sessionToken){
		User user = getUser(sessionToken);
		if(user.roles.contains(Role.CHALLENGE_MANAGER)){
			UserGroup userGroup = userGroupRepository.findByUsersId(user.id);
			UserSessionDTO userSessionDTO = userMapper.userToUserSessionDTO(user, sessionToken, userGroup);
			return userSessionDTO;
		}else{
			throw new InvalidSessionToken();
		}
	}

	@Timed
	@RequestMapping(value="/activate", method=RequestMethod.GET)
	public ModelAndView activate(@RequestParam("activationKey") String activationKey,ModelAndView modelAndView){
		User user = userRepository.findByActivationKey(activationKey);
		if(user!=null){
			user.status = Status.VERIFIED;
			userRepository.save(user);
		}else{
			throw new GenericUserException("Invalid activation key");
		}

		modelAndView.setViewName("mdl/layouts/base");
        modelAndView.addObject("view", "mdl/fragments/accountactivated");
        return modelAndView;
	}

	@Timed
	@RequestMapping(value="/api/resetPassword", method=RequestMethod.GET)
	public String resetPassword(@RequestParam("email") String email){
		User user = userRepository.findByEmailIgnoreCase(email);
		if(user!=null){
			user.resetKey = encrypt(user.email+new Date().toString());
			userRepository.save(user);
			mailService.sendPasswordResetMail(user, AppConfig.getBaseUrl());
		}else{
			throw new GenericUserException("Invalid email");
		}
		
		return "reset password link sent to email. TODO create a webpage";
	}
	
	@Timed
	@RequestMapping(value="/api/resetPassword", method=RequestMethod.PUT)
	public void resetPasswordUsingResetKey(@RequestBody ChangePasswordResetDTO changePasswordResetDTO){
		
		String email = changePasswordResetDTO.email;
		User user = userRepository.findByEmailIgnoreCase(email);
		String resetKey = changePasswordResetDTO.resetKey;
		String newPassword = changePasswordResetDTO.newPassword;
		if(resetKey.equals(user.resetKey)){
			String encryptedPassword = encrypt(newPassword,user.salt);
			user.encryptedPassword = encryptedPassword;
			user.resetKey = RandomStringUtils.randomAlphanumeric(10); //reset key
			userRepository.save(user);
		}else{
			throw new InvalidResetKey();
		}
	}

	@Timed
	@RequestMapping(value="/api/password", method=RequestMethod.PUT)
	public void changePassword(@RequestBody ChangePasswordDTO changePasswordDTO, @RequestHeader("X-session") String session){
		
		User user = getUser(session);
		String oldPassword = changePasswordDTO.oldPassword;
		String newPassword = changePasswordDTO.newPassword;

		String encryptedPassword = encrypt(oldPassword,user.salt);
		if(user.encryptedPassword.equals(encryptedPassword)){
			user.encryptedPassword  = encrypt(newPassword,user.salt);
			user.resetKey = RandomStringUtils.randomAlphanumeric(10); //reset key
			userRepository.save(user);
		}else{
			throw new InvalidPassword();
		}
	}

	@Timed
	@RequestMapping(value = "/api/profile", method = RequestMethod.GET)
	public UserProfileDTO getProfile(@RequestHeader("X-session") String session) {
		User user = getUser(session);
		return new UserProfileDTO(user);
	}

	@Timed
	@RequestMapping(value = "/api/profile", method = RequestMethod.PUT)
	public void updateProfile(@RequestBody UserProfileDTO userProfileDTO, @RequestHeader("X-session") String session) {
		User user = getUser(session);
		if (user.profile == null) {
			user.profile = new UserProfile();
			userProfileRepository.save(user.profile);
			userRepository.save(user);
		}
		user.profile.name = userProfileDTO.name;
		user.profile.pictureUrl = userProfileDTO.pictureUrl;
		user.profile.birthDate = userProfileDTO.birthDate;
		user.profile.hiringDate = userProfileDTO.hiringDate;
		if(userProfileDTO.userGroup!=null){
			user.userGroup = userProfileDTO.userGroup;
			userRepository.save(user);
		}
		userRepository.save(user);
		userProfileRepository.save(user.profile);
	}
	
	@Timed
	@RequestMapping(value="/appleDevice", method=RequestMethod.POST)
	public void saveApnsToken(@RequestHeader("X-session") String sessionToken, @RequestBody SaveApnsTokenDTO request){
		User user = getUser(sessionToken);
		if(user!=null){
			user.apnsToken = request.apnsToken;
			userRepository.save(user);
		}
	}

	@Timed
	@RequestMapping(value="/androidDevice", method=RequestMethod.POST)
	public void saveGcmId(@RequestHeader("X-session") String sessionToken, @RequestBody SaveGcmIdDTO request){
		User user = getUser(sessionToken);
		if(user!=null){
			user.gcmId = request.gcmId;
			userRepository.save(user);
		}
	}

	@Timed
	@RequestMapping(value="/api/users",method=RequestMethod.GET)
	public ResponseEntity<List<UserProfileDTO>> listUsers(@RequestParam(value="page",required=false, defaultValue="1") int page,
			@RequestParam(value="size",required=false, defaultValue="10") int size, @RequestParam(value="sort", defaultValue="id") String sortBy,
			@RequestParam(value="direction", defaultValue="DESC") Direction direction) throws URISyntaxException{
		Sort sort = new Sort(direction,sortBy);
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<User> pageUsers = userRepository.findByUserGroupNotNull(pageRequest);
		List<UserProfileDTO> userProfileDTOs = UserMapper.usersToUserProfileDTOs(pageUsers.getContent());

		HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(pageUsers, "/api/users", page, size);
		return new ResponseEntity<List<UserProfileDTO>>(userProfileDTOs,headers,HttpStatus.OK);
	}
	
	@Timed
	@RequestMapping(value="/api/users/{userId}", method=RequestMethod.PUT)
	public void updateUserInfo(@PathVariable Long userId, @RequestBody UserProfileDTO request){
		User user = userRepository.findOne(userId);
		logger.debug("Save of user:{} started",userId);
//		User user = new User();
//		user.id = userId;
		
		user.awardGiver = request.awardGiver;
		if(request.userGroup!=null){
			UserGroup userGroup = new UserGroup();
			userGroup.id = request.userGroup.id;
			user.userGroup = userGroup;
		}
		userRepository.save(user);

		logger.debug("Save of user:{} was successful",userId);
	}
	
	@Timed
	@RequestMapping(value="/api/users",method=RequestMethod.DELETE)
	public void deleteUser(@RequestParam("email") String email){
		User user = userRepository.findByEmailIgnoreCase(email);
		
		List<PollFeedback> pollFeedbacks = pollFeedbackRepository.findByUser(user);
		pollFeedbackRepository.delete(pollFeedbacks);
		List<CompletedTask> completedTasks = completedTaskRepository.findByUser(user);
		completedTaskRepository.delete(completedTasks);
		List<UserChallenge> userChallenges = userChallengeRepository.findByUser(user);
		userChallengeRepository.delete(userChallenges);
		List<Nomination> awardNominations = nominationRepository.findByNominated(user);
		nominationRepository.delete(awardNominations);

		List<Nomination> awardNominationated = nominationRepository.findByNominator(user);
		nominationRepository.delete(awardNominationated);
		List<Redemption> redemptions = redemptionRepository.findByUserId(user.id);
		redemptionRepository.delete(redemptions);
		
		List<Award> awards = awardRepository.findByWinners(user);
		for(Award award: awards){
			award.winners.remove(user);
			awardRepository.save(award);
		}
		
		userRepository.delete(user);
	}

	public User getUser(String sessionToken){
		Long userId = sessionHash.get(sessionToken);
		if(userId!=null){
			return userRepository.findOne(userId);
		}
		throw new InvalidSessionToken();
	}

	public static String encrypt(String password,String salt){
		return encrypt(password+salt);
	}
}
