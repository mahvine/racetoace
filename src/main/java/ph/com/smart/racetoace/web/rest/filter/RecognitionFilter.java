/**
 *
 */
package ph.com.smart.racetoace.web.rest.filter;

import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.servlet.http.HttpServletRequest;

import ph.com.smart.racetoace.rnr.model.Recognition;

/**
 * @author Jrdomingo
 * Jan 11, 2016 11:09:49 AM
 */
public class RecognitionFilter extends SpecificationFilter<Recognition>{

	/**
	 * @param request
	 */
	public RecognitionFilter(HttpServletRequest request) {
		super(request);
	}

	/* (non-Javadoc)
	 * @see ph.com.smart.racetoace.web.rest.filter.SpecificationFilter#createPredicates(javax.persistence.criteria.Root, javax.persistence.criteria.CriteriaQuery, javax.persistence.criteria.CriteriaBuilder, java.util.List)
	 */
	@Override
	public void createPredicates(Root<Recognition> root, CriteriaQuery<?> criteria, CriteriaBuilder criteriaBuilder, List<Predicate> predicates) {

		Long idAfter = getLongParam("idAfter");
		if(idAfter!=null){
			Predicate idAfterFilter = criteriaBuilder.greaterThan(root.<Long>get("id"), idAfter);
			predicates.add(idAfterFilter);
		}

		Long idBefore = getLongParam("idBefore");
		if(idBefore!=null){
			Predicate idLessFilter = criteriaBuilder.lessThan(root.<Long>get("id"), idBefore);
			predicates.add(idLessFilter);
		}
	}


}
