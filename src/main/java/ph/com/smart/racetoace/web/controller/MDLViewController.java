package ph.com.smart.racetoace.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Material Design Lite View Controller
 * @author JRDomingo
 * @since Sep 2, 2015 8:05:25 AM
 *
 */
@Controller
public class MDLViewController {

	@RequestMapping("/password")
	public ModelAndView changePassword(ModelAndView modelAndView){
		modelAndView.setViewName("mdl/layouts/base");
        modelAndView.addObject("view", "mdl/fragments/changepasswordform");
		return modelAndView;
	}


	@RequestMapping("/password/success")
	public ModelAndView changePasswordSuccess(ModelAndView modelAndView){
		modelAndView.setViewName("mdl/layouts/base");
        modelAndView.addObject("view", "mdl/fragments/changepasswordsuccess");
		return modelAndView;
	}
	
	@RequestMapping("/")
	public String index(){
		return "forward:/admin/index.html";
	}
	
}
