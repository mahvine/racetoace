/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

/**
 * @author JRDomingo
 * @since Sep 2, 2015 8:38:39 AM
 * 
 */
public class ChangePasswordResetDTO {
	
	public String email;
	public String resetKey;
	public String newPassword;

}
