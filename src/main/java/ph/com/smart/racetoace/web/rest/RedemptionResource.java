package ph.com.smart.racetoace.web.rest;

import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.Redemption;
import ph.com.smart.racetoace.rnr.repository.RedemptionRepository;
import ph.com.smart.racetoace.rnr.service.PointService;
import ph.com.smart.racetoace.rnr.service.RedemptionService;
import ph.com.smart.racetoace.web.rest.dto.RedemptionDTO;
import ph.com.smart.racetoace.web.rest.dto.TransferPointsDTO;
/**
 * Created by KCAsis on 9/3/2015.
 */
@RestController
@RequestMapping("/api")
public class RedemptionResource {

    @Inject
    UserResource userResource;

    @Inject
    RedemptionService redemptionService;
    
    @Inject
    RedemptionRepository redemptionRepository;
    
    @Inject
    PointService pointService;

    @RequestMapping(value = "/rewards/confirm/{code}", method = RequestMethod.GET)
    public void confirmRedemptionWithCode(@PathVariable(value = "code") String code) {
        // TODO: check if user is admin, only admin can confirm
        redemptionService.confirmRedemption(code);
    }

    @RequestMapping(value = "/rewards", method = RequestMethod.GET)
    public List<RedemptionDTO> getAllRedemptions(@RequestParam(value = "forUser", required = false) Long userId) {
        // TODO: check if user is admin, only admin can get per userId
    	List<Redemption> redemptions = redemptionService.getRedeemedItemsforUserId(userId); 
    	List<RedemptionDTO> redemptionDTOs = RedemptionDTO.list(redemptions);
        return redemptionDTOs;
    }

    @RequestMapping(value = "/rewards/my", method = RequestMethod.GET)
    public List<RedemptionDTO> getAllRedemptionsForUser(@RequestHeader("X-session") String sessionToken, 
    		@RequestParam(value="sort", required=false, defaultValue="id") String sortBy,
    		@RequestParam(value="direction", required=false, defaultValue="DESC") Direction direction) {
        User user = userResource.getUser(sessionToken);
        Sort sort = new Sort(new Order(direction, sortBy));
        List<Redemption> redemptions = redemptionRepository.findByUserId(user.id,sort);
//    	List<Redemption> redemptions = redemptionService.getRedeemedItemsforUserId(user.id); 
    	List<RedemptionDTO> redemptionDTOs = RedemptionDTO.list(redemptions);
        return redemptionDTOs;
    }
    

	@RequestMapping(value = "/rewards/transfer", method = RequestMethod.PUT)
	public void transferPoints(@RequestHeader("X-session") String sessionToken, @RequestBody TransferPointsDTO request) {
		User user = userResource.getUser(sessionToken);
		pointService.transferRewardPoints(user.id, request.userId, request.points);
	}
    
}
