package ph.com.smart.racetoace.web.rest.dto;

import ph.com.smart.racetoace.notification.Event;

public class CreateNotificationDTO {
	
	public Event.Type type;
	public String message;
	public Long challengeId;
	public Long awardId;
	public Long userId;
	public Long recognitionId;
	public Long teamChallengeId;
	public Long teamId;
	public Long surveyId;

}
