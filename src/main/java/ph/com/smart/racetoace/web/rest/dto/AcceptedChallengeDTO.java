/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

import ph.com.smart.racetoace.core.model.UserChallenge.Status;

/**
 * @author Jrdomingo
 * @since Sep 14, 2015 9:46:55 AM
 * 
 */
public class AcceptedChallengeDTO {

	public UserProfileDTO user;
	
	public ChallengeDTO challenge;
	
	public Status status;
	
	public Boolean winner;
	
	public Long earnedPoints;
	
}
