package ph.com.smart.racetoace.web.rest.dto;

/**
 * Indicates the number of nomination a user has received on a certain nomination
 * @author Jrdomingo
 * @since Sep 20, 2015 1:20:19 PM
 */
public class UserNominationCountDTO {

	public UserProfileDTO user;
	public Long count;
	
}
