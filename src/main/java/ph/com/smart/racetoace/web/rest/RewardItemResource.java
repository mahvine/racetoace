/**
 *
 */
package ph.com.smart.racetoace.web.rest;

import java.net.URISyntaxException;
import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.Redemption;
import ph.com.smart.racetoace.rnr.model.RewardItem;
import ph.com.smart.racetoace.rnr.repository.RewardItemRepository;
import ph.com.smart.racetoace.rnr.service.RedemptionService;
import ph.com.smart.racetoace.web.rest.dto.RedemptionDTO;
import ph.com.smart.racetoace.web.rest.util.PaginationUtil;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 1:57:37 PM
 *
 */
@RestController
@RequestMapping("/api")
public class RewardItemResource {

	@Inject
	RewardItemRepository rewardItemRepository;

	@Inject
	RedemptionService redemptionService;

	@Inject
	UserResource userResource;

	@RequestMapping(value="/items", method=RequestMethod.POST)
	public void saveOrModify(@RequestBody RewardItem rewardItem){
		rewardItemRepository.save(rewardItem);
	}

	@RequestMapping(value = "/items", method = RequestMethod.DELETE)
	public void delete(@RequestBody RewardItem rewardItem) {
		rewardItemRepository.delete(rewardItem);
	}

	@RequestMapping(value = "/items/{id}", method = RequestMethod.DELETE)
	public void deleteItemById(@PathVariable(value = "id") Long itemId) {
		rewardItemRepository.delete(itemId);
	}

	@RequestMapping(value = "/items/{id}", method = RequestMethod.GET)
	public RewardItem getItemById(@PathVariable(value = "id") Long itemId) {
		return rewardItemRepository.findOne(itemId);
	}

	@RequestMapping(value = "/items/{id}/claim", method = RequestMethod.POST)
	public RedemptionDTO redeemItem(@PathVariable(value = "id") Long itemId,
			@RequestHeader("X-session") String sessionToken) {
		RewardItem redeemItem = rewardItemRepository.findOne(itemId);
		User user = userResource.getUser(sessionToken);
		String code = redemptionService.redeemRewardItem(user.id, redeemItem.id);
		Redemption redemption = redemptionService.getRedemptionByCode(code);
		RedemptionDTO redemptionDTO = new RedemptionDTO(redemption);
		return redemptionDTO;
	}

	@RequestMapping(value="/items", method=RequestMethod.GET)
	public ResponseEntity<List<RewardItem>> listItems(@RequestParam(value="page",required=false, defaultValue="0") int page,
			@RequestParam(value="size",required=false, defaultValue="20") int size,
			@RequestParam(value="sort", required=false, defaultValue="name") String sortBy,
			@RequestParam(value="direction", required=false, defaultValue="ASC") Direction direction, 
			@RequestParam(value="quantityGte", required=false, defaultValue="0") int quantity) throws URISyntaxException{
		Sort sort = new Sort( new Order(direction, sortBy));
		PageRequest pageRequest = PaginationUtil.generatePageRequest(page, size, sort);
		Page<RewardItem> pageRewardItems = rewardItemRepository.findByQuantityGreaterThanEqual(quantity, pageRequest);
		HttpHeaders httpHeaders = PaginationUtil.generatePaginationHttpHeaders(pageRewardItems, "/api/items", page, size);
		return new ResponseEntity<List<RewardItem>>(pageRewardItems.getContent(),httpHeaders, HttpStatus.OK); 
	}

}
