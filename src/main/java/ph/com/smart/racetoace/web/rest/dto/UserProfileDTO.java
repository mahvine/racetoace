package ph.com.smart.racetoace.web.rest.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.model.User.Role;
import ph.com.smart.racetoace.rnr.model.UserGroup;

/**
 * Created by KCAsis on 9/8/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserProfileDTO {
    public Long id;
    public String email;
    public String name;
    public long rewardPoints;
    public long scorePoints;
    public UserGroup userGroup;
    public String pictureUrl;
    public Date birthDate;
    public Date hiringDate;
    public String status;
    public boolean awardGiver;
    public boolean isAdmin;

    public UserProfileDTO() {
    }

    public UserProfileDTO(User user) {
        if (user.profile != null) {
            this.name = user.profile.name;
            this.pictureUrl = user.profile.pictureUrl;
            this.birthDate = user.profile.birthDate;
            this.hiringDate = user.profile.hiringDate;
        }
        this.id = user.id;
        this.rewardPoints = user.rewardPoints;
        this.scorePoints = user.scorePoints;
        this.email = user.email;
        this.userGroup = user.userGroup;
        this.status = user.status.toString();
        this.awardGiver = user.awardGiver;
        this.isAdmin = user.roles.contains(Role.CHALLENGE_MANAGER);
    }
}
