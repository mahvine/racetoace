package ph.com.smart.racetoace.web.rest;

import java.util.List;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.reports.UserExcelBuilder;

@Controller
@RequestMapping("/downloads")
public class DownloadsController {
	
	@Inject
	UserRepository userRepository;
	
	
	@RequestMapping(value="/users")
	public ModelAndView downloadVehicles( ModelAndView mav,
	        HttpServletRequest request, 
	        HttpServletResponse response) {
		List<User> users = userRepository.findByUserGroupNotNull();
	    mav.setView(new UserExcelBuilder(users));
	    return mav;
	}

}
