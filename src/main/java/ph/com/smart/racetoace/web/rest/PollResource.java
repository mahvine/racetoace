package ph.com.smart.racetoace.web.rest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.racetoace.core.model.task.PollTask;
import ph.com.smart.racetoace.core.model.task.PollTask.Selection;
import ph.com.smart.racetoace.core.task.repository.PollFeedbackRepository;
import ph.com.smart.racetoace.core.task.repository.PollTaskRepository;

@RestController
@RequestMapping(value="/api")
public class PollResource {
	
	@Inject
	PollFeedbackRepository pollFeedbackRepository;
	
	@Inject
	PollTaskRepository pollTaskRepository;
	
	@RequestMapping(value="/polls/{taskId}/results", method=RequestMethod.GET)
	public List<PollResultDTO> getResult(@PathVariable Long taskId){
		PollTask poll = pollTaskRepository.findOne(taskId);
		Map<String, PollResultDTO> resultsMap = new HashMap<String,PollResultDTO>();
		for(Selection selection : poll.selections){
			PollResultDTO pollResultDTO = new PollResultDTO();
			pollResultDTO.text = selection.text;
			pollResultDTO.choice = selection.choice;
			pollResultDTO.votes = 0L;
			resultsMap.put(selection.choice, pollResultDTO);
		}
		List<Object[]> rawResults = pollFeedbackRepository.getPollFeedbacks(taskId);
		List<PollResultDTO> results = new ArrayList<PollResultDTO>();
		for(Object[] rawResult:rawResults){
			String choice = (String) rawResult[0];
			PollResultDTO pollResultDTO = resultsMap.get(choice);
			pollResultDTO.votes = (Long) rawResult[1];
			results.add(pollResultDTO);
		}
		return results;
	}
	
	
	public static class PollResultDTO{
		public String choice;
		public Long votes;
		public String text;
	}

}
