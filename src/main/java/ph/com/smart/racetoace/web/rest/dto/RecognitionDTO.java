/**
 *
 */
package ph.com.smart.racetoace.web.rest.dto;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ph.com.smart.racetoace.rnr.model.Recognition;

/**
 * For saving/retrieving recognitions
 * @author Jrdomingo
 * Jan 11, 2016 10:58:22 AM
 */
@JsonInclude(Include.NON_NULL)
public class RecognitionDTO {

	public Long id;
	
	public UserProfileDTO user;
	
	public UserProfileDTO recognizer;
	
	public String title;
	
	public String description;
	
	public Date date;
	
	public Long userId; //for saving a recognition;
	
	public Long rewardPoints;
	
	public RecognitionDTO(){}
	
	public RecognitionDTO(Recognition recognition){
		this.id = recognition.id;
		if(recognition.user!=null){
			this.user = new UserProfileDTO(recognition.user);
		}
		if(recognition.recognizer!=null){
			this.recognizer = new UserProfileDTO(recognition.recognizer);
		}
		this.title = recognition.title;
		this.description = recognition.description;
		this.date = recognition.dateCreated;
		this.rewardPoints = recognition.rewardPoints;
	}
	
	
	
	
}
