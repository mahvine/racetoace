/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto.mapper;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.model.UserChallenge;
import ph.com.smart.racetoace.rnr.model.UserGroup;
import ph.com.smart.racetoace.web.rest.dto.AcceptedChallengeDTO;
import ph.com.smart.racetoace.web.rest.dto.ChallengeDTO;
import ph.com.smart.racetoace.web.rest.dto.UserProfileDTO;
import ph.com.smart.racetoace.web.rest.dto.UserSessionDTO;

/**
 * @author JRDomingo
 * @since Aug 26, 2015 1:24:15 PM
 * 
 */
@Component
public class UserMapper {

	public UserSessionDTO userToUserSessionDTO(User user, String sessionToken, UserGroup userGroup){
		UserSessionDTO userSessionDTO = new UserSessionDTO();
		userSessionDTO.email = user.email;
		userSessionDTO.profile = user.profile;
		userSessionDTO.sessionToken = sessionToken;
		userSessionDTO.userGroup = userGroup;
		userSessionDTO.currentPoints = user.rewardPoints;
		userSessionDTO.totalPoints = user.scorePoints;
		return userSessionDTO;
	}
	

	public UserSessionDTO userToUserSessionDTO(User user, String sessionToken){
		return userToUserSessionDTO(user, sessionToken, null);
	}
	

	public UserSessionDTO userToUserSessionDTO(User user){
		return userToUserSessionDTO(user, null, null);
	}
	
	public List<UserSessionDTO> usersTouserSessionDTOs(List<User> users){
		List<UserSessionDTO> userSessionDTOs = new ArrayList<UserSessionDTO>();
		for(User user: users){
			userSessionDTOs.add(userToUserSessionDTO(user));
		}
		return userSessionDTOs;
	}
	
	public static List<UserProfileDTO> usersToUserProfileDTOs(List<User> users){
		List<UserProfileDTO> userProfileDTOs = new ArrayList<UserProfileDTO>();
		for(User user: users){
			userProfileDTOs.add(new UserProfileDTO(user));
		}
		return userProfileDTOs;
	}
	
	public static UserProfileDTO userToUserProfileDTO(User user){
		return new UserProfileDTO(user);
	}

	public AcceptedChallengeDTO userChallengeToAcceptedChallengeDTO(UserChallenge userChallenge){
		AcceptedChallengeDTO acceptedChallengeDTO = new AcceptedChallengeDTO();
		acceptedChallengeDTO.challenge = partialChallengeToChallengeDTO(userChallenge.challenge);
		acceptedChallengeDTO.user = userToUserProfileDTO(userChallenge.user);
		acceptedChallengeDTO.earnedPoints = userChallenge.pointsEarned;
		acceptedChallengeDTO.status = userChallenge.status;
		acceptedChallengeDTO.winner = userChallenge.winner;
		return acceptedChallengeDTO;
	}

	public List<AcceptedChallengeDTO> userChallengesToAcceptedChallengeDTOs(List<UserChallenge> userChallenges){
		List<AcceptedChallengeDTO> acceptedChallengeDTOs = new ArrayList<AcceptedChallengeDTO>();
		for(UserChallenge userChallenge: userChallenges){
			acceptedChallengeDTOs.add(userChallengeToAcceptedChallengeDTO(userChallenge));
		}
		return acceptedChallengeDTOs;
	}

	public ChallengeDTO partialChallengeToChallengeDTO(Challenge challenge){
		ChallengeDTO challengeDTO = new ChallengeDTO();
		challengeDTO.details = challenge.details;
		challengeDTO.endDate = challenge.endDate;
		challengeDTO.id = challenge.id;
		challengeDTO.startDate = challenge.startDate;
		challengeDTO.title = challenge.title;
		challengeDTO.consolationPoints = challenge.consolationPoints;
		challengeDTO.winnerPoints = challenge.winnerPoints;
		challengeDTO.penalty = challenge.penalty;
		
		return challengeDTO;
	}
	
}
