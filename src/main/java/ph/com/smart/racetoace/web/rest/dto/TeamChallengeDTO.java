/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

import java.util.Date;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.TeamChallenge;
import ph.com.smart.racetoace.rnr.model.TeamChallenge.Status;
import ph.com.smart.racetoace.rnr.model.UserTeam;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 10:36:50 AM
 * 
 */
public class TeamChallengeDTO{

	public String details;
	public Long id;
	public String title;
	public Date endDate;
	public Date startDate;
	public Long winnerPoints;
	public Status status;
	
	public TeamChallengeDTO(TeamChallenge teamChallenge, User user){
		this.details = teamChallenge.details;
		this.id = teamChallenge.id;
		this.title= teamChallenge.title;
		this.endDate = teamChallenge.endDate;
		this.startDate = teamChallenge.startDate;
		this.winnerPoints = teamChallenge.winnerPoints;
		this.status = Status.OPEN;
		if(user!=null){
			for(UserTeam userTeam : teamChallenge.teams){
				if(userTeam.members.contains(user)){
					this.status = Status.JOINED;
					if(userTeam.winner){
						this.status = Status.COMPLETED;
					}
				}
			}
			
		}
	}
	
	
}
