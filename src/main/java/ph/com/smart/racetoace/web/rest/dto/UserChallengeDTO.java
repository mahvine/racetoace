/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.model.UserChallenge.Status;
import ph.com.smart.racetoace.core.model.task.PollTask.Selection;

/**
 * To return {@link Challenge} <b>Status</b> per {@link User}
 * <p> Status are as follows:
 * <ul>
 * 	<li>OPEN</li>
 * 	<li>ACCEPTED</li>
 * 	<li>COMPLETED</li>
 * 	<li>FAILED</li>
 * 	<li>CLOSED</li>
 * </ul>
 * </p>
 * 
 * @author JRDomingo
 * @since Sep 2, 2015 1:56:40 PM
 * 
 */
@JsonInclude(Include.NON_NULL)
public class UserChallengeDTO extends BaseChallengeDTO{

	public List<UserTaskDTO> tasks = new ArrayList<UserTaskDTO>();
	
	public Status status;
	
	public Boolean winner; 
	
	@JsonInclude(Include.NON_NULL)
	public static class UserTaskDTO extends BaseTaskDTO{
		
		
		public List<Selection> selections; //POLL
		public TaskStatus status; 
	}
	
	public enum TaskStatus{
		UNDONE,DONE
	}
}
