package ph.com.smart.racetoace.web.rest.dto;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by KCAsis on 9/11/2015.
 */
public class NominateUserDTO {
    @JsonProperty(value = "nominatedId")
    public Long nominated;
    public String reason;
}
