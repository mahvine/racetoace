/**
 * 
 */
package ph.com.smart.racetoace.web.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.Order;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.core.model.CompletedTask;
import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.model.UserChallenge;
import ph.com.smart.racetoace.core.model.UserChallenge.Status;
import ph.com.smart.racetoace.core.repository.ChallengeRepository;
import ph.com.smart.racetoace.core.repository.CompletedTaskRepository;
import ph.com.smart.racetoace.core.repository.UserChallengeRepository;
import ph.com.smart.racetoace.rnr.service.UserChallengeService;
import ph.com.smart.racetoace.web.rest.dto.UserChallengeDTO;
import ph.com.smart.racetoace.web.rest.dto.mapper.UserChallengeDTOTranslator;

/**
 * @author JRDomingo
 * @since Sep 2, 2015 10:14:58 AM
 * 
 */
@RestController
@RequestMapping("/api")
public class UserChallengeResource {

	@Inject
	UserResource userResource;
	
	@Inject
	UserChallengeService userChallengeService;
	
	@Inject
	ChallengeRepository challengeRepository;
	
	@Inject
	UserChallengeRepository userChallengeRepository;
	
	@Inject
	CompletedTaskRepository completedTaskRepository;
	
	@RequestMapping(value="/userChallenges/tasks", method=RequestMethod.POST)
	public void completeTask(@RequestHeader("X-session") String sessionToken,
			@RequestBody CompleteTaskDTO request){
		
		User user = userResource.getUser(sessionToken);
		if(request.choice!=null){
			userChallengeService.completePollTask(user, request.taskId, request.choice);
		}else if(request.answer!=null){
			userChallengeService.completeQuizTask(user, request.taskId, request.answer);
		}else if(request.code!=null){
			userChallengeService.completeSimpleTask(user, request.taskId, request.code);
		}else if(request.longitude != null && request.latitude != null){
			userChallengeService.completeLocationTask(user, request.taskId, request.longitude, request.latitude);
		}
	}
	

	@RequestMapping(value="/userChallenges", method=RequestMethod.POST)
	public void acceptChallenge(@RequestHeader("X-session") String sessionToken,
			@RequestBody AcceptChallengeDTO request){
		
		User user = userResource.getUser(sessionToken);
		Challenge challenge = challengeRepository.findOne(request.challengeId);
		userChallengeService.acceptChallenge(user, challenge);
	}
	
	

	@RequestMapping(value="/userChallenges", method=RequestMethod.GET)
	public List<UserChallengeDTO>  getUserChallenges(@RequestHeader("X-session") String sessionToken, 
			@RequestParam(value="status",required=false) Status statusFilter){
		Sort sort = new Sort( new Order(Direction.DESC, "startDate"),
					new Order(Direction.ASC, "endDate"));
		
		User user = userResource.getUser(sessionToken);
		Date today = new Date();
		List<UserChallengeDTO> userChallengeDTOs = null;
		
//		List<Challenge> challenges = challengeRepository.findAll(sort);
		if(statusFilter!=null){

			List<UserChallenge> userChallenges = userChallengeRepository.findByUserAndStatus(user,statusFilter);
			List<Challenge> challenges = new ArrayList<Challenge>();
			for(UserChallenge userChallenge:userChallenges){
				challenges.add(userChallenge.challenge);
			}
			List<CompletedTask> completedTasks = completedTaskRepository.findByUserAndTaskChallengeIn(user, challenges);
			
			//enrich
			userChallengeDTOs = UserChallengeDTOTranslator.translate(challenges, userChallenges, completedTasks, today,statusFilter);
		}else{
			List<Challenge> challenges = challengeRepository.findByStartDateBeforeAndEndDateAfter(today, today, sort);
			List<UserChallenge> userChallenges = userChallengeRepository.findByUser(user);
			List<CompletedTask> completedTasks = completedTaskRepository.findByUser(user);
			//enrich
			userChallengeDTOs = UserChallengeDTOTranslator.translate(challenges, userChallenges, completedTasks, today,statusFilter);
		}
		
		
		
		return userChallengeDTOs;
	}
	
	//TODO optimize me
	@RequestMapping(value="/userChallenges/{id}", method=RequestMethod.GET)
	public UserChallengeDTO  getUserChallengesById(@RequestHeader("X-session") String sessionToken, 
			@RequestParam(value="status",required=false) Status statusFilter,
			@PathVariable("id") Long challengeId){
		List<UserChallengeDTO> userChallengeDTOs = getUserChallenges(sessionToken, statusFilter);
		for(UserChallengeDTO userChallengeDTO: userChallengeDTOs){
			if(userChallengeDTO.id.equals(challengeId)){
				return userChallengeDTO;
			}
		}
		
		return null;
	}
	
	
	
	public static class AcceptChallengeDTO{
		
		public Long challengeId;
		
	}
	
	public static class CompleteTaskDTO{
		public Long taskId;
		public String code;
		public String answer;
		public String choice;
		public Float longitude;
		public Float latitude;
	}
	
}
