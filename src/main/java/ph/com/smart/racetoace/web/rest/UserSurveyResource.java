package ph.com.smart.racetoace.web.rest;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codahale.metrics.annotation.Timed;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.Survey;
import ph.com.smart.racetoace.rnr.model.Survey.Status;
import ph.com.smart.racetoace.rnr.model.SurveyAnswer;
import ph.com.smart.racetoace.rnr.model.SurveyQuestion;
import ph.com.smart.racetoace.rnr.model.UserSurvey;
import ph.com.smart.racetoace.rnr.repository.SurveyAnswerRepository;
import ph.com.smart.racetoace.rnr.repository.SurveyRepository;
import ph.com.smart.racetoace.rnr.repository.UserSurveyRepository;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.SurveyResponseAlreadyExists;
import ph.com.smart.racetoace.web.rest.dto.RespondToSurveyDTO;
import ph.com.smart.racetoace.web.rest.dto.RespondToSurveyDTO.SurveyAnswerDTO;

@RestController
@RequestMapping("/api")
public class UserSurveyResource {
	
	private static Logger logger = LoggerFactory.getLogger(UserScoreResource.class);
	
	@Inject
	SurveyRepository surveyRepository;
	
	@Inject
	UserResource userResource;
	
	@Inject
	UserSurveyRepository userSurveyRepository;
	
	@Inject
	SurveyAnswerRepository surveyAnswerRepository;

	@Timed
	@RequestMapping(value="/userSurveys", method=RequestMethod.GET)
	public List<Survey> list(@RequestHeader("X-session") String sessionToken){
		User user = userResource.getUser(sessionToken);
		Date today = new Date();
		List<Survey> openSurveys = surveyRepository.findByStartDateBeforeAndEndDateAfter(today, today);
		
		List<UserSurvey> userSurveys = userSurveyRepository.findByUserAndSurveyIn(user, openSurveys);
		logger.debug("found {} open surveys",openSurveys.size());
		for(Survey survey: openSurveys){
			survey.status = Survey.Status.OPEN;
			for(UserSurvey userSurvey: userSurveys){
				if(survey.id.equals(userSurvey.survey.id)){
					survey.status = Survey.Status.COMPLETED;
				}
			}
		}
		//remove open
		for(int i=openSurveys.size()-1;i>0;i--){
			Survey survey = openSurveys.get(i);
			if(survey.status.equals(Status.COMPLETED)){
				openSurveys.remove(survey);
			}
		}
		
		return openSurveys;
	}

	@Timed
	@RequestMapping(value="/userSurveys/{id}", method=RequestMethod.GET)
	public Survey getById(@RequestHeader("X-session") String sessionToken, @PathVariable("id") Long surveyId){
		User user = userResource.getUser(sessionToken);
		Survey surveyById = surveyRepository.findOne(surveyId);
		List<Survey> openSurveys = new ArrayList<Survey>();
		openSurveys.add(surveyById);
		List<UserSurvey> userSurveys = userSurveyRepository.findByUserAndSurveyIn(user, openSurveys);
		if(userSurveys.isEmpty()){
			surveyById.status = Status.OPEN;
		}else{
			surveyById.status = Status.COMPLETED;
		}
		return surveyById;
	}
	
	@Timed
	@RequestMapping(value="/userSurveys", method=RequestMethod.POST)
	public void respond(@RequestHeader("X-session") String sessionToken, @RequestBody RespondToSurveyDTO request){
		User user = userResource.getUser(sessionToken);
		Survey survey = surveyRepository.findOne(request.surveyId);
		UserSurvey userSurvey = userSurveyRepository.findByUserAndSurvey(user, survey);
		
		Map<Long, SurveyQuestion> questionMap = new HashMap<Long, SurveyQuestion>();
		
		for(SurveyQuestion question : survey.questions){
			questionMap.put(question.id, question);
		}
		
		if(userSurvey==null){
			userSurvey = new UserSurvey();
			userSurvey.dateCreated = new Date();
			userSurvey.user = user;
			userSurvey.survey = survey;
			userSurvey.comment = request.comment;
			userSurveyRepository.save(userSurvey);
			
			List<SurveyAnswer> surveyAnswers = new ArrayList<SurveyAnswer>();
			
			for(SurveyAnswerDTO answer:request.answers){
				SurveyAnswer surveyAnswer = new SurveyAnswer();
				surveyAnswer.question = questionMap.get(answer.questionId);
				surveyAnswer.rating = answer.rating;
				surveyAnswer.user = user;
				if(surveyAnswer.question != null){
					surveyAnswers.add(surveyAnswer);
				}
			}
			surveyAnswerRepository.save(surveyAnswers);
		}else{
			throw new SurveyResponseAlreadyExists();
		}
		
	}

}
