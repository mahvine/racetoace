/**
 * 
 */
package ph.com.smart.racetoace.web.rest.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * @author JRDomingo
 * @since Sep 2, 2015 2:05:07 PM
 * 
 */
@JsonInclude(Include.NON_NULL)
public class BaseChallengeDTO {

	public Long id;
	
	@NotNull
	@Size(min=1,message="Invalid title")
	public String title;
	
	@NotNull
	@Size(min=1,message="Invalid title")
	public String details;

	public Date startDate;
	
	public Date endDate;
	
	public Long winnerPoints;
	
	public Long consolationPoints;
	
	public Long penalty;
}
