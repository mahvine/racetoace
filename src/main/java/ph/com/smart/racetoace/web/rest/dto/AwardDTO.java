package ph.com.smart.racetoace.web.rest.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.Award;
import ph.com.smart.racetoace.rnr.model.Award.Status;
import ph.com.smart.racetoace.rnr.model.Nomination;
import ph.com.smart.racetoace.rnr.model.UserGroup;

/**
 * Created by KCAsis on 9/9/2015.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AwardDTO {

    public Long id;
    public String name; // title
    public String description;
    public Status status;
    public Date startDate;
    public Date endDate;
    public long rewardPoints;
    public Award.Type type;
    public NominatedUserWithProfileDTO nomination;
    public UserGroup userGroup;
    
    public List<UserProfileDTO> winners;

    public AwardDTO() {}
    

    public AwardDTO(Award award) {
        this.id = award.id;
        this.name = award.name;
        this.description = award.description;
        this.startDate = award.startDate;
        this.endDate = award.endDate;
        this.status = award.status;
        this.rewardPoints = award.rewardPoints;
        this.type = award.type;
        this.userGroup = award.userGroup;
        if(award.winners!=null){
        	this.winners = new ArrayList<UserProfileDTO>();
        	for(User winner: award.winners){
        		winners.add(new UserProfileDTO(winner));
        	}
        }
    }

    public AwardDTO(Award award, Nomination nomination) {
    	this(award);
        this.nomination = nomination != null ? new NominatedUserWithProfileDTO(nomination) : null;
    }
}
