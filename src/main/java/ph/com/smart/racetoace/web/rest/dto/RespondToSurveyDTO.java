/**
 *
 */
package ph.com.smart.racetoace.web.rest.dto;

import java.util.List;

/**
 * @author Jrdomingo
 * Jan 12, 2016 7:19:34 PM
 */
public class RespondToSurveyDTO {

	public Long surveyId;
	
	public String comment;
	
	public List<SurveyAnswerDTO> answers;
	
	public static class SurveyAnswerDTO{
		public Long questionId;
		public Integer rating;
	}
}
