/**
 * 
 */
package ph.com.smart.racetoace.rnr.repository;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ph.com.smart.racetoace.rnr.model.Redemption;

import java.util.List;

/**
 * @author JRDomingo
 * @since Sep 3, 2015 11:13:35 AM
 * 
 */
@Repository
public interface RedemptionRepository extends JpaRepository<Redemption, Long>{

    Redemption findByCode(String code);

    List<Redemption> findByUserId(Long userId);
    List<Redemption> findByUserId(Long userId, Sort sort);

    List<Redemption> findByRewardItemId(Long rewardItemId);

}
