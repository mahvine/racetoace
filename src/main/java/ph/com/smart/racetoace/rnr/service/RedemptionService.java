package ph.com.smart.racetoace.rnr.service;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.hashids.Hashids;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.rnr.model.Redemption;
import ph.com.smart.racetoace.rnr.model.RewardItem;
import ph.com.smart.racetoace.rnr.repository.RedemptionRepository;
import ph.com.smart.racetoace.rnr.repository.RewardItemRepository;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.InsufficientRewardsPoints;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.InvalidRedemptionCode;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.ItemsOutOfStock;

/**
 * Created by KCAsis on 9/3/2015.
 */
@Service
public class RedemptionService {

    private static final Logger logger = LoggerFactory.getLogger(RedemptionService.class);
    private static final String HASH = "r@c3+04c3";
    private static final String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
    // all longs (64bit) are 1.8446744e+19 in length
    private static final Hashids hasher = new Hashids(HASH, 8, ALPHABET); //min len of 8, 36^8 ~ 2.8211099e+12 entries, alphabet is now only uppercase

    @Inject
    UserRepository userRepository;

    @Inject
    RewardItemRepository rewardItemRepository;

    @Inject
    RedemptionRepository redemptionRepository;

    public synchronized String redeemRewardItem(Long userId, Long rewardItemId) { //so that only one thread at a time can only deduct points
        User user = userRepository.findOne(userId);
        RewardItem item = rewardItemRepository.findOne(rewardItemId);
        String retval = null;
        if (user.rewardPoints >= item.pointRequirement) {
            if (item.quantity > 0) {
                item.quantity--;
                user.rewardPoints -= item.pointRequirement;
                if (userRepository.save(user) == null || rewardItemRepository.save(item) == null) {
                    throw new RuntimeException("Unable to redeem your points. Please try again later");
                }
                logger.info("{} points was deducted to User:{} for redeeming RewardItem:{}",item.pointRequirement,user.id,item.id);

                //save redemption record
                Redemption redemption = new Redemption();
                redemption.dateCreated = new Date();
                redemption.rewardItem = item;
                redemption.user = user;
                redemption = redemptionRepository.save(redemption);
                // make redemption code unique
                String itemName = item.name.replace(" ", "").toUpperCase();
                redemption.code = itemName.length() >= 4 ? itemName.toUpperCase().substring(0,4) : itemName.toUpperCase() + "xxxx".substring(itemName.length() <= 4 ? itemName.length() : 4);
                redemption.code += "-" + hasher.encode(redemption.id);
                redemptionRepository.save(redemption);
                retval = redemption.code;
                logger.info("Created redemption record with id:{} for redeeming RewardItem:{}",redemption.id, item.id);
            } else {
                throw new ItemsOutOfStock();
            }
        } else {
            throw new InsufficientRewardsPoints();
        }
        return retval;
    }

    public synchronized void confirmRedemption(String code) {
//        long[] result = hasher.decode(code.substring(5)); // [redemptionId]
        Redemption redemption = redemptionRepository.findByCode(code);
        logger.info("Attempting to claim Redemption with userId");
        if (redemption != null && redemption.dateClaimed == null) {
            redemption.dateClaimed = new Date();
            redemptionRepository.save(redemption);
        } else {
            throw new InvalidRedemptionCode();
        }
    }
    
    public Redemption getRedemptionByCode(String code){
        Redemption redemption = redemptionRepository.findByCode(code);
        return redemption;
    }

    public List<Redemption> getRedeemedItemsforUserId(Long userId) {
        return userId != null ? redemptionRepository.findByUserId(userId)
                : redemptionRepository.findAll();
    }

    public List<Redemption> getRedeemedItemsForRewardItemId(Long rewardItemId) {
        if (rewardItemId != null) {
            return redemptionRepository.findByRewardItemId(rewardItemId);
        } else {
            throw new IllegalArgumentException("rewardItemId must not be null!");
        }
    }
}
