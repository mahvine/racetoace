package ph.com.smart.racetoace.rnr.model;

import javax.persistence.*;

/**
 * 
 * @author JRDomingo
 * @since Aug 24, 2015 9:24:15 AM
 *
 */
@Entity
public class RewardItem {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;

	public String name;
	
	public String description;

	@Column(name = "point_requirement")
	public Long pointRequirement;

	public int quantity;

	public String resourcePath;
	
}
