/**
 *
 */
package ph.com.smart.racetoace.rnr.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ph.com.smart.racetoace.rnr.model.UserGroup;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 4:40:27 PM
 */
public interface UserGroupRepository extends JpaRepository<UserGroup, Long> {

    UserGroup findByUsersId(Long userId);

}
