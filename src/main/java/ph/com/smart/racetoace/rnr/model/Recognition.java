package ph.com.smart.racetoace.rnr.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ph.com.smart.racetoace.core.model.User;

/**
 * primary model for recognizing users for deeds they did
 * @author JRDomingo
 * Jan 11, 2016 9:57:11 AM
 * 
 */
@Entity
@Table(name="recognition")
public class Recognition {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String title;
	
	public String description;
	
	@ManyToOne
	@JoinColumn(name="user_id",referencedColumnName="id")
	public User user;
	
	@Column(name="reward_points")
	public Long rewardPoints;
	
	@ManyToOne
	@JoinColumn(name="recognizer_id",referencedColumnName="id")
	public User recognizer;
	
	@Column(name="date_created")
	public Date dateCreated;

	@Override
	public String toString() {
		return "{" + (id != null ? "id:" + id + ", " : "") + (title != null ? "title:" + title + ", " : "")
				+ (description != null ? "description:" + description + ", " : "")
				+ (user != null ? "user:" + user + ", " : "")
				+ (rewardPoints != null ? "rewardPoints:" + rewardPoints + ", " : "")
				+ (recognizer != null ? "recognizer:" + recognizer + ", " : "")
				+ (dateCreated != null ? "dateCreated:" + dateCreated : "") + "}";
	}

	

}