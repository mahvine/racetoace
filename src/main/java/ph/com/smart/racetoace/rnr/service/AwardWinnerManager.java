package ph.com.smart.racetoace.rnr.service;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.notification.gcm.GCMNotifierService;
import ph.com.smart.racetoace.rnr.model.Award;
import ph.com.smart.racetoace.rnr.model.Award.Status;
import ph.com.smart.racetoace.rnr.model.Nomination;
import ph.com.smart.racetoace.rnr.model.UserGroup;
import ph.com.smart.racetoace.rnr.repository.AwardRepository;
import ph.com.smart.racetoace.rnr.repository.NominationRepository;
import ph.com.smart.racetoace.rnr.repository.UserGroupRepository;

/**
 * Created by KCAsis on 9/10/2015.
 */
@Component
public class AwardWinnerManager {

	private static final Logger logger = LoggerFactory.getLogger(AwardService.class);

	@Inject
	AwardRepository awardRepository;
	
	@Inject
	NominationRepository nominationRepository;
	
	@Inject
	UserGroupRepository userGroupRepository;
	
	@Inject
	UserRepository userRepository;
	
	@Inject
	PointService pointService;
	
	@Inject
	GCMNotifierService gcmNotifierService;
	
    public void getWinnersPerAwardPerGroup(Long awardId) {

        final long currentTime = new Date().getTime();

        // Map<GroupId, Map<UserId, User>>
        HashMap<Long, HashMap<Long, User>> userGroupMap = new HashMap<>();

        Award award = awardRepository.findOne(awardId);
        List<Nomination> nominations = nominationRepository.findByAward(award);

        // initialize data structs
        for (UserGroup ug : userGroupRepository.findAll()) {
            userGroupMap.put(ug.id, new HashMap<Long, User>());
        }
//        award.pendingWinnerMap = new HashMap<>(); // Map<userId, User>

        // compute nominations - nominations are guaranteed to be tied to the awardId
        for (Nomination n : nominations) {
            if (n.date.getTime() <= award.endDate.getTime() && award.endDate.getTime() <= currentTime) {
                User tempNominee = userGroupMap.get(n.nominated.userGroup.id).get(n.nominated.id);
                User nominee =  tempNominee == null ? n.nominated : tempNominee;
                nominee.nominations++;
                userGroupMap.get(nominee.userGroup.id).put(nominee.id, nominee);
            }
        }

        for (HashMap<Long, User> maps : userGroupMap.values()) {
            LinkedList<User> users = new LinkedList<>(maps.values());
            if (users.size() == 0) continue;
            Collections.sort(users, new Comparator<User>() {
                @Override
                public int compare(User o1, User o2) {
                    if (o1.nominations == o2.nominations) {
                        return 0;
                    } else return o1.nominations > o2.nominations ? 1 : -1;
                }
            });
        }
    }

    public void processWinnersAndPoints(Long awardId) {
        // Object[3] {UserGroup.id, Nomination.nominated, votes}
        // TODO: we are not processing the date
        List<Object[]> resultSet = awardRepository.processWinnersPerAwardId(awardId);
        LinkedList<User> tempList = new LinkedList<>();
        for (Object[] resultRow : resultSet) {
            User nominatedUser = (User) resultRow[0];
            long votes = (long) resultRow[1];
            nominatedUser.nominations = votes;
                tempList.add(nominatedUser);
        }
        Collections.sort(tempList, new Comparator<User>() {
            @Override
            public int compare(User o1, User o2) {
                if (o1.nominations == o2.nominations) return 0;
                else return o1.nominations > o2.nominations ? -1 : 1; // reverse sort
            }
        });
        if(!tempList.isEmpty()){
	        // Get top and account for ties
	        long topVoteCount = tempList.get(0).nominations;
	
	    	logger.info("top vote count:{} for award:{} ", topVoteCount,awardId);
	        for (User u : tempList) {
	            if (u.nominations < topVoteCount) break;
	            Award a = awardRepository.findOne(awardId);
	            a.winners.add(u);
	            awardRepository.save(a);
	            // Process points awarded
	            pointService.addRewardPoints(u.id, a.rewardPoints);
	            pointService.addScorePoints(u.id, a.rewardPoints);
	            logger.info("added {} points to user:{} for winning award:{} ",a.rewardPoints, u.id,awardId);
	        }
        }
    }
    
    @Scheduled(cron="${app.process.due.awards.cron}")
    public void processAllOpenAwards(){
    	Date date = new Date();
    	List<Award> dueAwards = awardRepository.findByEndDateLessThanAndStatus(date, Status.OPEN);
    	logger.info("processing all awards:{} due by {}", dueAwards.size(), date);
    	for(Award award : dueAwards){
    		processWinnersAndPoints(award.id);
    		award.status = Status.CLOSED;
    		awardRepository.save(award);
    	}
    	
    }
    

}
