package ph.com.smart.racetoace.rnr.model;

import java.util.Date;

import javax.persistence.*;

import ph.com.smart.racetoace.core.model.User;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 9:24:11 AM
 *
 */
@Entity
@Table(name="nomination")
public class Nomination {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public Date date = new Date();

	@Column(columnDefinition = "VARCHAR(512)")
	public String reason;

	@ManyToOne
	@JoinColumn(name="award_id", referencedColumnName="id")
	public Award award;

	@ManyToOne
	@JoinColumn(name="nominator_id", referencedColumnName="id")
	public User nominator;

	@ManyToOne
	@JoinColumn(name="nominated_id", referencedColumnName="id")
	public User nominated;
}
