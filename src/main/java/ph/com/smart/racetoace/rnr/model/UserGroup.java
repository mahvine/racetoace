/**
 * 
 */
package ph.com.smart.racetoace.rnr.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ph.com.smart.racetoace.core.model.User;

/**
 * Groupings in RnR
 * @author JRDomingo
 * @since Aug 24, 2015 4:27:06 PM
 * 
 */
@Entity
@Table(name="user_group")
public class UserGroup {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String name;
	
	public String description;

	@JsonIgnore
	@OneToMany
	@JoinColumn(name="user_group_id", referencedColumnName="id")
	public List<User> users = new ArrayList<User>();


}
