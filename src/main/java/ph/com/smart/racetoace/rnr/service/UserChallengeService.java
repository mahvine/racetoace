/**
 * 
 */
package ph.com.smart.racetoace.rnr.service;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.core.model.CompletedTask;
import ph.com.smart.racetoace.core.model.Task;
import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.model.UserChallenge;
import ph.com.smart.racetoace.core.model.UserChallenge.Status;
import ph.com.smart.racetoace.core.model.task.LocationTask;
import ph.com.smart.racetoace.core.model.task.PollFeedback;
import ph.com.smart.racetoace.core.model.task.PollTask;
import ph.com.smart.racetoace.core.model.task.PollTask.Selection;
import ph.com.smart.racetoace.core.model.task.QuizTask;
import ph.com.smart.racetoace.core.model.task.SimpleTask;
import ph.com.smart.racetoace.core.repository.ChallengeRepository;
import ph.com.smart.racetoace.core.repository.CompletedTaskRepository;
import ph.com.smart.racetoace.core.repository.UserChallengeRepository;
import ph.com.smart.racetoace.core.task.repository.LocationTaskRepository;
import ph.com.smart.racetoace.core.task.repository.PollFeedbackRepository;
import ph.com.smart.racetoace.core.task.repository.PollTaskRepository;
import ph.com.smart.racetoace.core.task.repository.QuizTaskRepository;
import ph.com.smart.racetoace.core.task.repository.SimpleTaskRepository;
import ph.com.smart.racetoace.notification.PushGateway;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.CannotCompleteTask;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.ChallengeAlreadyAccepted;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.InvalidPollFeedbackChoice;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.TaskAlreadyCompleted;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.TaskNotFound;
import ph.com.smart.racetoace.web.rest.dto.UserProfileDTO;

/**
 * Service that: <br>
 * <p>
 * 1.) Let a {@link User} accept a {@link Challenge}<br>
 * 2.) {@link User} complete a {@link SimpleTask}<br>
 * 3.) {@link User} complete a {@link QuizTask}<br>
 * 4.) {@link User} complete a {@link PollTask}<br>
 *   
 * @author JRDomingo
 * @since Sep 2, 2015 10:27:36 AM
 * 
 */
@Service
public class UserChallengeService {

	private static final Logger logger = LoggerFactory.getLogger(UserChallengeService.class);
	
	@Inject
	public UserChallengeRepository userChallengeRepository;
	
	@Inject
	public SimpleTaskRepository simpleTaskRepository;

	@Inject
	public QuizTaskRepository quizTaskRepository;

	@Inject
	public PollTaskRepository pollTaskRepository;
	
	@Inject
	public ChallengeRepository challengeRepository;

	@Inject
	public CompletedTaskRepository completedTaskRepository;
	
	@Inject
	public PollFeedbackRepository pollFeedbackRepository;
	
	@Inject
	public LocationTaskRepository locationTaskRepository;
	
	@Inject
	public PointService pointService;
	
	@Inject
	public PushGateway pushGateway;
	
	public void acceptChallenge(User user, Challenge challenge){
		logger.debug("Attempt to accept Challenge:{} by User:{}", challenge.id, user.id);
		UserChallenge userChallenge = userChallengeRepository.findByUserAndChallenge(user, challenge);
		if(userChallenge == null){
			userChallenge = new UserChallenge();
			userChallenge.challenge = challenge;
			userChallenge.user = user;
			userChallenge.date = new Date();
			userChallenge.status = Status.ACCEPTED;
			userChallenge.winner = false;
			userChallengeRepository.save(userChallenge);
			logger.info("User challenge created:{}", userChallenge);
		}else{
			logger.debug("Challenge:{} already accepted by User:{}", challenge.id, user.id);
			throw new ChallengeAlreadyAccepted();
		}
	}
	
	/**
	 * Helper method for validation purposes
	 * @param task
	 * @param taskId
	 * @param userId
	 */
	private void checkIfTaskIsNotNull(Task task, Long taskId, Long userId){
		if(task==null){
			logger.info("Failed to complete a Task:{} by User:{} because task is not found", taskId, userId);
			throw new TaskNotFound(taskId);
		}
	}
	
	/**
	 * Helper method for validation purposes
	 * @param task
	 * @param user
	 */
	private void checkIfTaskIsTaskIsCompleted(Task task, User user){
		CompletedTask completedTask = completedTaskRepository.findByTaskAndUser(task, user);
		if(completedTask != null){ 
			logger.info("Task:{} is already completed by User:{}", task.id,user.id);
			throw new TaskAlreadyCompleted();
		}
		logger.debug("Task:{} is not yet completed by user:{}", task.id,user.id);
	}

	public void completeSimpleTask(User user, Long simpleTaskId, String code){
		logger.debug("Attempt to complete a Simple task:{} by User:{}", simpleTaskId, user.id);
		SimpleTask simpleTask = simpleTaskRepository.findOne(simpleTaskId);
		checkIfTaskIsNotNull(simpleTask, simpleTaskId, user.id);
		checkIfTaskIsTaskIsCompleted(simpleTask, user);
				
		if(simpleTask.code.equalsIgnoreCase(code.trim())){
			CompletedTask completedTask = new CompletedTask();
			completedTask.date = new Date();
			completedTask.task = simpleTask;
			completedTask.user = user;
			
			completedTaskRepository.save(completedTask);
			logger.info("Completed a Simple task:{} by User:{}", simpleTaskId, user.id);
			checkAndTagIfChallengeIsCompleted(simpleTask, user);
		}else{
			throw new CannotCompleteTask("Invalid code");
		}
	}

	

	public void completeQuizTask(User user, Long quizTaskId, String answer){
		logger.debug("Attempt to complete a Quiz task:{} by User:{}", quizTaskId, user.id);
		QuizTask quizTask = quizTaskRepository.findOne(quizTaskId);
		checkIfTaskIsNotNull(quizTask, quizTaskId, user.id);
		checkIfTaskIsTaskIsCompleted(quizTask, user);
		
		if(quizTask.answer.equalsIgnoreCase(answer.trim())){
			CompletedTask completedTask = new CompletedTask();
			completedTask.date = new Date();
			completedTask.task = quizTask;
			completedTask.user = user;
			completedTaskRepository.save(completedTask);
			logger.info("Completed a Quiz task:{} by User:{}", quizTaskId, user.id);

			checkAndTagIfChallengeIsCompleted(quizTask, user);
		}else{
			throw new CannotCompleteTask("Invalid answer");
		}
		
	}

	public void completePollTask(User user, Long pollTaskId, String choice){
		logger.debug("Attempt to complete a Poll task:{} by User:{}", pollTaskId, user.id);
		choice = choice.toUpperCase().trim();
		PollTask pollTask = pollTaskRepository.findOne(pollTaskId);
		checkIfTaskIsNotNull(pollTask, pollTaskId, user.id);
		checkIfTaskIsTaskIsCompleted(pollTask, user);
		boolean validChoice = false;
		for(Selection selection: pollTask.selections){
			selection.choice = selection.choice.toUpperCase().trim();
			if(choice.equalsIgnoreCase(selection.choice)){
				validChoice = true;
			}
		}
		if(validChoice){
			
		}else{
			logger.info("Cannot complete task invalid selection choice:{} for poll:{} by user:{}", choice, pollTask.id,user.id);
			throw new InvalidPollFeedbackChoice("Invalid selection choice");
		}
		
		PollFeedback pollFeedback = new PollFeedback();
		pollFeedback.choice = choice;
		pollFeedback.date = new Date();
		pollFeedback.pollTask = pollTask;
		pollFeedback.user = user;
		pollFeedbackRepository.save(pollFeedback);

		logger.info("Save PollFeedback with Choice:{} for Poll:{} by User:{}", choice, pollTask.id,user.id);

		CompletedTask completedTask = new CompletedTask();
		completedTask.date = new Date();
		completedTask.task = pollTask;
		completedTask.user = user;
		completedTaskRepository.save(completedTask);
		logger.info("Completed PollTask:{} by User:{}",pollTask.id,user.id);

		checkAndTagIfChallengeIsCompleted(pollTask, user);
	}
	
	public void completeLocationTask(User user, Long locationTaskId, float longitude, float latitude){
		
		logger.debug("Attempt to complete a Location task:{} by User:{}", locationTaskId, user.id);
		LocationTask locationTask = locationTaskRepository.findOne(locationTaskId);
		double distance = distance(locationTask.latitude, locationTask.longitude, latitude, longitude, "m");
		logger.debug("task lat-long:{}-{}\n user lat-long: {}-{}",locationTask.latitude,locationTask.longitude,latitude, longitude);
		if(distance <= locationTask.radius){

			CompletedTask completedTask = new CompletedTask();
			completedTask.date = new Date();
			completedTask.task = locationTask;
			completedTask.user = user;
			completedTaskRepository.save(completedTask);
			logger.info("Completed LocationTask:{} by User:{}",locationTask.id,user.id);
		}else{
			int distanceRoundOff = (int) distance;
			throw new CannotCompleteTask(distanceRoundOff+" meters away from the approximate location keep looking");
		}
		
		
	}
	
	
	
	public double distance(float lat1, float lon1, float lat2, float lon2, String unit) {
		    double radlat1 = Math.PI * lat1/180;
		    double radlat2 = Math.PI * lat2/180;
		    double radlon1 = Math.PI * lon1/180;
		    double radlon2 = Math.PI * lon2/180;
		    double theta = lon1-lon2;
		    double radtheta = Math.PI * theta/180;
		    double dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
		    dist = Math.acos(dist);
		    dist = dist * 180/Math.PI;
		    dist = dist * 60 * 1.1515;
		    if(unit!=null){
			    if (unit.equalsIgnoreCase("K")) { 
			    	dist = dist * 1.609344; 
			    } else if (unit=="N") { 
			    	dist = dist * 0.8684 ;
			    } else { 
			    	dist = dist * 1609.344; 
			    };
		    }else{
		    	dist = dist * 1609.344;
		    }
		    return dist;
		}

	
	
	
	@Async
	private synchronized void checkAndTagIfChallengeIsCompleted(Task task, User user){
		Challenge challenge = challengeRepository.findByTasksId(task.id);
		if(challenge!=null){
			if(challenge.endDate.getTime() >= new Date().getTime()){
				UserChallenge userChallenge = userChallengeRepository.findByUserAndChallenge(user, challenge);
				List<CompletedTask> completedTasks = completedTaskRepository.findByUserAndTaskChallenge(user, challenge);
				if(challenge.tasks.size() == completedTasks.size()){
					if(userChallenge != null){
						long numberOfWhoCompleted = userChallengeRepository.countByChallengeAndStatus(challenge, Status.COMPLETED);
						userChallenge.status = Status.COMPLETED;
						userChallenge.user = user;
						userChallenge.challenge = challenge;
						long pointsEarned  = 0;
						if(numberOfWhoCompleted == 0){
							userChallenge.pointsEarned = challenge.winnerPoints;
							pointsEarned =  challenge.winnerPoints;
							userChallenge.winner = true;
							pushGateway.notifyChallengeHasWinner(challenge, new UserProfileDTO(user));
						}else{
							pointsEarned =  challenge.consolationPoints;
							userChallenge.winner = false;
						}

						userChallenge.pointsEarned = pointsEarned;
						userChallenge.dateCompleted = new Date();
						userChallengeRepository.save(userChallenge);
//						add points here
						logger.info("Adding points:{} user:{}", pointsEarned, user.id);
						pointService.addRewardPoints(user.id, pointsEarned);
						pointService.addScorePoints(user.id, pointsEarned);

						logger.info("Challenge:{} was completed by User:{}", challenge.id, user.id);
					}else{
						logger.info("Cannot find challenge for task id:{}", task.id);
					}
				}else{
					logger.info("Challenge:{} is not yet complete by User:{} remaining tasks:{}",challenge.id,user.id, (challenge.tasks.size()-completedTasks.size()));
				}
			}else{
				logger.info("Challenge:{} was not completed within the given time by User:{}",user.id);
			}
		}else{

			logger.info("No challenge was found for task:{}",task.id);
		}
	}
	

}
