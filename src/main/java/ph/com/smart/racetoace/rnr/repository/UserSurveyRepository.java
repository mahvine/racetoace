package ph.com.smart.racetoace.rnr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.Survey;
import ph.com.smart.racetoace.rnr.model.UserSurvey;
import ph.com.smart.racetoace.rnr.model.UserSurvey.UserSurveyPK;

@Repository
public interface UserSurveyRepository extends JpaRepository<UserSurvey,UserSurveyPK>{

	List<UserSurvey> findByUserAndSurveyIn(User user, List<Survey> surveys);

	List<UserSurvey> findBySurvey(Survey survey);

	UserSurvey findByUserAndSurvey(User user, Survey survey);
	
}
