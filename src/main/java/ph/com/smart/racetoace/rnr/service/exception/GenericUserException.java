/**
 * 
 */
package ph.com.smart.racetoace.rnr.service.exception;

/**
 * @author JRDomingo
 * @since Aug 27, 2015 11:05:47 AM
 * 
 */
@SuppressWarnings("serial")
public class GenericUserException extends RuntimeException{

	private String code;
	
	/**
	 * 
	 */
	public GenericUserException(String message, String code) {
		super(message);
		this.setCode(code);
	}

	/**
	 * @param message
	 * @param cause
	 * @param enableSuppression
	 * @param writableStackTrace
	 */
	public GenericUserException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 * @param cause
	 */
	public GenericUserException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param message
	 */
	public GenericUserException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param cause
	 */
	public GenericUserException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	public static class InvalidResetKey extends GenericUserException{
		public InvalidResetKey(){
			super("Invalid reset key", "1001");
		}
	}
	

	public static class InvalidPassword extends GenericUserException{
		public InvalidPassword(){
			super("Invalid credentials. Please check your email and password.", "1002");
		}
	}

	public static class ChallengeAlreadyAccepted extends GenericUserException{
		public ChallengeAlreadyAccepted(){
			super("Challenge already accepted", "1003");
		}
	}
	
	public static class TaskNotFound extends GenericUserException{
		public TaskNotFound(Long taskId){
			super("Task does not exist with id:"+taskId, "1004");
		}
	}

	public static class CannotCompleteTask extends GenericUserException{
		
		public CannotCompleteTask(String message){
			super(message, "1005");
		}
	}

	public static class TaskAlreadyCompleted extends GenericUserException{
		public TaskAlreadyCompleted(){
			super("Task already completed", "1006");
		}
	}

	public static class DuplicatePollFeedbackChoice extends GenericUserException{
		public DuplicatePollFeedbackChoice(){
			super("Duplicate selection choice", "1007");
		}
	}

	public static class InvalidPollFeedbackChoice extends GenericUserException{
		public InvalidPollFeedbackChoice(String message){
			super(message, "1008");
		}
	}
	

	public static class InvalidSessionToken extends GenericUserException{
		public InvalidSessionToken(){
			super("Invalid session token", "1009");
		}
	}
	

	public static class AccountNotVerified extends GenericUserException{
		public AccountNotVerified(){
			super("Account not yet verified", "1010");
		}
	}

	public static class UnregisteredEmail extends GenericUserException{
		public UnregisteredEmail(){
			super("Email address not recognized", "1011");
		}
	}
	

	public static class EmailAlreadyRegistered extends GenericUserException{
		public EmailAlreadyRegistered(){
			super("Email address already registered", "1012");
		}
	}
	


	public static class InvalidGroupNomination extends GenericUserException{
		public InvalidGroupNomination(){
			super("You cannot nominate a person from another group", "1013");
		}
	}
	


	public static class AlreadyNominated extends GenericUserException{
		public AlreadyNominated(){
			super("You have already nominated", "1014");
		}
	}

	public static class AwardNominationClosed extends GenericUserException{
		public AwardNominationClosed(){
			super("Award nomination already closed", "1014");
		}
	}

	public static class ItemsOutOfStock extends GenericUserException{
		public ItemsOutOfStock(){
			super("Item is no longer available","1015");
		}
	}
	
	public static class InsufficientRewardsPoints extends GenericUserException{
		public InsufficientRewardsPoints(){
			super("Insufficient points","1016");
		}
	}
	
	

	public static class InvalidRedemptionCode extends GenericUserException{
		public InvalidRedemptionCode(){
			super("Invalid redemption code. Code is invalid or already claimed","1017");
		}
	}

	public static class InvalidPoints extends GenericUserException{
		public InvalidPoints(){
			super("Invalid points","1018");
		}
	}

	public static class CannotAwardSelf extends GenericUserException{
		public CannotAwardSelf(){
			super("You cannot give an award to yourself.","1019");
		}
	}

	public static class AlreadyJoinedTeamChallenge extends GenericUserException{
		public AlreadyJoinedTeamChallenge(){
			super("Already a member of a team","1020");
		}
	}

	public static class InvalidTeamCode extends GenericUserException{
		public InvalidTeamCode(){
			super("Invalid code","1021");
		}
	}

	public static class SurveyResponseAlreadyExists extends GenericUserException{
		public SurveyResponseAlreadyExists(){
			super("Already answered this survey","1022");
		}
	}
}
