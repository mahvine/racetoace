package ph.com.smart.racetoace.rnr.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.rnr.model.Survey;

@Repository
public interface SurveyRepository extends JpaRepository<Survey,Long>{

	List<Survey> findByStartDateBeforeAndEndDateAfter(Date startAfter, Date endBefore);
	
}
