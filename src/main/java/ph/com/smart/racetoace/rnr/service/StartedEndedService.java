package ph.com.smart.racetoace.rnr.service;

import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.core.repository.ChallengeRepository;
import ph.com.smart.racetoace.notification.PushGateway;
import ph.com.smart.racetoace.rnr.model.Award;
import ph.com.smart.racetoace.rnr.model.TeamChallenge;
import ph.com.smart.racetoace.rnr.repository.AwardRepository;
import ph.com.smart.racetoace.rnr.repository.TeamChallengeRepository;

/**
 * For notifying the following
 * 1.	awards that started 
 * 2.	challenges that started
 * 3.	challenges that ended 
 * 
 * @author jrdomingo
 * @since Sep 20, 2015 3:17:37 PM
 */
@Service
public class StartedEndedService {

	private static final Logger logger = LoggerFactory.getLogger(StartedEndedService.class);
	@Inject
	PushGateway pushGateway;
	
	@Inject
	AwardRepository awardRepository;
	
	@Inject
	ChallengeRepository challengeRepository;
	
	@Inject
	TeamChallengeRepository teamChallengeRepository;
	
    @Scheduled(cron="0 * * * * *")
	public void processNow(){
    	Date thisMinute = new Date();
    	
    	List<Award> startedAwards = awardRepository.findByStartDate(thisMinute);
    	for(Award award: startedAwards){
    		pushGateway.notifyAwardHasStarted(award);
    		logger.info("Award:{} started",award.id);
    	}

    	List<Award> endedAwards = awardRepository.findByEndDate(thisMinute);
    	for(Award award: endedAwards){
    		pushGateway.notifyAwardHasEnded(award);
    		logger.info("Award:{} ended",award.id);
    	}
    	
    	List<Challenge> startedChallenges = challengeRepository.findByStartDate(thisMinute);
    	for(Challenge challenge: startedChallenges){
    		pushGateway.notifyChallengeHasStarted(challenge);
    		logger.info("Challenge:{} started",challenge.id);
    	}
    	
    	List<Challenge> endedChallenges = challengeRepository.findByEndDate(thisMinute);
    	for(Challenge challenge: endedChallenges){
    		pushGateway.notifyChallengeHasEnded(challenge);
    		logger.info("Challenge:{} ended",challenge.id);
    	}
    	

    	List<TeamChallenge> startedTeamChallenges = teamChallengeRepository.findByStartDate(thisMinute);
    	for(TeamChallenge challenge: startedTeamChallenges){
    		pushGateway.notifyTeamChallengeHasStarted(challenge);
    		logger.info("Team Challenge:{} started",challenge.id);
    	}
    	
    	List<TeamChallenge> endedTeamChallenges = teamChallengeRepository.findByEndDate(thisMinute);
    	for(TeamChallenge challenge: endedTeamChallenges){
    		pushGateway.notifyTeamChallengeHasEnded(challenge);
    		logger.info("Team Challenge:{} ended",challenge.id);
    	}
    	
    	
	}
    
    
    
	
}
