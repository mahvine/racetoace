/**
 *
 */
package ph.com.smart.racetoace.rnr.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.Award;
import ph.com.smart.racetoace.rnr.model.Award.Status;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 10:06:45 AM
 */
@Repository
public interface AwardRepository extends JpaRepository<Award, Long>, JpaSpecificationExecutor<Award> {


    Page<Award> findByEndDateGreaterThanEqual(Date date, Pageable pageable);

    Page<Award> findByEndDateLessThan(Date date, Pageable pageable);

    List<Award> findByEndDateGreaterThanEqual(Date date, Sort Sort);

    List<Award> findByEndDateLessThan(Date date, Sort Sort);

//    @Query("select ug.id, n.nominated, count(n.id) " +
//            "from UserGroup ug, Nomination n, User u " +
//            "where n.nominated.id = u.id and u.userGroup.id = ug.id and n.award.id = ?1 " +
//            "group by n.nominated.id " +
//            "order by ug.id")
    @Query("select n.nominated, count(n.id) " +
            "from  Nomination n " +
            "where n.award.id = ?1 " +
            "group by n.nominated.id ")
    List<Object[]> processWinnersPerAwardId(Long awardId);


    List<Award> findByEndDateLessThanAndStatus(Date date, Status status);

    List<Award> findByStartDate(Date startDate);
    
    List<Award> findByEndDate(Date endDate);
    
    List<Award> findByWinners(User user);
    
}
