/**
 *
 */
package ph.com.smart.racetoace.rnr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.rnr.model.Recognition;

/**
 * @author Jrdomingo
 * Jan 11, 2016 10:11:19 AM
 */
@Repository
public interface RecognitionRepository extends JpaRepository<Recognition,Long>, JpaSpecificationExecutor<Recognition>{

}
