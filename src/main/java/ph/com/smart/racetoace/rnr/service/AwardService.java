/**
 * 
 */
package ph.com.smart.racetoace.rnr.service;

import java.util.Date;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.rnr.model.Award;
import ph.com.smart.racetoace.rnr.model.Nomination;
import ph.com.smart.racetoace.rnr.repository.AwardRepository;
import ph.com.smart.racetoace.rnr.repository.NominationRepository;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.AlreadyNominated;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.AwardNominationClosed;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.InvalidGroupNomination;

/**
 * @author Jrdomingo
 * @since Sep 14, 2015 11:31:41 AM
 * 
 */
@Service
public class AwardService {

	@Inject
	AwardRepository awardRepository;
	
	@Inject
	NominationRepository nominationRepository;
	
	@Inject
	UserRepository userRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(AwardService.class);
	
	public void nominate(User nominator, Long awardId, Long nominatedUserId, String reason){
		
		User nominee = userRepository.findOne(nominatedUserId);
		Award award = awardRepository.findOne(awardId);
		Nomination previousNomination = nominationRepository.findByNominatorAndAward(nominator, award);
		if(award.endDate!=null){
			if(award.endDate.getTime() < new Date().getTime()){
				throw new AwardNominationClosed();
			}
		}
		if(previousNomination==null){
			logger.debug("no previous nomination for award:{} from user:{}",award.id, nominator);
	        if (nominator.userGroup.id.equals(nominee.userGroup.id)) {
	            Nomination nomination = new Nomination();
	            nomination.reason = reason;
	            nomination.nominator = nominator;
	            nomination.nominated = nominee;
	            nomination.award = award;
	            nominationRepository.save(nomination);

				logger.info("nomination saved for award:{} from user:{} nominated:{}",award.id, nominator.id,nominee.id);
	        } else {
	        	//You cannot nominate a person from another group
	            throw new InvalidGroupNomination();
	        }
		}else{
			throw new AlreadyNominated();
		}
		
	}
	
	
	
}
