/**
 * 
 */
package ph.com.smart.racetoace.rnr.service;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ph.com.smart.racetoace.core.model.Task;
import ph.com.smart.racetoace.core.model.task.LocationTask;
import ph.com.smart.racetoace.core.model.task.PollTask;
import ph.com.smart.racetoace.core.model.task.PollTask.Selection;
import ph.com.smart.racetoace.core.model.task.QuizTask;
import ph.com.smart.racetoace.core.model.task.SimpleTask;
import ph.com.smart.racetoace.core.task.repository.LocationTaskRepository;
import ph.com.smart.racetoace.core.task.repository.PollTaskRepository;
import ph.com.smart.racetoace.core.task.repository.QuizTaskRepository;
import ph.com.smart.racetoace.core.task.repository.SimpleTaskRepository;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.DuplicatePollFeedbackChoice;
import ph.com.smart.racetoace.web.rest.dto.TaskDTO;
import ph.com.smart.racetoace.web.rest.dto.mapper.ChallengeDTOMapper;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 10:58:31 AM
 * 
 */
@Service
public class TaskService {
	
	@Inject
	LocationTaskRepository locationTaskRepository;
	
	@Inject
	PollTaskRepository pollTaskRepository;
	
	@Inject
	SimpleTaskRepository simpleTaskRepository;
	
	@Inject
	QuizTaskRepository quizTaskRepository;

	@Inject
	ChallengeDTOMapper challengeDTOMapper;
	
	public static Logger logger = LoggerFactory.getLogger(TaskService.class);
	
	public List<Task> save(List<TaskDTO> listTaskDTO){
		List<Task> listTask = new ArrayList<Task>();
		for(TaskDTO taskDTO : listTaskDTO){
			listTask.add(save(taskDTO));
		}
		return listTask;
	}
	
	public Task save(TaskDTO taskDTO){
		Task task = challengeDTOMapper.taskDTOToTask(taskDTO);
		if(task instanceof SimpleTask){
			SimpleTask simpleTask = (SimpleTask) task;
			simpleTask.code = simpleTask.code.toUpperCase().trim();
			task = simpleTaskRepository.save(simpleTask);
			logger.info("saved simple task = id:{}, description:{}, code:{}",task.id, task.description, simpleTask.code);
		}else if (task instanceof QuizTask){
			QuizTask quizTask = (QuizTask) task;
			quizTask.answer = quizTask.answer.toUpperCase().trim();
			task = quizTaskRepository.save(quizTask);
			logger.info("saved quiz task = id:{}, description:{}, answer:{}",task.id, task.description, quizTask.answer);
		}else if (task instanceof PollTask){
			PollTask pollTask = (PollTask) task;
			List<String> choices = new ArrayList<String>();
			for(Selection selection: pollTask.selections){
				selection.choice = selection.choice.toUpperCase().trim();
				if(choices.contains(selection.choice)){
					throw new DuplicatePollFeedbackChoice();
				}else{
					choices.add(selection.choice);
				}
			}
			task = pollTaskRepository.save(pollTask);
			logger.info("saved a poll task: {}", pollTask);
		}else if (task instanceof LocationTask){
			LocationTask locationTask = (LocationTask) task;
			task = locationTaskRepository.save(locationTask);
			logger.info("saved a location task: {}", locationTask);
		}
		return task;
	}
	
}
