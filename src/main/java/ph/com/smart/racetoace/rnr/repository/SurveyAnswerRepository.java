package ph.com.smart.racetoace.rnr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.SurveyAnswer;
import ph.com.smart.racetoace.rnr.model.SurveyAnswer.SurveyAnswerPK;
import ph.com.smart.racetoace.rnr.model.SurveyQuestion;

@Repository
public interface SurveyAnswerRepository extends JpaRepository<SurveyAnswer,SurveyAnswerPK>{
	
	@Query("select sa.question.id,avg(sa.rating),count(sa.user) from SurveyAnswer sa where sa.question.id=?1 group by sa.question.id")
	public List<Object[]> getAverageAnswer(Long questionId);
	
	public SurveyAnswer findByUserAndQuestion(User user, SurveyQuestion question);
	
}
