package ph.com.smart.racetoace.rnr.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.UserSurvey.UserSurveyPK;

@Entity
@Table(name="user_survey")
@IdClass(UserSurveyPK.class)
public class UserSurvey {

	@Id
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	public User user;
	
	@Id
	@ManyToOne
	@JoinColumn(name="survey_id", referencedColumnName="id")
	public Survey survey;
	
	@Column(name="date_created")
	public Date dateCreated;
	
	@Column(name="comment")
	public String comment;

	@SuppressWarnings("serial")
	public static class UserSurveyPK implements Serializable{
		private Long user;
		private Long survey;
		public Long getUser() {
			return user;
		}
		public void setUser(Long user) {
			this.user = user;
		}
		public Long getSurvey() {
			return survey;
		}
		public void setSurvey(Long survey) {
			this.survey = survey;
		}
	}

}
