/**
 *
 */
package ph.com.smart.racetoace.rnr.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * @author Jrdomingo
 * Jan 12, 2016 6:42:09 PM
 */
@Entity
@Table(name="survey_question")
public class SurveyQuestion {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String text;

	@Transient
	public Double average;

	@Transient
	public Long respondents;
}
