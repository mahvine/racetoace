/**
 * 
 */
package ph.com.smart.racetoace.rnr.service;

import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import ph.com.smart.racetoace.core.model.UserChallenge;
import ph.com.smart.racetoace.core.model.UserChallenge.Status;
import ph.com.smart.racetoace.core.repository.UserChallengeRepository;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 10:58:54 AM
 * 
 */
@Component
public class UserChallengeManager {
	
	private static final Logger logger = LoggerFactory.getLogger(UserChallengeManager.class);
	
	private ScheduledExecutorService executorService;
	private int validationInterval = 60;
	
	@Inject
	UserChallengeRepository userChallengeRepository;
	
	@Inject
	PointService pointService;
	
	public UserChallengeManager(){
		executorService = Executors.newSingleThreadScheduledExecutor();
		        executorService.scheduleWithFixedDelay(new Runnable()
		        {
		            @Override
		            public void run() {
		            	invalidateAcceptedChallenges();
		            	
		            }
		        }, validationInterval, validationInterval, TimeUnit.SECONDS);
	}
	
	//TODO must use spring cron expression not working with it for some reason
	public void invalidateAcceptedChallenges(){
		List<UserChallenge> acceptedUserChallenges = userChallengeRepository.findByStatusAndChallengeEndDateBefore(Status.ACCEPTED, new Date());
		for(UserChallenge userChallenge: acceptedUserChallenges){
			logger.info("Challenge:{} failed by User:{}",userChallenge.challenge.id, userChallenge.user.id);
			userChallenge.status = Status.FAILED;
			userChallenge.pointsEarned = -userChallenge.challenge.penalty;
			userChallengeRepository.save(userChallenge);
			
			// deduct points
			pointService.deductRewardPoints(userChallenge.user.id, userChallenge.challenge.penalty);
			pointService.deductScorePoints(userChallenge.user.id, userChallenge.challenge.penalty);
			
		}
		
	}
	
	
}
