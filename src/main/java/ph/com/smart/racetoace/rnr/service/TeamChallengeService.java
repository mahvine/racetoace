/**
 *
 */
package ph.com.smart.racetoace.rnr.service;

import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.notification.PushGateway;
import ph.com.smart.racetoace.rnr.model.TeamChallenge;
import ph.com.smart.racetoace.rnr.model.UserTeam;
import ph.com.smart.racetoace.rnr.repository.TeamChallengeRepository;
import ph.com.smart.racetoace.rnr.repository.UserTeamRepository;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.AlreadyJoinedTeamChallenge;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.InvalidTeamCode;

/**
 * @author Jrdomingo
 * Jan 21, 2016 2:47:44 PM
 * TODO
 */
@Service
public class TeamChallengeService {
	
	@Inject
	TeamChallengeRepository teamChallengeRepository;
	
	@Inject
	UserTeamRepository userTeamRepository;
	
	private static final Logger logger = LoggerFactory.getLogger(TeamChallengeService.class);
	
	@Inject
	PointService pointService;
	
	@Inject
	PushGateway pushGateway;
	
	//create group
	public void createTeam(Long teamChallengeId, User leader, String name, String code){
		TeamChallenge teamChallenge = teamChallengeRepository.findOne(teamChallengeId);
		if(teamChallenge == null){
			throw new RuntimeException("team challenge non existent");
		}
		// check if already joined
		UserTeam userTeam = getTeamChallengeAndMember(teamChallengeId, leader.id);
		if(userTeam!=null){
			throw new AlreadyJoinedTeamChallenge();
		}
		
		//save
		userTeam = new UserTeam();
		userTeam.teamChallenge = teamChallenge;
		userTeam.members.add(leader);
		userTeam.name = name;
		userTeam.code = code;
		userTeamRepository.save(userTeam);
		logger.info("created a userTeam:{}",userTeam);
	}
	
	public UserTeam getTeamChallengeAndMember(Long teamChallengeId, Long userId){
		List<UserTeam> userTeams = userTeamRepository.findByTeamChallengeIdAndMembersId(teamChallengeId, userId);
		if(userTeams.size()>0){
			return userTeams.get(0);
		}
		return null;
	}
	
	//add member to team
	public void addMemberToTeam(Long teamId, User user, String code){
		
		UserTeam userTeam = userTeamRepository.findOne(teamId);
		TeamChallenge teamChallenge = userTeam.teamChallenge;
		
		UserTeam existingUserTeam = getTeamChallengeAndMember(teamChallenge.id, user.id);
		if(existingUserTeam!=null){
			throw new AlreadyJoinedTeamChallenge();
		}
		
		if(userTeam.code.equals(code)){
			userTeam.members.add(user);
			userTeamRepository.save(userTeam);
			logger.info("{} joined {}",user,userTeam);
		}else{
			throw new InvalidTeamCode();
		}
	}
	
	//set winning team of a challenge
	public void completeTeamChallenge(Long teamChallengeId, Long teamId){
		TeamChallenge teamChallenge = teamChallengeRepository.findOne(teamChallengeId);
		UserTeam userTeam = userTeamRepository.findOne(teamId);

		logger.info("team:{} to complete teamChallenge:{}",userTeam,teamChallenge);
		if(userTeam.teamChallenge.id.equals(teamChallenge.id)){
			if(!userTeam.winner){
				userTeam.pointsRewarded = teamChallenge.winnerPoints;
				userTeam.winner = true;
				userTeamRepository.save(userTeam);
				
				for(User member: userTeam.members){
					pointService.addRewardPoints(member.id, userTeam.pointsRewarded);
					pointService.addScorePoints(member.id, userTeam.pointsRewarded);
				}
				logger.info("{} has declared {} a winner",teamChallenge,userTeam);
				pushGateway.notifyTeamChallengeHasCompleted(teamChallenge, userTeam);
			}else{
				throw new RuntimeException("Team already a winner");
			}
		}else{
			throw new RuntimeException("Team challenge mismatch");
		}
	}
	
}
