/**
 *
 */
package ph.com.smart.racetoace.rnr.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.rnr.model.UserTeam;

/**
 * @author Jrdomingo
 * Jan 21, 2016 2:46:37 PM
 */
@Repository
public interface UserTeamRepository extends JpaRepository<UserTeam, Long>{
	
	public List<UserTeam> findByTeamChallengeIdAndMembersId(Long teamChallengeId,Long userId);

	public List<UserTeam> findByTeamChallengeId(Long teamChallengeId);

	public List<UserTeam> findByMembersIdAndWinner(Long userId, boolean winner);

	
}
