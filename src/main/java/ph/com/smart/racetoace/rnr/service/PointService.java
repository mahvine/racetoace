/**
 * 
 */
package ph.com.smart.racetoace.rnr.service;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.InsufficientRewardsPoints;

/**
 * @author JRDomingo
 * @since Sep 3, 2015 11:14:23 AM
 * 
 */
@Service
public class PointService {

	private static final Logger logger = LoggerFactory.getLogger(PointService.class);
	
    @Inject
    UserRepository userRepository;

    /**
     * @param userId
     * @param penalty
     * @return total deducted points (may be total penalty or current reward points)
     */
    public synchronized long deductRewardPoints(Long userId, Long penalty){
    	User user = userRepository.findOne(userId);
    	long rewardDeducted = 0;
    	if(user.rewardPoints >= penalty){
    		rewardDeducted = penalty;
    	}else{
    		rewardDeducted = user.rewardPoints;
    	}
    	user.rewardPoints -= rewardDeducted;
    	userRepository.save(user);
    	logger.info("{} reward points deducted to user:{}",rewardDeducted,user.id);
    	return rewardDeducted;
    }
    
    /**
     * 
     * @param userId
     * @param penalty
     * 
     * @return
     */
    public synchronized long deductScorePoints(Long userId, Long penalty){
    	User user = userRepository.findOne(userId);
    	long scoreDeducted = 0;
    	if(user.scorePoints >= penalty){
    		scoreDeducted = penalty;
    	}else{
    		scoreDeducted = user.scorePoints;
    	}
    	user.scorePoints -= scoreDeducted;
    	userRepository.save(user);

    	logger.info("{} score points deducted to user:{}",scoreDeducted,user.id);
    	return scoreDeducted;
    }
    
    public synchronized void addScorePoints(Long userId, Long points){
    	User user = userRepository.findOne(userId);
    	user.scorePoints += points;
    	userRepository.save(user);
    	logger.info("{} score points added to user:{}",points,user.id);
    }
    
    public synchronized void addRewardPoints(Long userId, Long points){
    	User user = userRepository.findOne(userId);
    	user.rewardPoints += points;
    	userRepository.save(user);

    	logger.info("{} reward points added to user:{}",points,user.id);
    }

    /**
     * @param sourceUserId
     * @param destinationUserId
     * @param points
     */
    public synchronized void transferRewardPoints(Long sourceUserId, Long destinationUserId, Long points){
    	User sourceUser = userRepository.findOne(sourceUserId);
    	if(sourceUser.rewardPoints < points){
    		throw new InsufficientRewardsPoints();
    	}
    	sourceUser.rewardPoints -= points;
    	userRepository.save(sourceUser);
    	User destinationUser = userRepository.findOne(destinationUserId);
    	destinationUser.rewardPoints += points;
    	userRepository.save(destinationUser);
    	logger.info("{} reward points transferred to {} from {}",points,destinationUserId, sourceUserId);
    }
    
}
