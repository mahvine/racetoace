/**
 *
 */
package ph.com.smart.racetoace.rnr.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.rnr.model.TeamChallenge;

/**
 * @author Jrdomingo
 * Jan 21, 2016 2:45:50 PM
 */
@Repository
public interface TeamChallengeRepository extends JpaRepository<TeamChallenge,Long>{

	public List<TeamChallenge> findByStartDateBeforeAndEndDateAfter(Date startAfter, Date endBefore, Sort sort);

	public List<TeamChallenge> findByStartDate(Date startDate);

	public List<TeamChallenge> findByEndDate(Date endDate);
	
}
