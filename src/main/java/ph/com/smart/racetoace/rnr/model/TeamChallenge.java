package ph.com.smart.racetoace.rnr.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name="team_challenge")
public class TeamChallenge{

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String title; // title
	
	public String details;

	@Column(name = "start_date")
	public Date startDate = new Date();

	@Column(name = "end_date")
	public Date endDate;

	@Column(name = "winner_points")
	public long winnerPoints;
	
	@JsonIgnore
	@OneToMany(fetch=FetchType.LAZY)
	@JoinTable(name = "user_team", joinColumns = @JoinColumn(name = "team_challenge_id"), 
			   inverseJoinColumns = @JoinColumn(name = "id"))
	public List<UserTeam> teams;
	
	public enum Status{
		COMPLETED, JOINED, OPEN
	}

}
