package ph.com.smart.racetoace.rnr.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="survey")
public class Survey {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String title;

	@Column(name="start_date")
	public Date startDate;
	
	@Column(name="end_date")
	public Date endDate;

	@OneToMany
	@JoinColumn(name="survey_id", referencedColumnName="id")
	public List<SurveyQuestion> questions = new ArrayList<SurveyQuestion>();
	
	@Transient
	public Status status;
	
	public enum Status{
		OPEN,COMPLETED
	}
	
	
}
