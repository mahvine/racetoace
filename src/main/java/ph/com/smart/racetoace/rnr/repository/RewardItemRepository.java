/**
 * 
 */
package ph.com.smart.racetoace.rnr.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import ph.com.smart.racetoace.rnr.model.RewardItem;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 10:07:27 AM
 * 
 */
public interface RewardItemRepository extends JpaRepository<RewardItem,Long>{
	
	Page<RewardItem> findByQuantityGreaterThanEqual(int quantity,Pageable pageable);
}
