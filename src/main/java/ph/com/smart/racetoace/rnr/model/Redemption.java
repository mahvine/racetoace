/**
 * 
 */
package ph.com.smart.racetoace.rnr.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ph.com.smart.racetoace.core.model.User;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 9:24:01 AM
 * 
 */
@Entity
@Table(name="redemption")
public class Redemption {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	public User user;
	
	@ManyToOne
	@JoinColumn(name="reward_item_id", referencedColumnName="id")
	public RewardItem rewardItem;
	
	public String code;
	
	@Column(name="date_created")
	public Date dateCreated;

	@Column(name="date_claimed")
	public Date dateClaimed;
}
