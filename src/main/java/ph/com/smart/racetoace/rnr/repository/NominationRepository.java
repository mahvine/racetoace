/**
 *
 */
package ph.com.smart.racetoace.rnr.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.Award;
import ph.com.smart.racetoace.rnr.model.Nomination;

import java.util.List;

/**
 * @author JRDomingo
 * @since Sep 3, 2015 11:13:46 AM
 */
public interface NominationRepository extends JpaRepository<Nomination, Long> {

    Nomination findByNominatorAndAward(User nominator, Award award);

    List<Nomination> findByAward(Award award);
    
    @Query(value="select n.nominated, count(n.nominated) from Nomination n where n.award.id=?1 group by n.nominated")
    List<Object[]> countUserNominationsPerAward(Long awardId);

    List<Nomination> findByNominated(User user);

    List<Nomination> findByNominator(User user);
}
