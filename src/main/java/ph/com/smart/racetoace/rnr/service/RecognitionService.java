/**
 *
 */
package ph.com.smart.racetoace.rnr.service;

import java.util.Date;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.notification.PushGateway;
import ph.com.smart.racetoace.rnr.model.Recognition;
import ph.com.smart.racetoace.rnr.repository.RecognitionRepository;
import ph.com.smart.racetoace.rnr.service.exception.GenericUserException.InvalidPoints;

/**
 * @author Jrdomingo
 * Jan 11, 2016 10:12:21 AM
 */
@Service
public class RecognitionService {

	@Inject
	RecognitionRepository recognitionRepository;
	
	@Inject
	UserRepository userRepository;

	@Inject
	PointService pointService;
	
	@Inject
	PushGateway pushGateway;
	
	private static final Logger logger = LoggerFactory.getLogger(RecognitionService.class);
	
	public void recognize(Long userId, String title, String description, Long recognizerId, Long points){
		if(points ==null){
			throw new InvalidPoints();
		}
		Recognition recognition = new Recognition();
		recognition.dateCreated = new Date();
		recognition.title = title;
		recognition.description = description;
		recognition.user = userRepository.findOne(userId);
		recognition.rewardPoints = points;
		recognition.recognizer = userRepository.findOne(recognizerId);
		recognitionRepository.save(recognition);
		logger.info("saved recognition:{}",recognition);
		pointService.addRewardPoints(userId, points);
		pointService.addScorePoints(userId, points);
		
		pushGateway.notifyNewRecognition(recognition);
	}
	
}
