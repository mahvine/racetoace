package ph.com.smart.racetoace.rnr.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;

import com.fasterxml.jackson.annotation.JsonIgnore;

import ph.com.smart.racetoace.core.model.User;

/**
 * 
 * @author JRDomingo
 * @since Aug 24, 2015 9:24:20 AM
 *
 */
@Entity
public class Award {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String name; // title
	
	public String description;

	@Column(name = "start_date")
	public Date startDate = new Date();

	@Column(name = "end_date")
	public Date endDate;

	@Column(name = "reward_points")
	public long rewardPoints;

	@ManyToOne(targetEntity=UserGroup.class)
	@JoinColumn(name="user_group_id", referencedColumnName="id")
	public UserGroup userGroup;
	
	
	@JsonIgnore
	@ManyToMany(fetch=FetchType.EAGER)
	@JoinColumn(name="user_id", referencedColumnName="id")
	public List<User> winners;

	@Enumerated(EnumType.STRING)
	public Type type;

	@Enumerated(EnumType.STRING)
	public Status status;
	
	@PrePersist
	public void setStatusAsOpen(){
		if(id==null){
			status = Status.OPEN;
		}
		if(status==null){
			status = Status.OPEN;
		}
	}
	
	public enum Type{
		GROUP,INDIVIDUAL
	}
	
	public enum Status{
		OPEN, CLOSED
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		Award award = (Award) o;

		return id.equals(award.id);

	}

	@Override
	public int hashCode() {
		return id.hashCode();
	}
}
