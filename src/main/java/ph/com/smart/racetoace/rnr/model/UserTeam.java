/**
 *
 */
package ph.com.smart.racetoace.rnr.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import ph.com.smart.racetoace.core.model.User;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * @author Jrdomingo
 * Jan 21, 2016 2:06:47 PM
 */
@Entity
@Table(name="user_team")
public class UserTeam {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="team_challenge_id", referencedColumnName="id")
	public TeamChallenge teamChallenge;
	
	public String name;
	
	public String code;
	
	public boolean winner = false;
	
	@Column(name="points_rewarded")
	public Long pointsRewarded;
	
	@ManyToMany
	public List<User> members = new ArrayList<User>();

	@Override
	public String toString() {
		return "{" + (id != null ? "id:" + id + ", " : "")
				+ (teamChallenge != null ? "teamChallenge:" + teamChallenge + ", " : "")
				+ (name != null ? "name:" + name + ", " : "") + (code != null ? "code:" + code + ", " : "") + "winner:"
				+ winner + ", " + (pointsRewarded != null ? "pointsRewarded:" + pointsRewarded + ", " : "")
				+ (members != null ? "members:" + members : "") + "}";
	}
	
}
