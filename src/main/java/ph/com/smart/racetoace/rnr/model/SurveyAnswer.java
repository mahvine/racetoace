/**
 *
 */
package ph.com.smart.racetoace.rnr.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.SurveyAnswer.SurveyAnswerPK;

/**
 * @author Jrdomingo
 * Jan 12, 2016 6:57:02 PM
 */
@Entity
@Table(name="survey_answer")
@IdClass(SurveyAnswerPK.class)
public class SurveyAnswer {
	
	@Id
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	public User user;
	
	@Id
	@ManyToOne
	@JoinColumn(name="question_id", referencedColumnName="id")
	public SurveyQuestion question;
	
	public Integer rating;
	
	
	@SuppressWarnings("serial")
	public static class SurveyAnswerPK implements Serializable{
		private Long user;
		private Long question;
		public Long getUser() {
			return user;
		}
		public void setUser(Long user) {
			this.user = user;
		}
		public Long getQuestion() {
			return question;
		}
		public void setQuestion(Long question) {
			this.question = question;
		}
	}


}
