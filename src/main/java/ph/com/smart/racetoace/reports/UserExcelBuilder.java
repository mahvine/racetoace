package ph.com.smart.racetoace.reports;

import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import ph.com.smart.racetoace.core.model.User;

public class UserExcelBuilder extends AbstractExcelView{

	private List<User> users;
	public UserExcelBuilder(List<User> vehicles){
		this.users = vehicles;
	}
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,	HttpServletResponse response) throws Exception {

		
        // create a new Excel sheet
        HSSFSheet sheet = workbook.createSheet("All Users");
        sheet.setDefaultColumnWidth(30);
         
        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
//      style.setFillForegroundColor(HSSFColor.TEAL.index);
      style.setFillForegroundColor(HSSFColor.ORANGE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        
        style.setFont(font);

        int rowCount = 0;
        // create header row
        HSSFRow header = sheet.createRow(rowCount++);
         
        header.createCell(0).setCellValue("Email");
        header.getCell(0).setCellStyle(style);
         
        header.createCell(1).setCellValue("Name");
        header.getCell(1).setCellStyle(style);
         

        header.createCell(2).setCellValue("Birthdate");
        header.getCell(2).setCellStyle(style);

        header.createCell(3).setCellValue("Hiringdate");
        header.getCell(3).setCellStyle(style);

        header.createCell(4).setCellValue("Unspent Points");
        header.getCell(4).setCellStyle(style);

        header.createCell(5).setCellValue("Total Points");
        header.getCell(5).setCellStyle(style);

        header.createCell(6).setCellValue("Status");
        header.getCell(6).setCellStyle(style);
        
        header.createCell(7).setCellValue("Group");
        header.getCell(7).setCellStyle(style);

         
        // create data rows
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (User user : users) {
            HSSFRow aRow = sheet.createRow(rowCount++);
            
            if(user.email!=null){
            	aRow.createCell(0).setCellValue(user.email);
            }

            if(user.profile!=null){
            	if(user.profile.name!=null){
            		aRow.createCell(1).setCellValue(user.profile.name);
            	}
            }

            if(user.profile!=null){
            	if(user.profile.birthDate!=null){
            		aRow.createCell(2).setCellValue(dateFormat.format(user.profile.birthDate));
            	}
            }
            
            if(user.profile!=null){
            	if(user.profile.hiringDate!=null){
            		aRow.createCell(3).setCellValue(dateFormat.format(user.profile.hiringDate));
            	}
            }

            aRow.createCell(4).setCellValue(user.rewardPoints);

            aRow.createCell(5).setCellValue(user.scorePoints);

            aRow.createCell(6).setCellValue(user.status.toString());

            
            if(user.userGroup!=null){
           		aRow.createCell(7).setCellValue(user.userGroup.name);
            }

        }
        
        rowCount+=4;
        
        
//            //SET PICTURE
//            InputStream inputStream = new FileInputStream(resourcePath);
//            byte[] bytes = IOUtils.toByteArray(inputStream);
//            int pictureIdx = workbook.addPicture(bytes, Workbook.PICTURE_TYPE_PNG);
//            //close the input stream
//            inputStream.close();
//            //Returns an object that handles instantiating concrete classes
//            CreationHelper helper = workbook.getCreationHelper();
//            
//            //Creates the top-level drawing patriarch.
//            Drawing drawing = sheet.createDrawingPatriarch();
//
//            //Create an anchor that is attached to the worksheet
//            ClientAnchor anchor = helper.createClientAnchor();
//            //set top-left corner for the image
//            anchor.setCol1(1);
//            anchor.setRow1(rowCount);
//            //Creates a picture
//            Picture pict = drawing.createPicture(anchor, pictureIdx);
//            //Reset the image to the original size
//            pict.resize(0.1);
            
     // Set the headers
        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=AllUsers.xls");

        // Here is where you will want to put the code to build the Excel spreadsheet
        OutputStream outStream = null;

        try {
            outStream = response.getOutputStream();
            workbook.write(outStream);
            outStream.flush();
        } finally {
            outStream.close();
        }  
	}

}
