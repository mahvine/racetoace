package ph.com.smart.racetoace.reports;

import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.springframework.web.servlet.view.document.AbstractExcelView;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.Survey;
import ph.com.smart.racetoace.rnr.model.SurveyAnswer;
import ph.com.smart.racetoace.rnr.model.SurveyQuestion;
import ph.com.smart.racetoace.rnr.model.UserSurvey;
import ph.com.smart.racetoace.rnr.repository.SurveyAnswerRepository;

public class UserSurveyExcelBuilder extends AbstractExcelView{

	private List<UserSurvey> userSurveys;
	private Survey survey;
	private SurveyAnswerRepository surveyAnswerRepository;
	public UserSurveyExcelBuilder(Survey survey, List<UserSurvey> userSurveys, SurveyAnswerRepository surveyAnswerRepository){
		this.userSurveys = userSurveys;
		this.survey = survey;
		this.surveyAnswerRepository = surveyAnswerRepository;
	}
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, HSSFWorkbook workbook, HttpServletRequest request,	HttpServletResponse response) throws Exception {

		
        // create a new Excel sheet
        HSSFSheet sheet = workbook.createSheet("All Users");
        sheet.setDefaultColumnWidth(30);
         
        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
//      style.setFillForegroundColor(HSSFColor.TEAL.index);
        style.setFillForegroundColor(HSSFColor.ORANGE.index);
        style.setFillPattern(CellStyle.SOLID_FOREGROUND);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
        font.setColor(HSSFColor.WHITE.index);
        
        style.setFont(font);

        int rowCount = 0;
        // create header row
        HSSFRow header = sheet.createRow(rowCount++);
         
        header.createCell(0).setCellValue("Email");
        header.getCell(0).setCellStyle(style);
         
        header.createCell(1).setCellValue("Name");
        header.getCell(1).setCellStyle(style);
        int q = 0;
        for(SurveyQuestion question : survey.questions){

            header.createCell(q+2).setCellValue(question.text);
            header.getCell(q+2).setCellStyle(style);
        	q++;
        }

        header.createCell(q+2).setCellValue("Comments");
        header.getCell(q+2).setCellStyle(style);

        // create data rows
        
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        for (UserSurvey userSurvey : userSurveys) {
        	User user = userSurvey.user;
            HSSFRow aRow = sheet.createRow(rowCount++);
            
            if(user.email!=null){
            	aRow.createCell(0).setCellValue(user.email);
            }

            if(user.profile!=null){
            	if(user.profile.name!=null){
            		aRow.createCell(1).setCellValue(user.profile.name);
            	}
            }
            

            int a = 0;
            for(SurveyQuestion question : survey.questions){
            	SurveyAnswer answer = surveyAnswerRepository.findByUserAndQuestion(user, question);
            	if(answer!=null){
            		aRow.createCell(a+2).setCellValue(answer.rating+"");
            	}
            	a++;
            }
            if(userSurvey.comment!=null){
            	aRow.createCell(a+2).setCellValue(userSurvey.comment);
            }

        }
        
        rowCount+=4;
        
     // Set the headers
        response.setHeader("Content-Type", "application/octet-stream");
        response.setHeader("Content-Disposition", "attachment; filename=Survey-"+survey.title+" Response List.xls");

        // Here is where you will want to put the code to build the Excel spreadsheet
        OutputStream outStream = null;

        try {
            outStream = response.getOutputStream();
            workbook.write(outStream);
            outStream.flush();
        } finally {
            outStream.close();
        }  
	}

}
