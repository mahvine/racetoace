package ph.com.smart.racetoace.notification.gcm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * For GCM push notifications using topics (pub/sub) 
 * @author JRDomingo
 * @since Sep 24, 2015 4:03:14 PM
 *
 */
@Service
public class GCMNotifierService {
	

	RestTemplate restTemplate = new RestTemplate();
	
	@Value("${gcm.urlLocation}")
	private String gcmUrlLocation;

	@Value("${gcm.apiKey}")
	private String gcmApiKey;

	public static Logger logger = LoggerFactory.getLogger(GCMNotifierService.class); 
	
	@Async
	public void sendToGcm(String to, Object object){
		GCMDTO gcmDTO = new GCMDTO();
		gcmDTO.to = to;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Authorization", "key=" + gcmApiKey);
		gcmDTO.data = object;
		HttpEntity<GCMDTO> request = new HttpEntity<GCMDTO>(gcmDTO, headers);

		ResponseEntity<String> responseEntity = restTemplate.exchange(gcmUrlLocation, HttpMethod.POST, request, String.class);
		logger.info("gcm response:\n{}",responseEntity.getBody());
	}


	public String getGcmApiKey() {
		return gcmApiKey;
	}

	public void setGcmApiKey(String gcmApiKey) {
		this.gcmApiKey = gcmApiKey;
	}

	public String getGcmUrlLocation() {
		return gcmUrlLocation;
	}


	public void setGcmUrlLocation(String gcmUrlLocation) {
		this.gcmUrlLocation = gcmUrlLocation;
	}

	public static class GCMDTO{
		public String to;
		public Object data;
	}

}
