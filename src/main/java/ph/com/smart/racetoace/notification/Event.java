package ph.com.smart.racetoace.notification;

import ph.com.smart.racetoace.rnr.model.Award;
import ph.com.smart.racetoace.rnr.model.Survey;
import ph.com.smart.racetoace.web.rest.dto.ChallengeDTO;
import ph.com.smart.racetoace.web.rest.dto.RecognitionDTO;
import ph.com.smart.racetoace.web.rest.dto.TeamChallengeDTO;
import ph.com.smart.racetoace.web.rest.dto.UserProfileDTO;

/**
 * causes mobile push notifications<br> see {@link Type}s
 * @author JRDomingo
 * @since Sep 24, 2015 4:04:09 PM
 *
 */
public class Event{
	public Type type;
	public String message;
	public UserProfileDTO user;
	public ChallengeDTO challenge;
	public Award award;
	public TeamChallengeDTO teamChallenge;
	public Survey survey;
	public RecognitionDTO recognition;
	
	/**
	 * <table>
	 * 	<tr><td><b>Name</b></td><td><b>Objects attached</b></td></tr>
	 * 	<tr><td>CHALLENGE_STARTED</td><td>challenge</td></tr>
	 * 	<tr><td>CHALLENGE_ENDED</td><td>challenge</td></tr>
	 * 	<tr><td>CHALLENGE_HAS_WINNER</td><td>challenge, user</td></tr>
	 * 	<tr><td>AWARD_STARTED</td><td>award</td></tr>
	 * 	<tr><td>AWARD_ENDED</td><td>award</td></tr>
	 * 	<tr><td>ANNOUNCEMENT</td><td></td></tr>
	 * </table>
	 * 	
	 * @author JRDomingo
	 * @since Sep 24, 2015 4:06:28 PM
	 *
	 */
	public enum Type{
	//	Name,						//object attribute present
		CHALLENGE_STARTED, 			//challenge
		CHALLENGE_ENDED, 			//challenge
		CHALLENGE_HAS_WINNER,	//challenge, user
		AWARD_STARTED,				//award
		AWARD_ENDED,				//award
		ANNOUNCEMENT,
		//PHASE 2
		RECOGNITION_NEW,
		SURVEY_NEW,
		TEAM_CHALLENGE_STARTED,
		TEAM_CHALLENGE_ENDED,
		TEAM_CHALLENGE_COMPLETED
	}
}