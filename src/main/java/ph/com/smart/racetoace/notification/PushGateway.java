package ph.com.smart.racetoace.notification;

import javax.inject.Inject;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javapns.notification.PushNotificationPayload;
import ph.com.smart.racetoace.config.Constants;
import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.notification.Event.Type;
import ph.com.smart.racetoace.notification.apns.ApplePushNotificationService;
import ph.com.smart.racetoace.notification.gcm.GCMNotifierService;
import ph.com.smart.racetoace.rnr.model.Award;
import ph.com.smart.racetoace.rnr.model.Recognition;
import ph.com.smart.racetoace.rnr.model.Survey;
import ph.com.smart.racetoace.rnr.model.TeamChallenge;
import ph.com.smart.racetoace.rnr.model.UserTeam;
import ph.com.smart.racetoace.web.rest.dto.RecognitionDTO;
import ph.com.smart.racetoace.web.rest.dto.TeamChallengeDTO;
import ph.com.smart.racetoace.web.rest.dto.UserProfileDTO;
import ph.com.smart.racetoace.web.rest.dto.mapper.ChallengeDTOMapper;

/**
 * The one who cascades {@link Event}s to GCM and APNS 
 * @author jrdomingo
 * @since Sep 24, 2015 1:16:10 PM
 *
 */
@Service
@ConfigurationProperties(prefix = "app")
public class PushGateway {
	
	private static final Logger logger = LoggerFactory.getLogger(PushGateway.class);
	private String challengeStartedMessage = "New challenge";
	private String challengeEndedMessage = "Challenge closed";
	private String challengeHasWinnerMessage = "Challenge winner";
	private String awardStartedMessage = "Nomination is now open";
	private String awardEndedMessage = "Award winners";
	
	private String teamChallengeStartedMessage = "New team challenge\n{}";
	private String teamChallengeEndedMessage = "Team challenge closed\n{}";
	private String teamChallengeCompletedMessage = "Team challenge completed\n{} completed {}! Congratulations!";
	private String newSurveyMessage = "New survey\n{}";
	private String newRecognitionMessage = "Cause for Applause\n{} is the {}";
	
	@Inject
	GCMNotifierService gcmNotifierService;
	
	@Inject
	ApplePushNotificationService apns;

	private String rootTopic= "/topics/racetoace";
	
	private String groupTopicPrefix = "/topics/racetoace/groups/";
	
	@Inject
	public PushGateway(	Environment env){
		String[] profiles = env.getActiveProfiles(); 
		if(profiles.length>1){
			if(profiles[0].equals(Constants.SPRING_PROFILE_DEVELOPMENT)){
				rootTopic = "/topics/racetoacedev";
				setGroupTopicPrefix("/topics/racetoacedev/groups/");
			}
		}
	}
	
	
	/**
	 * Utility method for converting GCM events to APNS(because GCM is more straight forward)
	 * @param event
	 * @return
	 * @throws JSONException
	 */
	PushNotificationPayload eventToPayload(Event event) throws JSONException{
		PushNotificationPayload payload = PushNotificationPayload.complex();
		if(event.type.equals(Type.CHALLENGE_STARTED)){
			payload.addCustomAlertBody(challengeStartedMessage+"\n"+event.challenge.title);
			payload.addCustomDictionary("challengeId", event.challenge.id);
		} else if(event.type.equals(Type.CHALLENGE_ENDED)){
			payload.addCustomAlertBody(challengeEndedMessage+"\n"+event.challenge.title);
			payload.addCustomDictionary("challengeId", event.challenge.id);
			
		} else if(event.type.equals(Type.CHALLENGE_HAS_WINNER)){
			payload.addAlert(challengeHasWinnerMessage+"\n"+event.challenge.title);
			payload.addCustomDictionary("challengeId", event.challenge.id);
			
		} else if(event.type.equals(Type.AWARD_STARTED)){
			payload.addAlert(awardStartedMessage+"\n"+event.award.name);
			payload.addCustomDictionary("awardId", event.award.id);
			
		}  else if(event.type.equals(Type.AWARD_ENDED)){
			String customBody = "awardEndedMessage";
			if(event.challenge!=null && event.user !=null){
				customBody += "\n"+event.user.name+" completed "+event.award.name+" first! Congratulations!";
			}
			payload.addAlert(customBody);
			payload.addCustomDictionary("award", event.award.id);
		} else if(event.type.equals(Type.TEAM_CHALLENGE_STARTED)){
			payload.addCustomAlertBody(event.message);
			payload.addCustomDictionary("teamChallengeId", event.teamChallenge.id);
		} else if(event.type.equals(Type.TEAM_CHALLENGE_ENDED)){
			payload.addCustomAlertBody(event.message);
			payload.addCustomDictionary("teamChallengeId", event.teamChallenge.id);
			
		} else if(event.type.equals(Type.RECOGNITION_NEW)){
			payload.addCustomAlertBody(event.message);
			payload.addCustomDictionary("recognitionId", event.recognition.id);
			
		} else if(event.type.equals(Type.SURVEY_NEW)){
			payload.addCustomAlertBody(event.message);
			payload.addCustomDictionary("surveyId", event.survey.id);
			
		} else if(event.type.equals(Type.TEAM_CHALLENGE_COMPLETED)){
			payload.addCustomAlertBody(event.message);
			payload.addCustomDictionary("teamChallengeId", event.teamChallenge.id);
			
		} else {
			payload.addAlert("Race to Ace!");
		}
		payload.addCustomDictionary("type",event.type.toString());
		
		return payload;
		
	}
	
	

	public void notifyChallengeHasStarted(Challenge challenge){
		
		/**Android**/
		Event event = new Event();
		event.type = Type.CHALLENGE_STARTED;
		event.challenge = ChallengeDTOMapper.challengeToChallengeDTO(challenge, false);
		event.message = MessageFormatter.format(challengeStartedMessage, challenge.title).getMessage();
		gcmNotifierService.sendToGcm(rootTopic, new EventWrapper(event));
		
		/**APNS**/
		PushNotificationPayload payload = null;
		try {
			payload = eventToPayload(event);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		apns.sendAppleUsers(payload, null);
	}

	public void notifyChallengeHasEnded(Challenge challenge){
		/**Android**/
		Event event = new Event();
		event.type = Type.CHALLENGE_ENDED;
		event.challenge = ChallengeDTOMapper.challengeToChallengeDTO(challenge, false);
		event.message = MessageFormatter.format(challengeEndedMessage, challenge.title).getMessage();
		gcmNotifierService.sendToGcm(rootTopic, new EventWrapper(event));

		/**APNS**/
		PushNotificationPayload payload = null;
		try {
			payload = eventToPayload(event);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		apns.sendAppleUsers(payload, null);
	}

	public void notifyChallengeHasWinner(Challenge challenge, UserProfileDTO user){
		/**Android**/
		Event event = new Event();
		event.type = Type.CHALLENGE_HAS_WINNER;
		event.challenge = ChallengeDTOMapper.challengeToChallengeDTO(challenge, false);
		event.user = user;
		event.message = MessageFormatter.format(challengeHasWinnerMessage, user.name, challenge.title).getMessage();
		gcmNotifierService.sendToGcm(rootTopic, new EventWrapper(event));

		/**APNS**/
		PushNotificationPayload payload = null;
		try {
			payload = eventToPayload(event);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		apns.sendAppleUsers(payload, null);
	}

	public void notifyAwardHasStarted(Award award){
		
		/**Android**/
		Event event = new Event();
		event.type = Type.AWARD_STARTED;
		event.award = award;
		event.message = MessageFormatter.format(awardStartedMessage, award.name).getMessage();
		long groupId = 0;
		if(award.userGroup!=null){
			groupId = award.userGroup.id;
		}
		gcmNotifierService.sendToGcm(rootTopic, new EventWrapper(event));
		

		/**APNS**/
		PushNotificationPayload payload = null;
		try {
			payload = eventToPayload(event);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if(award.userGroup!=null){
			apns.sendAppleUsers(payload, award.userGroup);
		}
	}

	public void notifyAwardHasEnded(Award award){
		
		/**Android**/
		Event event = new Event();
		event.type = Type.AWARD_ENDED;
		event.award = award;
		event.message = MessageFormatter.format(awardEndedMessage, award.name).getMessage();

		long groupId = 0;
		if(award.userGroup!=null){
			groupId = award.userGroup.id;
		}
		
		
		gcmNotifierService.sendToGcm(rootTopic, new EventWrapper(event));

		/**APNS**/
		PushNotificationPayload payload = null;
		try {
			payload = eventToPayload(event);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		if(award.userGroup!=null){
			apns.sendAppleUsers(payload, award.userGroup);
		}
	}



	public void notifyAnnouncement(String message){
		Event event = new Event();
		event.type = Type.ANNOUNCEMENT;
		event.message = message;
		gcmNotifierService.sendToGcm(rootTopic, new EventWrapper(event));
	}

	
	/********************
	 *  Phase 2 
	 *  Notifications
	 ********************/
	

	public void notifyTeamChallengeHasStarted(TeamChallenge teamChallenge){
		
		/**Android**/
		Event event = new Event();
		event.type = Type.TEAM_CHALLENGE_STARTED;
		event.teamChallenge = new TeamChallengeDTO(teamChallenge, null);
		event.message = MessageFormatter.format(teamChallengeStartedMessage, teamChallenge.title).getMessage();
		gcmNotifierService.sendToGcm(rootTopic, new EventWrapper(event));
		
		/**APNS**/
		PushNotificationPayload payload = null;
		try {
			payload = eventToPayload(event);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		apns.sendAppleUsers(payload, null);
	}

	public void notifyTeamChallengeHasEnded(TeamChallenge teamChallenge){
		/**Android**/
		Event event = new Event();
		event.type = Type.TEAM_CHALLENGE_ENDED;
		event.teamChallenge = new TeamChallengeDTO(teamChallenge, null);
		event.message = MessageFormatter.format(teamChallengeEndedMessage, teamChallenge.title).getMessage();
		gcmNotifierService.sendToGcm(rootTopic, new EventWrapper(event));

		/**APNS**/
		PushNotificationPayload payload = null;
		try {
			payload = eventToPayload(event);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		apns.sendAppleUsers(payload, null);
	}

	public void notifyTeamChallengeHasCompleted(TeamChallenge teamChallenge, UserTeam userTeam){
		/**Android**/
		Event event = new Event();
		event.type = Type.TEAM_CHALLENGE_COMPLETED;
		event.teamChallenge = new TeamChallengeDTO(teamChallenge, null);
		event.message = MessageFormatter.format(teamChallengeCompletedMessage, userTeam.name, teamChallenge.title).getMessage();
		gcmNotifierService.sendToGcm(rootTopic, new EventWrapper(event));

		/**APNS**/
		PushNotificationPayload payload = null;
		try {
			payload = eventToPayload(event);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		apns.sendAppleUsers(payload, null);
	}

	public void notifyNewRecognition(Recognition recognition){
		/**Android**/
		Event event = new Event();
		event.type = Type.RECOGNITION_NEW;
		event.recognition = new RecognitionDTO(recognition);
		event.message = MessageFormatter.format(newRecognitionMessage, recognition.user.profile.name, recognition.title).getMessage();
		gcmNotifierService.sendToGcm(rootTopic, new EventWrapper(event));

		/**APNS**/
		PushNotificationPayload payload = null;
		try {
			payload = eventToPayload(event);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		apns.sendAppleUsers(payload, null);
	}

	public void notifyNewSurvey(Survey survey){
		/**Android**/
		Event event = new Event();
		event.type = Type.SURVEY_NEW;
		event.survey = survey;
		event.message = MessageFormatter.format(newSurveyMessage, survey.title).getMessage();
		gcmNotifierService.sendToGcm(rootTopic, new EventWrapper(event));

		/**APNS**/
		PushNotificationPayload payload = null;
		try {
			payload = eventToPayload(event);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		apns.sendAppleUsers(payload, null);
	}


	
	public String getChallengeStartedMessage() {
		return challengeStartedMessage;
	}


	public void setChallengeStartedMessage(String challengeStartedMessage) {
		this.challengeStartedMessage = challengeStartedMessage;
	}

	public String getChallengeHasWinnerMessage() {
		return challengeHasWinnerMessage;
	}


	public void setChallengeHasWinnerMessage(String challengeHasWinnerMessage) {
		this.challengeHasWinnerMessage = challengeHasWinnerMessage;
	}

	public String getChallengeEndedMessage() {
		return challengeEndedMessage;
	}


	public void setChallengeEndedMessage(String challengeEndedMessage) {
		this.challengeEndedMessage = challengeEndedMessage;
	}

	public String getAwardStartedMessage() {
		return awardStartedMessage;
	}


	public void setAwardStartedMessage(String awardStartedMessage) {
		this.awardStartedMessage = awardStartedMessage;
	}

	public String getAwardEndedMessage() {
		return awardEndedMessage;
	}


	public void setAwardEndedMessage(String awardEndedMessage) {
		this.awardEndedMessage = awardEndedMessage;
	}


	public String getRootTopic() {
		return rootTopic;
	}

	public void setRootTopic(String rootTopic) {
		this.rootTopic = rootTopic;
	}


	public String getGroupTopicPrefix() {
		return groupTopicPrefix;
	}

	public void setGroupTopicPrefix(String groupTopicPrefix) {
		this.groupTopicPrefix = groupTopicPrefix;
	}


	public String getTeamChallengeCompletedMessage() {
		return teamChallengeCompletedMessage;
	}

	public void setTeamChallengeCompletedMessage(String teamChallengeCompletedMessage) {
		this.teamChallengeCompletedMessage = teamChallengeCompletedMessage;
	}


	public static class EventWrapper{
		public Event event;
		public EventWrapper(){};
		public EventWrapper(Event event){
			this.event = event;
		}
	}
	
}
