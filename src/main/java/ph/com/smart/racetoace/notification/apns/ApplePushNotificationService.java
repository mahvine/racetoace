package ph.com.smart.racetoace.notification.apns;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;
import javapns.notification.transmission.PushQueue;
import ph.com.smart.racetoace.config.Constants;
import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.repository.UserRepository;
import ph.com.smart.racetoace.rnr.model.UserGroup;

/**
 * Communicates with APNS using JavaPNS 2.3
 * @author JRDomingo
 * @since Sep 24, 2015 4:02:40 PM
 *
 */
@Service
public class ApplePushNotificationService { 
	
	
	private Logger logger = (Logger) LoggerFactory.getLogger(ApplePushNotificationService.class.getName());
	
	@Value("${apns.keystoreLocation}")
	private String keystoreLocation;
	
	@Value("${apns.keystorePassword}")
	private String keystorePassword;
	
	@Inject
	UserRepository userRepository;
	
	@Value("${apns.production}")
	private boolean production = true;
	
	PushQueue queue;
	
	@Inject
	public ApplePushNotificationService(Environment env){
		String[] profiles = env.getActiveProfiles(); 
		if(profiles.length>1){
			if(profiles[0].equals(Constants.SPRING_PROFILE_DEVELOPMENT)){
				setProduction(true); // 
//				ProxyManager.setProxy("10.128.51.104", "1154");
			}
		}
		
	}
	
	//TODO remove me here
	public void sendAppleUsers(PushNotificationPayload payload, UserGroup userGroup){
		logger.debug("sending:{} to apple users with userGroup:{}",payload,userGroup);
		List<User> users = null;
		if(userGroup!=null){
			users = userRepository.findByUserGroupAndApnsTokenNotNull(userGroup);
		}else{
			users = userRepository.findByApnsTokenNotNull();
		}
		List<String> apnsTokens = new ArrayList<String>();
		for(User user: users){
			apnsTokens.add(user.apnsToken);
		}
		sendApns(apnsTokens, payload);
	}

	@Async
	public void sendApns(List<String> apnsTokens, PushNotificationPayload payload) {
	       /* Build a blank payload to customize */ 
	       /* Push your custom payload */ 
	        try {
	        	logger.info("Push me out of Smart:{} , users:{}",payload, apnsTokens.size());
	        	logger.debug("apns keystore location:{}",keystoreLocation);
				List<PushedNotification> notifications = Push.payload(payload, keystoreLocation, keystorePassword, production, apnsTokens);
				for(PushedNotification notification: notifications){
					logger.info("Apple Notification:"+notification.toString());
				}
				logger.info("apple push notification sent:{}",notifications.size());
			} catch (CommunicationException e) {
				e.printStackTrace();
			} catch (KeystoreException e) {
				e.printStackTrace();
			} catch (RuntimeException e) {
				e.printStackTrace();
			} catch (Exception e){
				e.printStackTrace();
			}
	}

	public String getKeystoreLocation() {
		return keystoreLocation;
	}

	public void setKeystoreLocation(String keystoreLocation) {
		this.keystoreLocation = keystoreLocation;
	}

	public String getKeystorePassword() {
		return keystorePassword;
	}

	public void setKeystorePassword(String keystorePassword) {
		this.keystorePassword = keystorePassword;
	}

	public boolean isProduction() {
		return production;
	}

	public void setProduction(boolean production) {
		this.production = production;
	}


}
