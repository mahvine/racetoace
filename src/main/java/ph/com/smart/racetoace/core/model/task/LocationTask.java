package ph.com.smart.racetoace.core.model.task;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ph.com.smart.racetoace.core.model.Task;

@Entity
@DiscriminatorValue("location")
public class LocationTask extends Task{
	
	public Float longitude;
	
	public Float latitude;
	
	public Integer radius;

}
