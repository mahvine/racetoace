package ph.com.smart.racetoace.core.model.task;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CollectionTable;
import javax.persistence.DiscriminatorValue;
import javax.persistence.ElementCollection;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;

import ph.com.smart.racetoace.core.model.Task;
import ph.com.smart.racetoace.core.model.User;

/**
 * A task where a {@link User} needs to feedback to a specific question
 * 
 * @author JRDomingo
 * @since Aug 24, 2015 9:24:29 AM
 *
 */
@Entity
@DiscriminatorValue("poll")
public class PollTask extends Task{

	@ElementCollection
	@CollectionTable(name = "poll_selection", joinColumns = 
		@JoinColumn(name = "selection_id") 
	)
	public List<Selection> selections = new ArrayList<Selection>();

	@Embeddable
	public static class Selection {
		public String choice;
		public String text;
		@Override
		public String toString() {
			return "{choice:" + choice + ", text:" + text + "}";
		}
		
	}

	@Override
	public String toString() {
		return "{selections:" + selections + ", id:" + id + ", description:" + description + "}";
	}
	
	

}
