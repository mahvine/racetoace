package ph.com.smart.racetoace.core.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ph.com.smart.racetoace.core.model.CompletedTask.TaskUserPK;

/**
 * Represents a {@link Task} completed by a {@link User}
 * @author JRDomingo
 * @since Sep 2, 2015 11:21:27 AM
 *
 */
@Entity
@Table(name="completed_task")
@IdClass(TaskUserPK.class)
public class CompletedTask {

	@Id
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	public User user;
	
	@Id
	@ManyToOne
	@JoinColumn(name="task_id", referencedColumnName="id")
	public Task task;
	
	public Date date;
	
	@SuppressWarnings("serial")
	public static class TaskUserPK implements Serializable{
		private Long user;
		private Long task;
		@Override
		public String toString() {
			return "{user:" + user + ", task:" + task + "}";
		}
		public Long getUser() {
			return user;
		}
		public void setUser(Long user) {
			this.user = user;
		}
		public Long getTask() {
			return task;
		}
		public void setTask(Long task) {
			this.task = task;
		}
	}
}
