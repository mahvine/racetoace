/**
 * 
 */
package ph.com.smart.racetoace.core.task.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.model.task.PollFeedback;

/**
 * @author JRDomingo
 * @since Sep 2, 2015 11:55:36 AM
 * 
 */
@Repository
public interface PollFeedbackRepository extends JpaRepository<PollFeedback, PollFeedback.PK>{
	
	public List<PollFeedback> findByUser(User user);

	@Query(value="select pf.choice, count(pf.pollTask) from PollFeedback pf where pf.pollTask.id = ?1 group by pf.choice")
	public List<Object[]> getPollFeedbacks(Long pollTaskId);

}
