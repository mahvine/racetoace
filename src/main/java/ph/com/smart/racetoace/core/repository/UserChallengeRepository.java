/**
 * 
 */
package ph.com.smart.racetoace.core.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.model.UserChallenge;
import ph.com.smart.racetoace.core.model.UserChallenge.ChallengeUserPK;
import ph.com.smart.racetoace.core.model.UserChallenge.Status;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 9:17:13 AM
 * 
 */
@Repository
public interface UserChallengeRepository extends JpaRepository<UserChallenge, ChallengeUserPK> {

	public List<UserChallenge> findByUser(User user);

	public List<UserChallenge> findByUserAndStatus(User user, Status status);

	public List<UserChallenge> findByChallenge(Challenge challenge);
	
	public List<UserChallenge> findByChallengeAndStatus(Challenge challenge, Status status);

	public List<UserChallenge> findByChallenge(Challenge challenge, Sort sort);
	
	public List<UserChallenge> findByChallengeAndStatus(Challenge challenge, Status status, Sort sort);

	public List<UserChallenge> findByStatusAndChallengeEndDateBefore(Status status, Date date);
	
	public UserChallenge findByUserAndChallenge(User user, Challenge challenge);

	public long countByChallengeAndStatus(Challenge challenge, Status status);
	
}
