package ph.com.smart.racetoace.core.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ph.com.smart.racetoace.core.model.UserChallenge.ChallengeUserPK;

/**
 * Represents {@link Challenge} accepted by a {@link User}
 * @author JRDomingo
 * @since Sep 1, 2015 9:50:10 AM
 *
 */
@Entity
@Table(name="user_challenge")
@IdClass(ChallengeUserPK.class)
public class UserChallenge {

	@Id
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	public User user;
	
	@Id
	@ManyToOne
	@JoinColumn(name="challenge_id", referencedColumnName="id")
	public Challenge challenge;
	
	public boolean winner;
	
	public Long pointsEarned;
	
	public Date date;
	
	@Column(name="date_completed")
	public Date dateCompleted;
	
	@Enumerated(EnumType.STRING)
	public Status status = Status.ACCEPTED;
	
	public enum Status{
		ACCEPTED, FAILED, COMPLETED
		,OPEN,CLOSED	//Not persisted, used by DTO only
	}
	
	@SuppressWarnings("serial")
	public static class ChallengeUserPK implements Serializable{
		private Long user;
		private Long challenge;
		public Long getUser() {
			return user;
		}
		public void setUser(Long user) {
			this.user = user;
		}
		public Long getChallenge() {
			return challenge;
		}
		public void setChallenge(Long challenge) {
			this.challenge = challenge;
		}
	}

	@Override
	public String toString() {
		return "{user:" + user + ", challenge:" + challenge + ", winner:" + winner + ", pointsEarned:" + pointsEarned
				+ ", date:" + date + ", status:" + status + "}";
	}
	
}
