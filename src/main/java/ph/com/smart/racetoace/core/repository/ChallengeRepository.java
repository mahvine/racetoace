package ph.com.smart.racetoace.core.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.core.model.Challenge;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 9:16:31 AM
 * 
 */
@Repository
public interface ChallengeRepository extends JpaRepository<Challenge,Long>{

	List<Challenge> findByStartDateAfter(Date startDate, Sort sort);
	List<Challenge> findByStartDateBeforeAndEndDateAfter(Date startDate, Date endDate, Sort sort);
	Challenge findByTasksId(Long taskId);

	List<Challenge> findByStartDate(Date startDate);
	
	List<Challenge> findByEndDate(Date endDate);
	
}
