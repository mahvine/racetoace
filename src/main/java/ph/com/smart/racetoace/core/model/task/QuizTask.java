package ph.com.smart.racetoace.core.model.task;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ph.com.smart.racetoace.core.model.Task;

/**
 * A task that has a predefined answer
 * @author JRDomingo
 * @since Aug 24, 2015 9:24:29 AM
 *
 */
@Entity
@DiscriminatorValue("quiz")
public class QuizTask extends Task{
	
//	
//	@ElementCollection
//	@CollectionTable(name = "quiz_selection", joinColumns = 
//		@JoinColumn(name = "selection_id") 
//	)
//	public List<Selection> selections = new ArrayList<Selection>();
//	
//	@Embeddable
//	public static class Selection{
//		public String choice;
//		public String text;
//		public boolean isAnswer;
//	}
	
	public String answer;

}
