package ph.com.smart.racetoace.core.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by KCAsis on 9/9/2015.
 */
@Entity
public class UserProfile {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    public Long id;
    public String name;
    @Column(name = "picture_url")
    public String pictureUrl;
    @Column(name = "birth_date")
    public Date birthDate;
    @Column(name = "hiring_date")
    public Date hiringDate;

}
