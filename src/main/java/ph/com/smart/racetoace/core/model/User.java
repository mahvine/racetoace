package ph.com.smart.racetoace.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

import ph.com.smart.racetoace.rnr.model.Nomination;
import ph.com.smart.racetoace.rnr.model.Redemption;
import ph.com.smart.racetoace.rnr.model.UserGroup;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy= GenerationType.AUTO)
	public Long id;
	
	@Column(name="email",unique=true,nullable=false)
	public String email;
	
	@Column(name="encryptedPassword")
	public String encryptedPassword;
	
	public String salt;

	@Column(name = "reward_points")
	public long rewardPoints;
	
	@Column(name="is_award_giver")
	public boolean awardGiver;
	
	/**
	 * Tracks {@link User}'s cummulative points minus penalty
	 */
	@Column(name= "score_points")
	public long scorePoints;

	@OneToOne(fetch=FetchType.EAGER, cascade={CascadeType.REMOVE})
	@JoinColumn(name="profile_id", referencedColumnName="id")
	public UserProfile profile;
	
	@ElementCollection(targetClass = Role.class)
	@Column(name = "name", nullable = false)
	@Enumerated(EnumType.STRING)
	public List<Role> roles = new ArrayList<Role>();
	
	@Column(name="reset_key")
	public String resetKey;

	@Column(name="activation_key")
	public String activationKey;

	@JsonIgnore
	@ManyToOne
	@JoinColumn(name="user_group_id", referencedColumnName="id")
	public UserGroup userGroup;

	@Column(name="apns_token")
	public String apnsToken;

	@Column(name="gcm_id")
	public String gcmId;
	
	@Transient
	public long nominations;

	@Enumerated(EnumType.STRING)
	public Status status = Status.CREATED;
	
	public enum Role{
		CHALLENGER,		//basic role
		CHALLENGE_MANAGER 	//manages the challenge
	}
	
	public enum Status{
		CREATED,VERIFIED
	}
	
	@Column(name="date_registered")
	public Date dateRegistered;
	
	
	
}
