/**
 * 
 */
package ph.com.smart.racetoace.core.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.core.model.task.PollTask;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 10:05:36 AM
 * 
 */
@Repository
public interface PollTaskRepository extends JpaRepository<PollTask, Long> {

}
