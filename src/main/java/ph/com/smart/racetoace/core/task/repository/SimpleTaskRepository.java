package ph.com.smart.racetoace.core.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.core.model.task.SimpleTask;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 9:18:05 AM
 * 
 */
@Repository
public interface SimpleTaskRepository extends JpaRepository<SimpleTask, Long>{

}
