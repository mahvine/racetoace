/**
 * 
 */
package ph.com.smart.racetoace.core.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.rnr.model.UserGroup;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 9:17:20 AM
 * 
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>{

	User findByEmailIgnoreCase(String email);
	
	User findByActivationKey(String activationKey);

	List<User> findByUserGroup(UserGroup userGroup);
	
	List<User> findByUserGroupAndApnsTokenNotNull(UserGroup userGroup);
	
	List<User> findByApnsTokenNotNull();

	Page<User> findByUserGroupNotNull(Pageable pageable); 

	List<User> findByUserGroupNotNull(); 

}
