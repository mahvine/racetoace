/**
 * 
 */
package ph.com.smart.racetoace.core.model.task;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import ph.com.smart.racetoace.core.model.User;
import ph.com.smart.racetoace.core.model.task.PollFeedback.PK;

/**
 * {@link PollTask} answered by a {@link User}   
 * @author JRDomingo
 * @since Sep 2, 2015 11:49:20 AM
 * 
 */
@Entity
@Table(name="poll_feedback")
@IdClass(PK.class)
public class PollFeedback {

	@Id
	@ManyToOne
	@JoinColumn(name="user_id", referencedColumnName="id")
	public User user;
	
	@Id
	@ManyToOne
	@JoinColumn(name="poll_task_id", referencedColumnName="id")
	public PollTask pollTask;
	
	public String choice;
	
	public Date date;
	
	
	public static class PK implements Serializable{
		/**
		 * 
		 */
		private static final long serialVersionUID = 2536468045261406249L;
		private Long user;
		private Long pollTask;
		@Override
		public String toString() {
			return "{user:" + user + ", pollTask:" + pollTask + "}";
		}
		public Long getUser() {
			return user;
		}
		public void setUser(Long user) {
			this.user = user;
		}
		public Long getPollTask() {
			return pollTask;
		}
		public void setPollTask(Long pollTask) {
			this.pollTask = pollTask;
		}
	}
}
