package ph.com.smart.racetoace.core.model.task;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

import ph.com.smart.racetoace.core.model.Task;
import ph.com.smart.racetoace.core.model.User;

/**
 * A simple task wherein a {@link User} needs to manually input a code given at the end of a real world task
 * @author JRDomingo
 * @since Aug 24, 2015 9:18:24 AM
 *
 */
@Entity
@DiscriminatorValue("simple")
public class SimpleTask extends Task{
	
	public String code;

}
