/**
 * 
 */
package ph.com.smart.racetoace.core.task.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.core.model.task.LocationTask;

/**
 * 
 * @author JRDomingo
 * @since Sep 6, 2015 10:19:03 AM
 */
@Repository
public interface LocationTaskRepository extends JpaRepository<LocationTask,Long>{
	

}
