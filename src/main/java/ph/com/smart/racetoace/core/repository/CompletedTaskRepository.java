/**
 * 
 */
package ph.com.smart.racetoace.core.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ph.com.smart.racetoace.core.model.Challenge;
import ph.com.smart.racetoace.core.model.CompletedTask;
import ph.com.smart.racetoace.core.model.CompletedTask.TaskUserPK;
import ph.com.smart.racetoace.core.model.Task;
import ph.com.smart.racetoace.core.model.User;

/**
 * @author JRDomingo
 * @since Aug 24, 2015 9:17:13 AM
 * 
 */
@Repository
public interface CompletedTaskRepository extends JpaRepository<CompletedTask, TaskUserPK>{

	public CompletedTask findByTaskAndUser(Task task, User user);
	

	public List<CompletedTask> findByUser(User user);

	public List<CompletedTask> findByUserAndTaskChallengeIn(User user, List<Challenge> challenges);
	
	public List<CompletedTask> findByUserAndTaskChallenge(User user, Challenge challenge);
	
}
