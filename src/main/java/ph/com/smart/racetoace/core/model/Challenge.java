package ph.com.smart.racetoace.core.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 
 * @author JRDomingo
 * @since Sep 1, 2015 9:51:23 AM
 *
 */
@Entity
@Table(name="challenge")
public class Challenge {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long id;
	
	public String title;
	
	public String details;
	
	public Date startDate;
	
	public Date endDate;
	
	public Long penalty;
	
	@Column(name="winner_points")
	public Long winnerPoints;

	@Column(name="consolation_points")
	public Long consolationPoints;
	
	@OneToMany
	@JoinColumn(name="challenge_id", referencedColumnName="id")
	public List<Task> tasks = new ArrayList<Task>();
}
