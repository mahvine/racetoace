package ph.com.smart.racetoace.core.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ph.com.smart.racetoace.core.model.UserProfile;

/**
 * Created by KCAsis on 9/9/2015.
 */
public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {
}
